<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Module Install 
 * 
 * @package Module Install  for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */

class Module_install {
	
	function Module_install() {
        $this->CI =& get_instance();
		$this->CI->load->model('crud');
	}
	
	function set_table(){
		$this->CI->crud->use_table('cms_modules');
	}
	
	/**
	 * install
	 *
	 * This function Used install modules
	 *
	 * @param	String  $module   Required
	 * @access  public
	 * @return	boolean		True if successful / False on failure
	 */
	function install($module = '', $acl = '', $acg = '', $overwrite = 'false'){
		$this->set_table();
		if(empty($module)){
			log_message('error', 'Library: Module_install; Method: install($module); $module Required');
            return FALSE;
		}else{
			$data = array(
			   'module' => $module,
			   'acl' => $acl,
			   'acg' => $acg
			);
			$count = $this->CI->crud->count_results(array('module' => $module));
			if($count != 0 && $overwrite == 'false'){
				return FALSE;
			}else{
				$insert = $this->CI->crud->create($data);
				if($insert){
					return TRUE;
				}else{
					log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); insert failed');	
					return FALSE;
				}
			}
		}
	}
	
	
	/**
	 * uninstall
	 *
	 * This function Used uninstall modules
	 *
	 * @param	String  $module   Required
	 * @access  public
	 * @return	boolean		True if successful / False on failure
	 */
	function uninstall($module = ''){
		$this->set_table();
	 	if(empty($module)){
			log_message('error', 'Library: Navigation; Method: unset_nav_items($module); $module Required');
            return FALSE;
		}else{
			$this->CI->crud->delete(array('module' => $module));
			return TRUE;
		}
	}

	/**
	 * uninstall
	 *
	 * This function Used uninstall modules
	 *
	 * @param	String  $module   Required
	 * @access  public
	 * @return	boolean		True if successful / False on failure
	 */
	function finish($module = ''){			
			redirect("mange_modules", 'location');
	}
	
}