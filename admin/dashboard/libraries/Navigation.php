<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Navigation 
 * 
 * @package Navigation for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */

 
 

class Navigation {
	
	
	function Navigation() {
        $this->CI =& get_instance();
		$this->CI->load->model('crud');
		
	}
	
	function set_table(){
		$this->CI->crud->use_table('cms_navigation');
	}
	
	 /**
	 * set_nav_item
	 *
	 * This function Used to install a navigation Item
	 *
	 * @param	String  $module   Required
	 * @param	String  $parent   Required
	 * @param	String  $title     Required 
	 * @param	String  $helpText Optional
	 * @param	String  $link 	  Required
	 * @param	String  $icon 	  Optional 
	 * @access  public
	 * @return	boolean		True if successful / False on failure
	 */
	function set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "2", $acl = '', $acg = '') {
		$this->set_table();
		if(empty($module)){
			log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); $module Required');
            return FALSE;
		}elseif(empty($parent)){
			log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); $parent Required');
            return FALSE;
		}elseif(empty($title)){
			log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); $tite Required');
            return FALSE;
		}elseif(empty($link)){
			log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); $link Required');
            return FALSE;
		}else{
			$data = array(
			   'module' => $module ,
			   'parent' => $parent ,
			   'title' => $title ,
			   'helpText' => $helpText ,
			   'link' => $link ,
			   'icon' => $icon,
			   'active' => $active,
			   'uri' => $uri,
			   'acl' => $acl,
			   'acg' => $acg
			);
			$count = $this->CI->crud->count_results(array('title' => $title, 'parent' => $parent));
			if($count != 0){
				return FALSE;
			}else{
				$insert = $this->CI->crud->create($data);
				if($insert){
					return TRUE;
				}else{
					log_message('error', 'Library: Navigation; Method: set_nav_item($module , $parent, $tite, $helpText , $link , $icon); insert failed');	
					return FALSE;
				}
			}
		}
	 }
	 
	/**
	 * unset_nav_items
	 *
	 * This function Used to uninstall a navigation Item
	 *
	 * @param	String  $module   Required
	 * @access  public
	 * @return	boolean		True if successful / False on failure
	 */
	 function unset_nav_items($module = ''){
	 	$this->set_table();
	 	if(empty($module)){
			log_message('error', 'Library: Navigation; Method: unset_nav_items($module); $module Required');
            return FALSE;
		}else{
			$this->CI->crud->delete(array('module' => $module));
			return TRUE;
		}
	 }
}