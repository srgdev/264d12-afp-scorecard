<?php

class Logevents {
	
	var $CI;
	var $badUrl;
	var $goodUrl;
	
	function Logevents(){
		$this->CI =& get_instance();
		
	}
	
	function logEvent($action, $desc){
		if($this->CI->configs->get("logging") == "true"){
			$user = $this->CI->auth->getUserName();;
			$data = array(
			   'action' => $action ,
			   'desc' => $desc ,
			   'user' => $user
			);
			
			$this->CI->db->insert('cms_log', $data); 
		}
	}
	
	function getLog($limit = 10){
		$this->CI->db->order_by("id", "desc"); 
		$query = $this->CI->db->get('cms_log', $limit);
		return $query->result();
	}
	
	function getFullLog($limit, $start){
		$this->CI->db->limit($limit, $start);
		$this->CI->db->order_by("id", "desc"); 
		$query = $this->CI->db->get('cms_log');
		return $query->result();
	}
	
	function record_count(){
		return $this->CI->db->count_all("cms_log");
	}
	
	function purge($val){
		
		$query = $this->CI->db->query("DELETE FROM cms_log WHERE DATE_SUB(CURDATE(),INTERVAL ".$val." DAY) >= date");
			
		// $query = $this->CI->db->get('cms_log');
		
// 		
	}
	
// CREATE TABLE `log` (
  // `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  // `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  // `action` varchar(255) NOT NULL,
  // `desc` varchar(255) NOT NULL,
  // `user` varchar(255) NOT NULL,
  // PRIMARY KEY (`id`)
// ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
	
}