<?php

class Buildtable {


	function Buildtable(){
		$this->CI =& get_instance();
		$this->CI->load->model('crud');
	}
	
	
	function build($titles = '', $fields = '',$rows, $edit = ''){
		
		$table = '<table  class="table table-striped"><thead><tr>';
		foreach($titles as $title){
			$table .= "<th>".$title."</th>";
		}
		$table .= '</tr></thead>';
		
		foreach($rows as $row){
			$i = 1;
			$table .= "<tr>";
			foreach($fields as $field){
				$table .= "<td>";
				$table .= stripslashes($row->$field);
				if($i == 1 && $edit != ''){
					$id = $row->id;
					$edit1 = str_replace('##id', $id, $edit);
					 $table .= $edit1; 
				}
				$table .= "</td>";
				$i++;
			}
			$table .= "</tr>";
			
		}
		
		$table .= "</table>";
		
		return $table;
	}
	
	function buildEdits($editLink, $deleteLink ){
		$edit = '<div class="editsOuter"><div class="edits">';
		$edit .= '<i class="icon-pencil"></i>'.anchor($editLink, 'Edit').' | <i class="icon-remove"></i> '.confirm($deleteLink,'Delete','Delete',"Permently Delete?");
		$edit .= '</div></div>';
		return $edit;
	}
	
	function buildModEdits($editLink ){
		$edit = '<div class="editsOuter"><div class="edits">';
		$edit .= '<i class="icon-check"></i>'.anchor($editLink, 'Moderate');
		$edit .= '</div></div>';
		return $edit;
	}
	
}