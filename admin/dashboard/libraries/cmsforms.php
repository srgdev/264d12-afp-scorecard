<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 *
 */
class CMSforms {

	function __construct() {
		$ci =& get_instance();
	}
	
	function buildForm($table, $options = '', $update = null , $exclude = null){
		
		$ci =& get_instance();
		

		
		
		//loop each field
		
		$q = $ci->db->query("DESCRIBE `$table`");
		
		$col = new StdClass;
		$opts = new StdClass;
		
		$fields = '';
		foreach($q->result() as $field)
        {
        	$part = '';	
        	$col->name = $field->Field;
            $col->type = preg_replace('/\(.+?$/',"",$field->Type);
            $col->primary = ($field->Key == 'PRI') ? 1 : 0;
            $col->auto_increment = ($field->Extra == 'auto_increment') ? 1 : 0;
			
			$name = $col->name;
			
			$opts->class = '';
			$opts->type = 'text';
			$opts->value = (isset($update)) ? $update->$name : '';
			
			if($col->primary == 0 AND $col->auto_increment == 0 AND $col->type != 'timestamp' ) {
				
				//Check Options
				if(!empty($options)){
					
					if(isset($options[$col->name])){
						
						if(isset($options[$col->name]['class'])){
							$opts->class = $options[$col->name]['class'];
						}
						
						if(isset($options[$col->name]['type'])){
							$opts->type = $options[$col->name]['type'];
						}
						
						if(isset($options[$col->name]['value'])){
							if($opts->value == ''){
								$opts->value = $options[$col->name]['value'];
							}
						}
					}
					
				}
				
				if($opts->type == "text" || $opts->type == "password"){
					$part = '<div class="control-group">';
					$part .= '<label class="control-label" for="'.$col->name.'">'.ucfirst($col->name).' *</label>';
					$part .= '<div class="controls">';
					$part .= '<input type="'.$opts->type.'" class="input-xlarge '.$opts->class.'" id="'.$col->name.'" name="'.$col->name.'" value="'.$opts->value.'">';
					$part .= '</div></div>';
				}

				if($opts->type == "textarea"){
					$part = '<div class="control-group">';
					$part .= '<label class="control-label" for="'.$col->name.'">'.ucfirst($col->name).' *</label>';
					$part .= '<div class="controls">';
					$part .= '<textarea  name="'.$col->name.'" class="'.$opts->class.'">'.$opts->value.'</textarea>';
					$part .= '</div></div>';
				}
				
				if($opts->type == "wysiwyg"){
					$part = '<textarea id="editor" name="'.$col->name.'" style="height: 460px;">'.$opts->value.'</textarea><br/>';
				}
				$otps->value = '';
				$fields .= $part;
			}
		}
		
		return $fields;
	}
}
