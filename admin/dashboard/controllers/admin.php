<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
		
    }
	

	

	
	public function index(){
		//$this->cmsemail->sendMail('psradich@gmail.com','','good','Hip Hip Hurray');
		$val = $this->configs->get('logKeepDays');
		$this->logevents->purge($val);
		redirect("admin/dash", 'location');
	}
	
	public function login()
	{
		if(isset($_REQUEST['username'])){
			if(isset($_REQUEST['remeber'])){
				$remeber = TRUE;
			}else{
				$remeber = FALSE;
			}
			// = (isset($_REQUEST['remeber'])) ? $_REQUEST['remeber'] : "";
			
			$this->auth->logIn($_REQUEST['username'], $_REQUEST['password'], $remeber);
		}
		$this->template->set_layout('login')->title('login')->build('pages/login');
	}
	
	public function logout(){
		$this->auth->logOut();
		redirect('admin/login', 'location');
	}
	
	public function changePassword(){
		if(isset($_REQUEST['old'])){
			$pass = $_REQUEST['password'];
			$change = $this->auth->changePass($pass);
			if($change){
				setMessage('success', 'Your Password has been Changed');
				redirect('admin/dash', 'location');
			}else{
				
			}
		}
		$this->template->title('dashboard')->build('pages/changePass');
	}
	
	public function valUserPass(){
		$this->load->model('users');
		
		$validateValue=$_REQUEST['fieldValue'];
		$validateId=$_REQUEST['fieldId'];
		$result  = $this->users->checkPassword($validateValue);
		$arrayToJs = array();
		$arrayToJs[0] = $validateId;
		$arrayToJs[1] = $result;			// RETURN TRUE
		echo json_encode($arrayToJs);
	}
	
	public function dash(){
		
		

		
		$logData = $this->logevents->getLog();
		// $this->template->set('visits', $visits);
		// $this->template->set('evisits', $evisits);
		$this->load->model('pageLoad');
		$loadTime = $this->pageLoad->getAverage();
		
		$this->template->set('logData', $logData);
		$this->template->set('loadTime', $loadTime);
		
		$this->template->title('dashboard')->build('themePages/dashboard');
	}
	
	public function stats(){
		$this->template->title('dashboard')->build('pages/stats');
	}
	
	public function logs(){
		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = base_url() .index_page()."/". "admin/logs";
        $config["total_rows"] = $this->logevents->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
		$config['cur_tag_open'] = '<li class="active"><a href="#" >';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
        $this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$links = $this->pagination->create_links();
		$logData = $this->logevents->getFullLog($config["per_page"], $page);
		$this->template->set('logData', $logData);
		$this->template->set('links', $links);
		
		$this->template->title('dashboard')->build('pages/logs');
	}
	
	public function exportLogs(){
		// $this->db->use_table('cms_log');
		// //$this->db->select('date', 'user', 'action','desc');
		// // run joins, order by, where, or anything else here
		// $query = $this->db->get();
		
		
		$this->toexcel->to_excel('cms_log', 'Event_Logs', 'Event Logs'); 
	}
	
	public function sectionModeration(){
		
		$group = $this->auth->getACG();
		$group  = str_replace(",", " ", $group);
		$group = explode(" ", $group);
		
		$mrows = $this->db->query("SELECT * FROM cms_moderation WHERE section IN('".join("','", $group)."') AND section_mod IS NULL " );
		
		$ids = ""; 
		foreach($mrows->result() as $row){
			$ids .= $row->page_id." ";
		}
		$ids = explode(" ", $ids);
		$rows = $this->db->query("SELECT * FROM cms_pages WHERE id IN('".join("','", $ids)."')");
		$rows = $rows->result();
		$edit = $this->buildtable->buildModEdits("admin/modPage/##id/" ); 
		$table = $this->buildtable->build(array('Title', 'Section', 'Author'), array('title','cat', 'author'), $rows,$edit); 
		
		$this->template->set('tables', $table);
		$this->template->title("Section Moderation")->build('pages/moderation');
		
	}

	public function globalModeration(){
		$this->crud->use_table('cms_moderation');
		$secMod  =  $this->configs->get('groupModeration');
		
		
		if($secMod == "false"){	
			$mrows = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
		}else{
			$mrows = $this->crud->retrieve(array('section_mod' => 'true'), '', 0, 0, array('id' => 'DESC'));	
		}
		
		
		$ids = ""; 
		foreach($mrows as $row){
			$ids .= $row->page_id." ";
		}
		$ids = explode(" ", $ids);
		$rows = $this->db->query("SELECT * FROM cms_pages WHERE id IN('".join("','", $ids)."')");
		$rows = $rows->result();
		
		$edit = $this->buildtable->buildModEdits("admin/modPage/##id/" ); 
		$table = $this->buildtable->build(array('Title', 'Section', 'Author'), array('title','cat', 'author'), $rows,$edit); 
		
		$this->template->set('tables', $table);
		$this->template->title("Section Moderation")->build('pages/moderation');
	}

	public function modPage(){
		$id = $this->uri->segment("3");
		$cat = $this->uri->segment("4");
		$this->pagesModel->editPage($cat);
		$this->template->set('mod', "true");
		$this->template->title('Moderate  Page')->build('pages/editPage');
	}
	
	public function install(){
		
		$this->load->model('admin_install');
		$this->admin_install->installer();
		echo "<br/><br/>";
		echo "Instalation Complete";
	}
	
	public function global_settings(){
		$this->template->title('Settings')->build('pages/global_settings');
	}
	
	
}
	
