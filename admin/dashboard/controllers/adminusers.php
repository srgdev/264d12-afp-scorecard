<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminusers extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function index(){ echo "dd";}
	
	
	public function allusers(){
		$this->load->model('users');
		$permissions = $this->users->permissions();
		if($this->uri->segment(3) === "deleted"){
			$this->template->set('message', "User was successfully deleted");
		}
		
		$data = $this->users->getAll();
		
		$this->crud->use_table('cms_acl');
		$acl = $this->crud->retrieve_all();
		$this->template->set('acl', $acl);
		
		
		$this->template->set('users', $data);
		$this->template->set('permissions', $permissions);
		$this->template->set('search', "no");
		
		$this->template->title('Users')->build('pages/users');
	}
	
	public function editUser(){
		$this->load->model('users');
		$user = $this->uri->segment(3);
		$data = $this->users->getUser($user);
		$permissions = $this->users->permissions();
		
		$this->template->set('user', $data);
		$this->template->set('permissions', $permissions);
		if($this->uri->segment(4) === FALSE ){
			$this->template->set('change', "no");
		}else{
			if($this->uri->segment(4) === "updated"){
				$this->template->set('message', "Updated ");
			}
			if($this->uri->segment(4) === "added"){
				$this->template->set('message', "Added");
			}
			$this->template->set('change', "yes");
		}
		
		$this->crud->use_table('cms_acl');
		$acl = $this->crud->retrieve_all();
		$this->template->set('acl', $acl);
		
		if($this->configs->get('useGroups') == "true"){
			$this->crud->use_table('cms_modules');
			$acg = $this->crud->retrieve_all();
			$this->template->set('acg', $acg);
		}
		
		$this->template->title('Edit User')->build('pages/editUser');
	}
	
	public function addUser(){
		$this->load->model('users');
		
		$this->crud->use_table('cms_acl');
		$acl = $this->crud->retrieve_all();
		$this->template->set('acl', $acl);
		
		if($this->configs->get('useGroups') == "true"){
			$this->crud->use_table('cms_modules');
			$acg = $this->crud->retrieve_all();
			$this->template->set('acg', $acg);
		}
		
		if($this->uri->segment(4) === FALSE ){
			$this->template->set('change', "no");
		}else{
			$this->template->set('change', "yes");
		}
		
		$this->template->title('Add User')->build('pages/addUser');
	}
	
	public function upadteUser(){
		$this->load->model('users');
		$this->users->update();
		$id = $_REQUEST['id'];
		redirect("adminusers/editUser/$id/updated", 'location');
	}
	
	public function insertUser(){
		$this->load->model('users');
		$id = $this->users->insert();
		redirect("adminusers/editUser/$id/added", 'location');
	}
	
	public function deleteUser(){
		$id = $this->uri->segment(3);
		$this->load->model('users');
		$id = $this->users->delete($id);
		redirect("adminusers/allusers/deleted", 'location');
	}
	
	public function searchUsers(){
		$this->load->model('users');
		$result = $this->users->search();
		$this->template->set('users', $result);
		$this->template->set('search', "yes");
		$this->template->set('term', $_REQUEST['term']);
		
		
		
		$this->template->title('Edit User')->build('pages/users');
	}

	public function valUser(){
		$this->load->model('users');
		
		$validateValue=$_REQUEST['fieldValue'];
		$validateId=$_REQUEST['fieldId'];
		$result  = $this->users->checkUsername($validateValue);
		$arrayToJs = array();
		$arrayToJs[0] = $validateId;
		$arrayToJs[1] = $result;			// RETURN TRUE
		echo json_encode($arrayToJs);
	}
}
	
