<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pages_controller extends CI_Controller {
	
	
	function __construct()
    {
        parent::__construct();
		
    }
	
	public function update(){
		$id = $_REQUEST['pageid'];
		$this->pagesModel->update($id);
		$this->session->set_flashdata('flash', 'Page Updated'); 
		redirect($_SERVER['HTTP_REFERER']."/".$id, 'location');
	}
	
	public function promote(){
		$page = $this->uri->segment(4);
		$id = $this->uri->segment(3);
		$this->session->set_flashdata('flash', 'Version Promoted'); 
		$this->pagesModel->promote($id,$page);
		redirect($_SERVER['HTTP_REFERER']."/".$id, 'location');
	}
	
	public function insert(){
		$cat = $this->uri->segment(3);
		$base_type = $this->uri->segment(4);
		if(!$cat){$cat = $_REQUEST['cat'];}
		$id = $this->pagesModel->savePage();
		$this->session->set_flashdata('flash', 'Page Created'); 
		redirect($cat."/editPage/".$id."/".$base_type, 'location');  
	}
	
	public function delete(){
		$this->crud->use_table('cms_pages');
		$id = $this->uri->segment(3);
		$cat = $this->uri->segment(4);
		$page = $this->uri->segment(5);
		$this->crud->delete(array('id' => $id));
		$this->logevents->logEvent($cat." Page deleted",$page." deleted");
		$this->session->set_flashdata('flash', 'Page Deleted'); 
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}
	
	
	public function saveGroupOrder(){
		$this->crud->use_table('cms_pagegroups');
		$data =  json_decode(stripslashes($_REQUEST['data']), true);
		for($i = 0; $i < count($data); $i++){
			//echo "here";
			$order = $i + 1;
			$id = $data[$i]['id'];
			
			if($i == 0){
				$row = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
				$group = $row->group_id;
				$lastOrder = $this->crud->count_all_where(array('group_id' => $group));
			}
			
			$count = $this->crud->count_all_where(array('id' => $id));
			if($count > 0) {
				$this->crud->update(array('id' => $id),array('order' => $order), 0, 0, array('id' => 'DESC'));
			}else{
				$this->crud->create(array('page_id' => $id, 'group_id' => $group, 'order' => $lastOrder));
			}
		}	
		return "good";	
	}
	
	public function deleteGroupItem(){
		$id = $this->uri->segment(3);
		
		$this->crud->use_table('cms_pagegroups');
		$this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
		$this->session->set_flashdata('flash', 'Group Item Deleted'); 
		redirect($_SERVER['HTTP_REFERER'], 'location');
	}
	
}
	
