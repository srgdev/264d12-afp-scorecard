<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo $template['title']; ?></title>
	
        <link href="<?php echo cssPath();?>/base.css" rel="stylesheet" type="text/css" />
				<link href="<?php echo cssPath();?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<?php echo sharedJS();?>/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<link type="text/css" href="<?php echo sharedCSS();?>/smoothness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo sharedJS();?>/redactor/redactor.css" />
		<link rel="stylesheet" href="<?php echo sharedCSS();?>/jgauge.css" type="text/css" />
		
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/jquery-ui-1.8.21.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/jquery.validationEngine.js"></script>
		<script src="<?php echo sharedJS();?>/redactor/redactor.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
		<script src="<?php echo sharedJS();?>/jquery.nestable.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/interface.js"></script>
		<script type="text/javascript" src="<?php echo sharedJS();?>/theme.js"></script>
		<?php
		if(isset($cat)){
			$this->session->set_userdata('section', $cat);
		}else{
			$this->session->set_userdata('section', "");
		} ?>
		
		
		
		<script>
			$(function(){
		
	     var buttons = ['formatting', '|', 'bold', 'italic'];
				
				
		
			
			$('.editor').redactor({ 
                    imageUpload: '<?php echo base_url(); ?><?php echo index_page(); ?>/fileUploads/insertImage',
                    fileUpload: '<?php echo base_url(); ?><?php echo index_page(); ?>/fileUploads/insertFile',
                    imageGetJson: '<?php echo base_url(); ?><?php echo index_page(); ?>/fileUploads/imagejson',
                    fileGetJson: '<?php echo base_url(); ?><?php echo index_page(); ?>/fileUploads/filejson',
                     buttons: ['html', '|', 'formatting', '|', 'bold', 'italic',  '|', 'unorderedlist', 'orderedlist', 'outdent', 'indent', '|', 'image', 'video', 'file', 'table', 'link', '|' , '|', 'alignment', '|', 'horizontalrule']
                    });
                    
                    
        })
        
			var updateOutput = function(e) {
			    var list   = e.length ? e : $(e.target),
			        output = list.data('output');
			    	
			        //output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
			        
			        var jdata = window.JSON.stringify(list.nestable('serialize'))
			        
					var dataUrl = "<?php echo site_url(); ?>/menus/saveMenu";
			    	
					$.ajax({
					  url: dataUrl,
					  type: "POST",
					  data: {data : jdata},
					  dataType: 'json'
					});
			};
			
			
		</script>
		
		
    
	</head>
	<body>
		<div id="wrapper"/>
		<div id="top">
			<div id="logo" class="left">
				Americans For Prosperity &nbsp;&nbsp; 
			</div>
			
			<ul class="topnav left">
				<li <?php if($this->uri->segment(1) === "admin" && $this->uri->segment(2) != "logs" && $this->uri->segment(2) != "stats"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("admin/dash", "<i class='icon-home icon-white'></i> Dashboard", 'title="Dashboard"'); ?>
				</li>
				<?php if($this->auth->checkLevel("1")){ ?>
					<li <?php if($this->uri->segment(1) === "adminusers"){ echo "class=\"active\""; } ?> >
						<?php echo anchor("adminusers/allusers", "<i class='icon-user icon-white'></i> Users", 'title="Manage Users"'); ?>
					</li>
					<li <?php if($this->uri->segment(1) === "settings" && $this->uri->segment(2) === "index"){ echo "class=\"active\""; } ?> >
						<?php echo anchor("settings/index", "<i class='icon-cog icon-white'></i> Settings", 'title="System Settings"'); ?>
					</li>
					<li <?php if($this->uri->segment(2) === "logs"){ echo "class=\"active\""; } ?> >
						<?php echo anchor("admin/logs", "<i class='icon-file icon-white'></i> Event Logs", 'title="Event Logs"'); ?>
					</li>
					<!-- <li <?php if($this->uri->segment(2) === "stats"){ echo "class=\"active\""; } ?> >
						<?php echo anchor("admin/stats", "<i class='icon-signal icon-white'></i> Statistics", 'title="Event Logs"'); ?>
					</li> -->
					<?php if($this->configs->get('moderation') == "true"){ ?>
						<?php $this->load->view("parts/gmod"); ?>
					<?php } ?>
					
				<?php } ?>
				
				
				<?php if($this->auth->getLevel() == "2"){ ?>
					<?php if($this->configs->get('moderation') == "true"){ ?>
						<?php $this->load->view("parts/gmod"); ?>
					<?php } ?>
				<?php } ?>
				
				
				<?php if($this->auth->getLevel() == "3"){ ?>
					<?php if($this->configs->get('moderation') == "true"){ ?>						
						<?php $this->load->view("parts/smod"); ?>
					<?php } ?>
				<?php } ?>
				
			</ul>
			
			<div id="topUser" class="right">
				    <div class="btn-group">
				    	
				    <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
				    <i class='icon-user '></i>
				    <?php echo $this->auth->getUserName(); ?>
				    <span class="caret"></span>
				    </a>
				    <ul class="dropdown-menu">
				    	<li><?php echo anchor(base_url()."index.php", "<i class='icon-eye-open '></i> View Site"); ?></li>
				    	<li><?php echo anchor("admin/changePassword", "<i class='icon-lock '></i> Change Password"); ?></li>
				    	<li><?php echo anchor("admin/logout", "<i class='icon-off '></i> Logout"); ?></li>
				    </ul>
				    </div>
				<!-- Welcome <?php echo $this->auth->getUserName(); ?> <?php echo anchor("admin/logout", "<i class='icon-off icon-white'></i> Logout"); ?> -->
			</div><!-- end topUser -->
			<br class="clear" />
		</div><!-- end top -->
		<div class="container-fluid">
			<div class="row-fluid">
				<div id="sidebar" class="span2">
					
					<?php $this->load->view("parts/sidebar"); ?>
				</div>
				<div id="main" class="span10">
					
					<?php echo $template['body']; ?>
				</div>
				
			</div>
		</div>
		</div>
	</body>
</html>