<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $template['title']; ?></title>
		<link href="<?php echo sharedCSS();?>/layout.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo cssPath();?>/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		
		<script>
			$(function(){
				 $('body').attr("class","js login");
    
			    /*
			      Add toggle switch after each checkbox.  If checked, then toggle the switch.
			    */
			     $('.checkbox').after(function(){
			       if ($(this).is(":checked")) {
			         return "<a href='#' class='toggle checked' ref='"+$(this).attr("id")+"'></a>";
			       }else{
			         return "<a href='#' class='toggle' ref='"+$(this).attr("id")+"'></a>";
			       }
			       
			       
			     });
			     
			     /*
			      When the toggle switch is clicked, check off / de-select the associated checkbox
			     */
			    $('.toggle').click(function(e) {
			       var checkboxID = $(this).attr("ref");
			       var checkbox = $('#'+checkboxID);
			
			       if (checkbox.is(":checked")) {
			         checkbox.removeAttr("checked");
			
			       }else{
			         checkbox.attr("checked","true");
			       }
			       $(this).toggleClass("checked");
			
			       e.preventDefault();
			
			    });
			    
			    
			    
			    $('form').submit(function(){
			    	$(this).hide();
			    	$('#loader').show();
			    })
				
			})
		</script>
	</head>
	<body class="login">
		<div id="login">
			
			<?php echo $template['body']; ?>
				
		</div>
	</body>
</html>