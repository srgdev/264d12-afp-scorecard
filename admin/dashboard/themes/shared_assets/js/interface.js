$(document).ready( function() {
			/**
			 * Maximize the real estate available to the portal contents
			 */
			
			
			stretch_sidebar();
			$(window).resize( stretch_sidebar );
			
			jQuery("form").validationEngine();
			$(".navInner").hide();
			$(".navInner.active").show();
			$(".navTitle").click(function(){
				$(this).toggleClass('active');
				navhead = $(this).siblings(".navInner").slideToggle();
			})
			
			$(".lightBox").fancybox({
				'titlePosition'		: 'inside',
				'transitionIn'		: 'none',
				'transitionOut'		: 'none'
			});
			
			// $('.datepicker').datepicker({
					// inline: true
			// });
			
			$('.datepicker').datetimepicker({ampm: true});
			$('.datepicker1').datetimepicker();
			$('.datepicker2').datetimepicker({timeFormat: '', showHour: false, showMinute: false, timeText: '', maxDate: new Date,  minDate: new Date(2013, 1, 19), dateFormat: 'm/d/yy'});
			
			$('.tip').tooltip()
			
			
		
			$('#nestable').nestable({
		        group: 1,
		        maxDepth : 2
		    })
		    
		    $('#nestable1').nestable({
		        group: 1,
		        maxDepth : 1
		    })
		    
		    $('.saveMenu').submit(function(){
		    	
		    	updateOutput($('#nestable').data('output', $('#nestable-output')));
		    	return false;
		    })
		    //.on('change', updateOutput);
		    //updateOutput($('#nestable').data('output', $('#nestable-output')));
	    
	    	$(".newEdit").live('click', function(){
	    		$('#tagline').modal('show')
	    	})
	    	
	    	$(".newEdit").live('click', function(){
	    		$('#tagline').modal('show')
	    	})
	    	
	    	$('.openerEdit').live('click', function(){
	    		$('#intro').modal('show')
	    	})
	    	
	    	$('.boxEdit').live('click', function(){
	    		var modal = '#' + $(this).attr('rel');
	    		$(modal).modal('show')
	    	})
	    	
	    	$('.contactEdit').live('click', function(){
	    		$('#contact').modal('show')
	    	})
	    	
	    	
	    	$('.editMenu').live('click', function(){
	    		$('#modalName').val($(this).attr('name'));
	    		$('#modalId').val($(this).attr('rel'));
	    		$('.modalLink').html($(this).attr('link'));
	    		$('#menuModel').modal('show')
	    	})
	    	
	    	$('iframe').iframeAutoHeight({debug: false, heightOffset: 30});
	    	
	    	
			$('#changeGraphSite').bind("change", function(){
				var sel = $(this).val();
				$('#graph').attr('src', sel);
				
			});
			
			$('#changePopSite').bind("change", function(){
				var sel = $(this).val();
				$('#popsites').attr('src', sel);
				
			});
			
			var pop = $('#changePopSite').val();
		    $('#popsites').attr('src', pop);
    	
    		var graph = $('#changeGraphSite').val();
		    $('#graph').attr('src', graph);
	});
		
		
		
	 
			
			
	function stretch_sidebar() {
		if( $(window).height() > $('#sidebar').height() ) {
			$( '#sidebar' ).height(
				$(window).height() - 40
			);
		}
		
		if( $('body').height() > $('#sidebar').height() ) {
			$( '#sidebar' ).height(
				$('body').height() - 40
			);
		}
	}

		
	function confirmMes(url, message){
		var retVal = confirm(message);
	    if( retVal == true ){
	      window.location = url;
		  return false;
	    }else{
		  return false;
	    }
	}
		
	
/*
  Plugin: iframe autoheight jQuery Plugin
  Version: 1.8.0
  Description: when the page loads set the height of an iframe based on the height of its contents
  see README: http://github.com/house9/jquery-iframe-auto-height 
*/
(function(a){a.fn.iframeAutoHeight=function(b){function d(a){c.debug&&c.debug===!0&&window.console&&console.log(a)}function e(b,c){d("Diagnostics from '"+c+"'");try{d("  "+a(b,window.top.document).contents().find("body")[0].scrollHeight+" for ...find('body')[0].scrollHeight"),d("  "+a(b.contentWindow.document).height()+" for ...contentWindow.document).height()"),d("  "+a(b.contentWindow.document.body).height()+" for ...contentWindow.document.body).height()")}catch(e){d("  unable to check in this state")}d("End diagnostics -> results vary by browser and when diagnostics are requested")}var c=a.extend({heightOffset:0,minHeight:0,callback:function(a){},animate:!1,debug:!1,diagnostics:!1,resetToMinHeight:!1,triggerFunctions:[],heightCalculationOverrides:[]},b);return d(c),this.each(function(){function g(a){var c=null;return jQuery.each(b,function(b,d){if(a[d])return c=f[d],!1}),c===null&&(c=f["default"]),c}function i(b){c.diagnostics&&e(b,"resizeHeight"),c.resetToMinHeight&&c.resetToMinHeight===!0&&(b.style.height=c.minHeight+"px");var f=a(b,window.top.document).contents().find("body"),h=g(a.browser),i=h(b,f,c,a.browser);d(i),i<c.minHeight&&(d("new height is less than minHeight"),i=c.minHeight+c.heightOffset),d("New Height: "+i),c.animate?a(b).animate({height:i+"px"},{duration:500}):b.style.height=i+"px",c.callback.apply(a(b),[{newFrameHeight:i}])}var b=["webkit","mozilla","msie","opera"],f=[];f["default"]=function(a,b,c,d){return b[0].scrollHeight+c.heightOffset},jQuery.each(b,function(a,b){f[b]=f["default"]}),jQuery.each(c.heightCalculationOverrides,function(a,b){f[b.browser]=b.calculation});var h=0;d(this),c.diagnostics&&e(this,"each iframe");if(c.triggerFunctions.length>0){d(c.triggerFunctions.length+" trigger Functions");for(var j=0;j<c.triggerFunctions.length;j++)c.triggerFunctions[j](i,this)}if(a.browser.safari||a.browser.opera){d("browser is webkit or opera"),a(this).load(function(){var a=0,b=this,e=function(){i(b)};h===0?a=500:b.style.height=c.minHeight+"px",d("load delay: "+a),setTimeout(e,a),h++});var k=a(this).attr("src");a(this).attr("src",""),a(this).attr("src",k)}else a(this).load(function(){i(this)})})}})(jQuery); 
 


