	<div class="cmsBlock">
		<h3>Site Stats</h3>
		<div id="widgetIframe"><iframe width="100%" height="250" src="<?php echo base_url(); ?>piwik/index.php?module=Widgetize&action=iframe&columns[]=nb_visits&moduleToWidgetize=VisitsSummary&actionToWidgetize=getEvolutionGraph&idSite=1&period=day&date=yesterday&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div>
	</div>
	
	<!-- <div id="widgetIframe"><iframe width="100%" height="100%" src="http://localhost:8888/NewCMS/piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageTitles&idSite=1&period=day&date=2012-09-05&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0"></iframe></div> -->
	<div class="cmsBlock">
		<h3>Popular Pages</h3>
	  <div id="widgetIframe"><iframe width="100%"  src="<?php echo base_url(); ?>piwik/index.php?module=Widgetize&action=iframe&moduleToWidgetize=Actions&actionToWidgetize=getPageTitles&idSite=1&period=day&date=2012-09-05&disableLink=1&widget=1" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" id="pagesCount" onload="autoIframe('pagesCount');"></iframe></div>
	
		<script type="text/javascript">
		   function autoIframe(frameId) {
		      try{
		     frame = document.getElementById(frameId);
		     innerDoc = (frame.contentDocument) ? frame.contentDocument : frame.contentWindow.document;
		     objToResize = (frame.style) ? frame.style : frame;
		     objToResize.height = innerDoc.body.scrollHeight + 10+'px';
		      }
		      catch(err){
		     window.status = err.message;
		      }
		   }
 	  </script>
   </div>
	<?php echo getMessage();  ?>
    
	<div class="cmsBlock">
	
		<h3>Recent System Events</h3>
		<div class="innerblock">
		<table  class="table table-striped">
			<thead>
			<tr>
				<th>Username</th>
				<th>Event</th>
				<th>Date/Time</th>
			</tr>
			</thead>
			<?php
			foreach($logData as $logLine){ ?> 
				<tr class="popParent"  >
					<td><?php echo $logLine->user ?></td>
					<td class="pop" data-content="<?php echo $logLine->desc ?>" rel="popover"><?php echo $logLine->action ?></td>
					<td><?php echo date_to_human($logLine->date) ?></td>
				</tr>	
			<?php } ?>
		
		</table>
	</div></div>
