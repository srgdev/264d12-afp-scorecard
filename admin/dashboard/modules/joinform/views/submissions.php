<h1 class="left" >Join Form Submissions</h1>
<?php 
    $attributes = array('class' => 'right');
    echo form_open('joinform/download', $attributes);
?>
<input type="hidden" name="start" value="<?php echo $start ?>" />
<input type="hidden" name="end" value="<?php echo $end ?>" />
<input type="submit" name="mysubmit" value="Download" class="btn" />
</form>
<br class="clear" />

<h4>Date Range</h4><br />
<?php 
    $attributes = array('class' => '');
    echo form_open('joinform/search', $attributes);
?>
Start: <input type="text" name="start" class=" input-medium search-query datepicker2" value="<?php echo $start ?>" /> End: <input type="text" name="end" class="input-medium search-query datepicker2" value="<?php echo $end ?>" /> <input type="submit" name="mysubmit" value="Search" class="btn" />
</form>

Total Submissions: <?php echo $total ?>
<br /><br />
<?php echo $table; ?>
