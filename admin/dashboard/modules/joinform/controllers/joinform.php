<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class joinform extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "joinform";
    }
    

    
    public function index(){
        $date = date('n/d/Y');
        $edit = ''; 
        $this->crud->use_table('afp_join');
        $rows =  $this->crud->retrieve(array('date' => $date), '', 0, 0, array('id' => 'DESC'));
        $table = $this->buildtable->build(array('Date', 'First Name', 'Last Name', 'Email', 'Zip'), array('date','first_name', 'last_name', 'email', 'zip'), $rows,$edit); 
        $this->template->set('table', $table);
        $this->template->set('start', $date);
        $this->template->set('end', $date);
        $this->template->set('total', count($rows)); 
        $this->template->title($this->cat)->build('submissions');
    }
    

    public function search(){
        $start = $_REQUEST['start'];
        $end = $_REQUEST['end'];
        $edit = '';
        $query = $this->db->query('SELECT * FROM afp_join WHERE date BETWEEN "'.$start.'" AND "'.$end.'" ');
        $table = $this->buildtable->build(array('Date', 'First Name', 'Last Name', 'Email', 'Zip'), array('date','first_name', 'last_name', 'email', 'zip'), $query->result(),$edit); 
        $this->template->set('table', $table);
        $this->template->set('start', $start);
        $this->template->set('end', $end); 
        $this->template->set('total', count($query->result()));
        $this->template->title($this->cat)->build('submissions');
        
    }
    
    public function download(){
       $start = $_REQUEST['start'];
       $end = $_REQUEST['end']; 
       $query = $this->db->query('SELECT * FROM afp_join WHERE date BETWEEN "'.$start.'" AND "'.$end.'" ');
       $title = 'scorecard_join_submissions';
       $this->toexcel->to_excel('afp_join',$title,'', $query->result());
    }
    
    
    
    
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Join Form', 'Mange Submissions', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}