<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Key Votes Model
 * Copyright (c) 2012, Stoneridge Group
 * All rights reserved.
 *
 * Changes on December 2015: Switched to Sunlight Foundation's Congress API v3
 * @copyright 2015 Stoneridge Group, Calvin deClaisse-Walford
 * @since 1.0.0
 */
class votes extends CI_Model
{

    /**
     * Sunlight API location
     */
    private $apiUrl = 'http://congress.api.sunlightfoundation.com/votes';

    /**
     * Current Session
     */
    private $currentSession;

    /**
     * API key
     */
    private $apiKey;
  
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // Assign current session and api key
        $this->currentSession = $this->config->item('CONG_SESSION');
        $this->apiKey = $this->config->item('SUNLIGHT_LABS_API_KEY');
    }
    
    /**
     * Add a vote based on congressional session, roll call, position, chamber, and so on
     * @param string|int $congnum
     * @param string|int session
     * @param string|int roll call (bill's id number)
     * @param string chamber (house or senate)
     * @param string issue
     * @param string description
     * @param string position summary
     * @param string vote title
     */
    public function addVote($congNum, $session, $roll_call, $chamber, $position,$issue, $description, $position_summary, $vote_title )
    {



        // Parse session as the first or second year of the in session years, since Sunlight doesn't use sessions as a way
        // to separate votes
        $this->session = $congNum;



        $ch = curl_init("https://api.propublica.org/congress/v1/".$this->session."/".$chamber."/sessions/".$session."/votes/".$roll_call.".json");


        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'X-API-Key: Q64HavAgWt1Ps5jSoUNKe9UgiMLkn3SI8zTFn4WP'
            )
        );

        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $data = json_decode($response);
        $results = $data->results->votes->vote;
        $data = $data->results->votes->vote;



        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        // If we have results, go ahead and start working with the data
        if($results){
            // Set up a data array to put into the database
            $data = array();
            $data['congress'] = $results->congress;
            $data['session'] = $session;
            $data['roll_call'] = $roll_call;
            $data['chamber'] = ucfirst($results->chamber);
            $data['result'] = $results->result;
            $data['vote_date'] = substr($results->date, 0, 10);
            $data['question'] = $results->question;
            $data['description'] = $description;
            $data['position_summary'] = $position_summary;
            $data['position'] = $position;
            $data['issue'] = $issue;
            $data['vote_title'] = $vote_title;
            //extract the bill number
            if(isset($results->bill_id)){
                $data['bill_id'] = substr($results->bill_id, 0, strpos($results->bill_id, '-'));
            }
            // Check if this exact vote already exists
            $this->crud->use_table('afp_key_votes');
            $matching_keyvotes =  $this->crud->retrieve(array('roll_call' => $data['roll_call'], 'chamber' => $data['chamber'], 'session' => $data['session'], 'congress' => $data['congress']), 'row', 0, 0, array('id' => 'DESC'));
            // If it exists, use that ID for updating votes
            if(count($matching_keyvotes) > 0 ){
                $lastID = $matching_keyvotes->id;

            // Else, we'll create the new entry
            } else {
                $lastID = $this->crud->create($data);
            }
            // Request positions for this roll call
//            $positions_request = $this->apiUrl . '?chamber='.$chamber.'&number='.$roll_call.'&year='.$year.'&congress='.$congNum.'&fields=voters&apikey='.$this->apiKey;
//            $positions_response = file_get_contents($positions_request);
//            $vote_positions = json_decode($positions_response, true);
            $positions = $results->positions;
            //record each vote
            if($positions && count($positions) > 0) {
                foreach($positions as $pos){

                    $v = array();
                    $v['voteID'] = $lastID;
                    $v['memID'] = $pos->member_id;
                    $v['congress'] = $congNum;

                    switch($pos->vote_position){
                        case 'Yes':
                            $v['position'] = 'Yes';
                            break;
                        case 'No':
                            $v['position'] = 'No';
                            break;
                        case 'Present':
                            $v['position'] = 'Present';
                            break;
                        case 'Not Voting':
                            $v['position'] = 'Not Voting';
                            break;
                    }

                    $this->crud->use_table('afp_votes');
                    $check =  $this->crud->retrieve(array('voteID' => $lastID, 'memID' => $pos->member_id , 'congress' => $congNum), 'row', 0, 0, array('id' => 'DESC'));

                    if(count($check) == 0 ) {
                        $this->crud->create($v);
                    }
                }
            }

            // Clear API cache
            $this->cacheClear();

            // Return ID of bill to edit
            return $lastID;

        } else {

            // Return nothing, invalid bill
            return false;
        }
    }

    /**
     * Get members by congress, chamber, and particular vote id
     * @param string|int congress
     * @param string chamber (house/senate)
     * @param string|int voteid
     */
    public function getMembers($congress, $chamber, $voteid)
    {

      // Check that members exist
      $this->crud->use_table('afp_votes');
      $check = $this->crud->retrieve(array('congress' => $congress, 'voteID' => $voteid), '', 0, 0, array('id' => 'DESC'));

      if(count($check) > 0){

        // Get all members for this vote
        $query = $this->db->query('SELECT *
          FROM afp_members, afp_votes  
          WHERE afp_members.congress = "'.$congress.'"
          AND afp_members.chamber = "'.$chamber.'"
          AND afp_votes.congress = "'.$congress.'"
          AND afp_votes.voteID = "'.$voteid.'"
          AND afp_votes.memID = afp_members.congID
        ');

      } else {

        // Simply get all members
        $query = $this->db->query('SELECT *
          FROM afp_members  
          WHERE afp_members.congress = "'.$congress.'"
          AND afp_members.chamber = "'.$chamber.'"
        ');

      }

      // Return query results
      return $query->result();

    }

    /**
     * Clear the API cache
     */
    public function cacheClear()
    {
      
        $cache_path = $_SERVER['DOCUMENT_ROOT']."/api/api_1_1/cache/";

        foreach(glob($cache_path . '*.*') as $v) {
           unlink($v);
        }
    }
  
}