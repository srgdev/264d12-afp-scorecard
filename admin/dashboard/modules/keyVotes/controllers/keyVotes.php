<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class keyVotes extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "keyVotes";
    }
    

    
    public function index(){
       $this->crud->use_table('afp_key_votes');
       $edits = $this->buildtable->buildEdits("keyVotes/editVote/##id", "keyVotes/delVote/##id" );
       $row =  $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
       $table = $this->buildtable->build(array('Roll Call', 'Chamber', 'Congress', 'Issue'), array('roll_call','chamber','congress','issue'), $row, $edits);
       $this->template->set('table', $table);
       $this->template->title("Key Votes")->build('listVotes');
    }
    
    public function addVote(){
        $this->crud->use_table('afp_sessions');
        $sessions = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('sessions', $sessions);
        
        $this->crud->use_table('afp_issues');
        $issues = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('issues', $issues); 
        
        $this->template->title("Add Key Votes")->build('addVote');
    }
    
    
    
    public function editVote(){
        $vote = $this->uri->segment('3');
        $this->crud->use_table('afp_key_votes');
        $votes = $this->crud->retrieve(array('id' => $vote), '', 0, 0, array('id' => 'DESC'));
        $this->template->set('votes', $votes);
        
        
        $this->crud->use_table('afp_sessions');
        $sessions = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('sessions', $sessions);
        
        $this->crud->use_table('afp_issues');
        $issues = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('issues', $issues); 
        
        $this->template->title("Add Key Votes")->build('editVote');
    }
    
    public function insertVote(){
        $congNum = $_REQUEST['congNum'];
        $session = $_REQUEST['sessionNum'];
        $roll_call = $_REQUEST['roll_call'];
        $position = $_REQUEST['position'];
        $chamber = $_REQUEST['chamber'];
        $issue = $_REQUEST['issue'];
        $description = $_REQUEST['description'];
        $position_summary = $_REQUEST['position_summary'];
        $vote_title = $_REQUEST['vote_title'];
        $this->load->model('votes');
        $vote = $this->votes->addVote($congNum, $session, $roll_call,$chamber, $position,$issue, $description, $position_summary, $vote_title );
        if($vote){
            setMessage('success', 'Key Vote Was Added');
            redirect("keyVotes/editVote/".$vote, 'location');
        } else {
            setMessage('error', 'Failed to add vote.');
            redirect("keyVotes/addVote/");
        }
    }
    
    public function updateVote(){
        $id = $_REQUEST['id'];
        $data['position'] = $_REQUEST['position'];
        $data['issue'] = $_REQUEST['issue'];
        $data['description'] = $_REQUEST['description'];
        $data['position_summary'] = $_REQUEST['position_summary'];
        $data['vote_title'] = $_REQUEST['vote_title'];
        $data['question'] = $_REQUEST['question'];
        $this->crud->use_table('afp_key_votes');
        $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
        setMessage('success', 'Key Vote Was updated '); 
        redirect("keyVotes/editVote/".$id, 'location');
    }

  
    public function delVote(){
        $id = $this->uri->segment(3);
        $this->crud->use_table('afp_key_votes');
        $this->crud->delete(array('id' => $id), 0, 0, array('id' => 'DESC'));
        $this->crud->use_table('afp_votes');
        $this->crud->delete(array('voteID' => $id), 0, 0, array('id' => 'DESC'));
        setMessage('success', 'Key Vote Was Deleted ');
        redirect("keyVotes", 'location');
    }
    
    public function addCustomVote(){
         $this->crud->use_table('afp_sessions');
        $sessions = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('sessions', $sessions);
        
        $this->crud->use_table('afp_issues');
        $issues = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $this->template->set('issues', $issues); 
        
        $this->template->title("Add Custom Votes")->build('addCustomVote');
    }
    
    
    public function insertCustomVote(){
        $data = array(
            'congress' => $_REQUEST['congNum'],
            'roll_call' => 'AFP',
            'position' => $_REQUEST['position'],
            'chamber' => $_REQUEST['chamber'],
            'issue' => $_REQUEST['issue'],
            'description' => $_REQUEST['description'],
            'position_summary' => $_REQUEST['position_summary'],
            'vote_title' => $_REQUEST['vote_title'],
            'custom' => 'yes'
        );
        $this->crud->use_table('afp_key_votes');
        $vote = $this->crud->create($data);
        setMessage('success', 'Custom Vote Was Added'); 
        redirect("keyVotes/editVote/".$vote, 'location');
    }
    
    public function updateMemberVotes(){
        $id = $_REQUEST['voteID'];
        $chamber = $_REQUEST['chamber'];
        $congress = $_REQUEST['congress'];
        
        $this->crud->use_table('afp_members');
        $members = $this->crud->retrieve(array('chamber' => $chamber, 'congress' => $congress), '', 0, 0, array('id' => 'DESC'));
        
        $this->crud->use_table('afp_votes');
        foreach($members as $mem){
            $memID = $mem->congID;
            $check = $this->crud->retrieve(array('memID' => $memID, 'voteID' => $id), '', 0, 0, array('id' => 'DESC'));
            if(count($check) > 0){
                 $this->crud->update(array('memID' => $memID, 'voteID' => $id, 'congress' => $congress),array('memID' => $memID, 'voteID' => $id, 'congress' => $congress, 'position' => $_REQUEST[$memID]), 0, 0, array('id' => 'DESC'));
            }else{
                $this->crud->create(array('memID' => $memID, 'voteID' => $id, 'congress' => $congress, 'position' => $_REQUEST[$memID]));
            }
        }
        setMessage('success', 'Member votes Saved'); 
        redirect("keyVotes/editVote/".$id, 'location');
    }
    
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Key Votes', 'Key Votes', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Home Page', 'Home Page', $this->cat.'/home', 'home', "home", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Menu', 'Menu', $this->cat.'/menu', 'th-list', "menu", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Quick Links', 'Quick Links', $this->cat.'/links', 'share', "links", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'News', 'News', $this->cat.'/news', 'tasks', "news", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'FAQ', 'FAQ', $this->cat.'/faq', 'question-sign', "faq", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Settings', 'Settings', $this->cat.'/settings', 'cog', "settings", "2", '3', $this->cat);
//         
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}