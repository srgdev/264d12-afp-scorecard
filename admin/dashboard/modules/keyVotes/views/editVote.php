<h1 class="left"><span class="ifont">K </span>Edit Key Vote <?php echo anchor('keyVotes/addVote', "<i class='icon-plus'></i> Add Vote", 'class="btn "'); ?></h1>
<br class="clear" />

<?php echo getMessage(); ?>

<?php foreach($votes as $vote){ ?>
<?php 
    
    $attributes = array('class' => 'form-horizontal well', 'id' => 'addUser');
    echo form_open('keyVotes/updateVote', $attributes);
?>  
<div class="container-fluid">
    <div class="row-fluid">
        <input type="hidden" name="id" id="id" value="<?php echo $this->uri->segment('3') ?>" />
        <div class="control-group">
            <label class="control-label" for="congNum">Congress Number</label>
            <div class="controls">
               <input type="text" class="input-xlarge" id="session" name="session" value="" disabled placeholder="<?php echo $vote->congress ?>">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="session">Session Number</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="session" name="session" value="" disabled placeholder="<?php echo $vote->session ?>">
                Session for the above congress (year 1 = 1, year 2 = 2)
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="chamber">Chamber</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="session" name="session" value="" disabled placeholder="<?php echo $vote->chamber ?>">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="roll_call">Roll Call Number</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="roll_call" name="roll_call" disabled placeholder="<?php echo $vote->roll_call; ?>">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="position">AFP Position</label>
            <div class="controls">
                <select name="position" id="position" class="input-xlarge validate[required]">
                    <option value="">Position</option>
                    <option value="yes" <?php if($vote->position == 'yes'){echo 'selected="selected"';} ?>>Yes</option>
                    <option value="no" <?php if($vote->position == 'no'){echo 'selected="selected"';} ?>>No</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="issue">Issue (optional)</label>
            <div class="controls">
                <select name="issue" id="issue" class="input-xlarge">
                    <option value="">optional</option>
                    <?php foreach($issues as $issue){ ?>
                        <option value="<?php echo $issue->issue_code ?>" <?php if($vote->issue == $issue->issue_code){echo 'selected="selected"';} ?>><?php echo $issue->issue_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="vote_title">Vote Title</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="vote_title" name="vote_title" value="<?php echo $vote->vote_title ?>">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="question">Question</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="question" name="question" value="<?php echo $vote->question ?>">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="description">Description</label>
            <div class="controls">
                <textarea class="input-xlarge" id="description" name="description"><?php echo stripcslashes($vote->description) ?></textarea>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="position_summary">Position Text</label>
            <div class="controls">
                <textarea class="input-xlarge" id="position_summary" name="position_summary"><?php echo stripcslashes($vote->position_summary) ?></textarea>
            </div>
        </div>
        <br />
        <?php echo anchor("keyVotes", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
        <input type="submit" name="mysubmit" value="Save Vote Information" class="btn btn-primary" />
        </form>
        <?php 
            
            $attributes = array('class' => 'form-horizontal well', 'id' => 'addUser');
            echo form_open('keyVotes/updateMemberVotes', $attributes);
        ?>  
        <input type="hidden" name="voteID" value="<?php echo  $vote->id ?>" />
        <input type="hidden" name="chamber" value="<?php echo  $vote->chamber ?>" />
        <input type="hidden" name="congress" value="<?php echo  $vote->congress ?>" />
        <?php if($vote->custom == 'yes'){?>
            <br /><br /><br /><br />
            <h2>Member Votes</h2>
            <br /><br />
            <?php $this->load->model('votes');
                  $members = $this->votes->getMembers($vote->congress, $vote->chamber, $vote->id);
             ?>
             <table rules="all" width="340px" cellpadding="4" cellspacing="5">
                 <tr>
                 	<th>Member</th>
                 	<th>Vote</th>
                 </tr>
             <?php foreach($members as $member){ ?>
                 <tr>
                 	<td><?php echo $member->fName ?> <?php echo $member->lName ?></td>
                 	<td>
                 	    <?php if(isset($member->position)){$position = $member->position; }else{ $position = 'No'; } ?>
                 	    <input type="radio" name="<?php echo $member->congID ?>" value="No" <?php if($position == 'No'){echo 'checked';} ?>> No
                        <input type="radio" name="<?php echo $member->congID ?>" value="Yes" <?php if($position == 'Yes'){echo 'checked';} ?> > Yes
                        <input type="radio" name="<?php echo $member->congID ?>" value="Not Voting" <?php if($position == 'Not Voting'){echo 'checked';} ?> > Not Voting
                 	</td>
                 </tr>
             <?php } ?>
             </table>
             <input type="submit" name="mysubmit" value="Save Member Votes" class="btn btn-primary" />
        <?php } ?>
    </div>            
</div>

<?php } ?>