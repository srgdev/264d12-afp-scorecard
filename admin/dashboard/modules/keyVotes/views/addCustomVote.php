<h1 class="left"><span class="ifont">K </span>Add Key Vote</h1>
<br class="clear" />

<?php 
    
    $attributes = array('class' => 'form-horizontal well', 'id' => 'addUser');
    echo form_open('keyVotes/insertCustomVote', $attributes);
?>  
<div class="container-fluid">
    <div class="row-fluid">

        <div class="control-group">
            <label class="control-label" for="congNum">Congress Number</label>
            <div class="controls">
                <select name="congNum" id="congNum" class="input-xlarge validate[required]">
                    <option value="">Session of Congress</option>
                    <?php foreach($sessions as $session){ ?>
                        <option value="<?php echo $session->congress ?>"><?php echo $session->congress ?> - ( <?php echo $session->years ?> )</option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="chamber">Chamber</label>
            <div class="controls">
                <select name="chamber" id="chamber" class="input-xlarge validate[required]">
                    <option value="">Chamber</option>
                    <option value="House">House</option>
                    <option value="Senate">Senate</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="position">AFP Position</label>
            <div class="controls">
                <select name="position" id="position" class="input-xlarge validate[required]">
                    <option value="">Position</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="issue">Issue (optional)</label>
            <div class="controls">
                <select name="issue" id="issue" class="input-xlarge">
                	<option value="">optional</option>
                	<?php foreach($issues as $issue){ ?>
                        <option value="<?php echo $issue->issue_code ?>"><?php echo $issue->issue_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="vote_title">Vote Title</label>
            <div class="controls">
                <input type="text" class="input-xlarge" id="vote_title" name="vote_title" value="">
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="description">Description</label>
            <div class="controls">
                <textarea class="input-xlarge" id="description" name="description"></textarea>
            </div>
        </div>
        
        <div class="control-group">
            <label class="control-label" for="position_summary">Position Text</label>
            <div class="controls">
                <textarea class="input-xlarge" id="position_summary" name="position_summary"></textarea>
            </div>
        </div>
        <br />
        <?php echo anchor("keyVotes", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
        <input type="submit" name="mysubmit" value="Add Vote" class="btn btn-primary" />
    </div>            
</div>