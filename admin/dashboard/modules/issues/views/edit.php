<h1>Edit Issue</h1>

<?php echo getMessage(); ?> 
<?php 
    
    $attributes = array('class' => 'form-horizontal well');
    echo form_open('issues/update', $attributes);
    foreach($issues as $issue) {
?>  

<div class="container-fluid">
        <div class="row-fluid">
            <div id="mainLeft" class="span12">
                <input type="hidden" name="id" id="id" value="<?php echo $issue->id; ?>" />
                <div class="control-group">
                    <label class="control-label" for="issue">Issue</label>
                    <div class="controls">
                        <span class="input-xlarge uneditable-input"><?php echo $issue->issue_name; ?></span>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="description">Description</label>
                    <div class="controls">
                        <textarea name="description" class="input-xlarge" style="height:250px;"><?php echo $issue->description ?></textarea>
                    </div>
                </div>
                
                
                <input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
    <?php echo anchor("adminusers/allusers", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
            </div>
            
            
          
    
    </div>
    </div>

    
    
    
<?php   

    }
?>

