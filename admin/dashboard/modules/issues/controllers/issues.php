<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class issues extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "issues";
    }
    

    
    public function index(){
        $edit = $this->buildtable->buildEdits("issues/edit/##id", "issues/" ); 
        $this->crud->use_table('afp_issues');
        $rows = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
        $table = $this->buildtable->build(array('Issue'), array('issue_name'), $rows,$edit); 
        $this->template->set('table', $table); 
        $this->template->title($this->cat)->build('listIssues.php');
    }
    

    
    public function edit(){
        $id = $this->uri->segment('3');
        $this->crud->use_table('afp_issues');
        $issues = $this->crud->retrieve(array('id' => $id), '', 0, 0, array('id' => 'DESC'));
        $this->template->set('issues', $issues); 
        $this->template->title(ucfirst($this->cat).'/ Edit Issue')->build('edit');
    }

    public function update(){
        $id = $_REQUEST['id'];
        $data = array(
            'id' => $id,
            'description' => $_REQUEST['description']
        );
        $this->crud->use_table('afp_issues');
        $this->crud->update(array('id' => $id),$data, 0, 0, array('id' => 'DESC'));
        setMessage('success', 'Issue description was saved');
        redirect('/issues/edit/'.$id, 'location');
          
    }
    
    
    
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Issues', 'Mange issues', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}