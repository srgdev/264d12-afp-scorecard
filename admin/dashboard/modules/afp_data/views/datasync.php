<h1>Data Sync</h1>

<script>
    $(function(){
        var urler = ' + <?php echo base_url().index_page()."/" ?> + ';
        $('.scoreBtn').click(function(){
            $(this).button('loading')
            var cong = $('.sessions').val();
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/afp_data/calcScores/'+cong,
               success: function(data){
                   $('.scoreBtn').button('reset');
               }
            });
        });
        
        
        $('.memBtn').click(function(){
            $(this).button('loading')
             var cong = $('.sessions').val();
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/afp_data/syncMem/calcScores/'+cong,
               success: function(data){
                   $('.memBtn').button('reset');
               }
            });
        });
        
        $('.lifeBtn').click(function(){
            $(this).button('loading')
             var cong = $('.sessions').val();
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/afp_data/calcLifetime/calcScores/'+cong,
               success: function(data){
                   $('.lifeBtn').button('reset');
               }
            });
        });
        
        $('.issueBtn').click(function(){
            $(this).button('loading')
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/afp_data/calcIssue',
               success: function(data){
                   $('.issueBtn').button('reset');
               }
            });
        });
        
    })
</script>
<h3>Congress to Sync</h3>
<br />
<select name="sessions" class="sessions">
    <option value="116">116</option>
    <option value="115">115</option>
    <option value="114">114</option>
    <option value="113">113</option>
    <option value="112">112</option>
    <option value="111">111</option>
    <option value="110">110</option>
</select>
<br /><br />
<h3>Sync Members Database</h3>
<br />
<button type="button" class="btn btn-primary loading memBtn" data-loading-text="Loading...">Sync Members</button>
<br /><br />
<h3>Calculate Scores</h3>
<br />
<button type="button" class="btn btn-primary loading scoreBtn" data-loading-text="Loading...">Calculate Scores</button>
<br /><br />
<h3>Calculate Life Time Scores</h3>
<br />
<button type="button" class="btn btn-primary loading lifeBtn" data-loading-text="Loading...">Calculate Life Time Scores</button>
<!-- <br /><br />
<h3>Calculate Issue Average Scores</h3>
<br />
<button type="button" class="btn btn-primary loading issueBtn" data-loading-text="Loading...">Calculate Issue Averages</button> -->
<!-- <br /><br />
<h3>Sync Committees Database</h3>
<br />
<button type="button" class="btn btn-primary loading comBtn" data-loading-text="Loading...">Sync Committees</button>
<br /><br />
<h3>Sync Legislation Database</h3>
<br />
<button type="button" class="btn btn-primary loading legBtn" data-loading-text="Loading...">Sync Legislation</button>
<br /><br />
<h3>Sync User Notifications</h3>
<br />
<button type="button" class="btn btn-primary loading notBtn" data-loading-text="Loading...">Sync Notifications</button> -->