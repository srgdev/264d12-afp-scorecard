<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class lifetime extends CI_Model
{
  
  function __construct(){
    parent::__construct();
    $this->congress = "";
  }
  
  
 function calcLifetime_old($congress){

     $this->congress = $congress;
     echo $this->congress;
     //get all uniqe members
     $this->crud->use_table('afp_members');
     $query = $this->db->query('SELECT DISTINCT congID FROM afp_members');
     $members =  $query->result();

     //get all sessions
     $this->crud->use_table('afp_sessions');
     $sessions = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));

     //loop each member
     foreach($members as $member){
         $memID = $member->congID;
         echo " ".$member->congID;
         $total = 0;
         $budgetTotal = 0;
         $energyTotal = 0;
         $healthTotal = 0;
         $taxesTotal = 0;
         $laborTotal = 0;
         $limitedTotal = 0;
         $firstTotal = 0;

         $score = "-";
         $budget = "-";
         $energy = "-";
         $health = "-";
         $taxes = "-";
         $labor = "-";
         $banking = "-";
         $property = "-";
         $technology = "-";
         $limited = "-";
         $first = "-";

         $lifetime = "-";

         foreach($sessions as $session){
             $ses = $session->congress;
             $this->crud->use_table('afp_'.$ses.'_scores');
             $check  = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
             if(count($check) > 0){

                 if($check->score != '-'){
                    $total++;
                    $score = $score + $check->score;
                 }

                 if($check->budget != '-'){ $budgetTotal++; $budget = $budget + $check->budget;}
                 if($check->energy != '-'){ $energyTotal++; $energy = $energy + $check->energy;}
                 if($check->health != '-'){ $healthTotal++; $health = $health + $check->health;}
                 if($check->taxes != '-'){ $taxesTotal++; $taxes = $taxes + $check->taxes;}
                 if($check->labor != '-'){ $laborTotal++; $labor = $labor + $check->labor;}
                 if($check->limited != '-'){$limitedTotal++; $limited = $limited + $check->limited;}
                 if($check->first != '-'){$firstTotal++; $first = $first + $check->first;}
             }
         }

         if($total > 0){  $lifetime = round($score/$total);  }
         if($budgetTotal > 0){  $budget = round($budget/$budgetTotal);  }
         if($energyTotal > 0){  $energy = round($energy/$energyTotal);  }
         if($healthTotal > 0){  $health = round($health/$healthTotal);  }
         if($taxesTotal > 0){  $taxes = round($taxes/$taxesTotal);  }
         if($laborTotal > 0){  $labor = round($labor/$laborTotal);  }
         if($limitedTotal > 0){  $limited = round($limited/$limitedTotal);  }
         if($firstTotal > 0){  $first = round($first/$firstTotal);  }


         $this->crud->use_table('afp_lifetime_scores');
         $data = array(
            'memID' => $memID,
            'lifeScore' => $lifetime,
            'budget' => $budget,
            'energy' => $energy,
            'health' => $health,
            'taxes' => $taxes,
            'labor' => $labor,
            'limited' => $limited,
            'first' => $first
         );

         $check  = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
         if(count($check)>0){
             $this->crud->update(array('memID' => $memID),$data, 0, 0, array('id' => 'DESC'));
         }else{
             $this->crud->create($data);
         }
     }
  $this->cacheClear();
  }



function calcLifetime($congress){

    $this->congress = $congress;
    echo $this->congress;
    //get all uniqe members

    $this->crud->use_table('afp_members');
    $members = $this->crud->retrieve(array(), '', 0, 0, array('id' => 'ASC'));

    //get all sessions
    $this->crud->use_table('afp_sessions');
    $sessions = $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));

    //loop each member
    foreach($members as $member){
        $memID = $member->congID;
        echo " ".$member->congID;
        $chamber = $member->chamber;

        $total = 0;
        //set starting values
        $budgetTotal = 0;
        $energyTotal = 0;
        $healthTotal = 0;
        $taxesTotal = 0;
        $laborTotal = 0;
        $limitedTotal = 0;
        $firstTotal = 0;

        $correct = 0;
        $budgetCorrect = 0;
        $energyCorrect = 0;
        $healthCorrect = 0;
        $taxesCorrect = 0;
        $laborCorrect = 0;
        $limitedCorrect = 0;
        $firstCorrect = 0;

        $wrong = 0;
        $budgetWrong = 0;
        $energyWrong = 0;
        $healthWrong = 0;
        $taxesWrong = 0;
        $laborWrong = 0;
        $limitedWrong = 0;
        $firstWrong = 0;

        $nv = 0;
        $budgetNv = 0;
        $energyNv = 0;
        $healthNv = 0;
        $taxesNv = 0;
        $laborNv = 0;
        $limitedNv = 0;
        $firstNv = 0;

        $score = "-";
        $budgetScore = "-";
        $energyScore = "-";
        $healthScore = "-";
        $taxesScore = "-";
        $laborScore = "-";
        $limitedScore = "-";
        $firstScore = "-";

        foreach($sessions as $session){
            $ses = $session->congress;
            $this->crud->use_table('afp_key_votes');

            //pull House votes
            $houseVotes = $this->crud->retrieve(array('congress' => $ses, 'chamber' => 'house'), '', 0, 0, array('id' => 'DESC'));

            //pull Senate votes
            $senateVotes = $this->crud->retrieve(array('congress' => $ses, 'chamber' => 'senate'), '', 0, 0, array('id' => 'DESC'));

            //loop chamber
            $loopArray = ($chamber == 'senate') ? $senateVotes : $houseVotes;

            //if the are chamber votes then calc scores for the member
            $looptotal = count($loopArray);
            if($looptotal > 0) {
                //Loop the chamber
                foreach ($loopArray as $vote) {
                    $afpPosition = strtolower($vote->position);

                    $voteID = $vote->id;
                    $issue = $vote->issue;

                    //Pull the position for the member
                    $this->crud->use_table('afp_votes');
                    $membersVote = $this->crud->retrieve(array('congress' => $ses, 'memID' => $memID, 'voteID' => $voteID), 'row', 0, 0, array('id' => 'DESC'));

                    if (count($membersVote) > 0) {

                        $memPosition = strtolower($membersVote->position);
                        // echo $memPosition."<br/>";
                        // echo $afpPosition."<br/>";
                        // echo "<br/>";
                        // echo "<br/>";
                        //calc right or wrong
                        if ($memPosition == 'not voting' || $memPosition == 'speaker') {
                            $nv++;
                            switch ($issue) {
                                case 'budget':
                                    $budgetNv++;
                                    break;
                                case 'energy':
                                    $energyNv++;
                                    break;
                                case 'health':
                                    $healthNv++;
                                    break;
                                case 'taxes':
                                    $taxesNv++;
                                    break;
                                case 'labor':
                                    $laborNv++;
                                    break;
                                case 'limited':
                                    $limitedNv++;
                                    break;
                                case 'first':
                                    $firstNv++;
                                    break;
                            }
                        } elseif ($memPosition == $afpPosition) {
                            $correct++;
                            switch ($issue) {
                                case 'budget':
                                    $budgetCorrect++;
                                    break;
                                case 'energy':
                                    $energyCorrect++;
                                    break;
                                case 'health':
                                    $healthCorrect++;
                                    break;
                                case 'taxes':
                                    $taxesCorrect++;
                                    break;
                                case 'labor':
                                    $laborCorrect++;
                                    break;
                                case 'limited':
                                    $limitedCorrect++;
                                    break;
                                case 'first':
                                    $firstCorrect++;
                                    break;
                            }
                        } else {
                            $wrong++;
                            switch ($issue) {
                                case 'budget':
                                    $budgetWrong++;
                                    break;
                                case 'energy':
                                    $energyWrong++;
                                    break;
                                case 'health':
                                    $healthWrong++;
                                    break;
                                case 'taxes':
                                    $taxesWrong++;
                                    break;
                                case 'labor':
                                    $laborWrong++;
                                    break;
                                case 'limited':
                                    $limitedWrong++;
                                    break;
                                case 'first':
                                    $firstWrong++;
                                    break;
                            }

                        }//end memposition else

                        switch ($issue) {
                            case 'budget':
                                $budgetTotal++;
                                break;
                            case 'energy':
                                $energyTotal++;
                                break;
                            case 'health':
                                $healthTotal++;
                                break;
                            case 'taxes':
                                $taxesTotal++;
                                break;
                            case 'labor':
                                $laborTotal++;
                                break;
                            case 'limited':
                                $limitedTotal++;
                                break;
                            case 'first':
                                $firstTotal++;
                                break;
                        }

                    }
                }
            }

        }

        $total = ($wrong + $correct);

        $budgetTotal = ($budgetWrong + $budgetCorrect);

        $energyTotal = ($energyWrong + $energyCorrect);

        $healthTotal = ($healthWrong + $healthCorrect);

        $taxesTotal = ($taxesWrong + $taxesCorrect);

        $laborTotal = ($laborWrong + $laborCorrect);

        $limitedTotal = ($limitedWrong + $limitedCorrect);

        $firstTotal = ($firstWrong + $firstCorrect);

        // //finally calc the scores
        if($total > 0){
            $lifetime = ($correct/$total) * 100;
            $lifetime = round($lifetime);
        }

         echo $total;
         echo "<br/>";
         echo $correct;
         echo "<br/>";
         echo $lifetime;
//
        if($budgetTotal > 0){
            $budgetScore = ($budgetCorrect/$budgetTotal) * 100;
            $budgetScore = round($budgetScore);
        }

        if($energyTotal > 0){
            $energyScore = ($energyCorrect/$energyTotal) * 100;
            $energyScore = round($energyScore);
        }

        if($healthTotal > 0){
            $healthScore = ($healthCorrect/$healthTotal) * 100;
            $healthScore = round($healthScore);
        }

        if($taxesTotal > 0){
            $taxesScore = ($taxesCorrect/$taxesTotal) * 100;
            $taxesScore = round($taxesScore);
        }

        if($laborTotal > 0){
            $laborScore = ($laborCorrect/$laborTotal) * 100;
            $laborScore = round($laborScore);
        }

        if($limitedTotal > 0){
            $limitedScore = ($limitedCorrect/$limitedTotal) * 100;
            $limitedScore = round($limitedScore);
        }

        if($firstTotal > 0){
            $firstScore = ($firstCorrect / $firstTotal) * 100;
            $firstScore = round($firstScore);
        }


        $this->crud->use_table('afp_lifetime_scores');
        $data = array(
            'memID' => $memID,
            'lifeScore' => $lifetime,
            'budget' => $budgetScore,
            'energy' => $energyScore,
            'health' => $healthScore,
            'taxes' => $taxesScore,
            'labor' => $laborScore,
            'limited' => $limitedScore,
            'first' => $firstScore
        );

        $check  = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
        if(count($check)>0){
            $this->crud->update(array('memID' => $memID),$data, 0, 0, array('id' => 'DESC'));
        }else{
            $this->crud->create($data);
        }
    }
    $this->cacheClear();
}

  public function cacheClear(){
      
        $cache_path = $_SERVER['DOCUMENT_ROOT']."/api/api_1_1/cache/";
        echo $cache_path;
        foreach(glob($cache_path.'*.*') as $v){
            echo "here";
           unlink($v);
        }
    } 
  
  
}