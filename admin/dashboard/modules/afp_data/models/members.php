<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class members extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->ny_key = "b622f623f53ab474a16f934553b1c242:1:67201947";
        $this->nyApiLocation = "http://api.nytimes.com/svc/politics/v3/us/legislative/congress/";
        $this->slApiLocation = "http://congress.api.sunlightfoundation.com/legislators/?apikey=";
        $this->session = $this->config->item('CONG_SESSION');
    }
    
    // Sync each member of congress to the DB by chamber 
    //
    // SRC: NY Times API
    public function syncMembers($congress, $chamber = "senate"){
        
        $this->session = $congress;
        
        echo $this->session;

        $ch = curl_init("https://api.propublica.org/congress/v1/115/".$chamber."/members.json");


        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'X-API-Key: Q64HavAgWt1Ps5jSoUNKe9UgiMLkn3SI8zTFn4WP'
            )
        );

        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $data = json_decode($response);



        $results = $data->results[0]->members;

        curl_close($ch);

        print_r($results);

        $session = $congress;
        foreach($results as $m){
            $state = $m->state;
            echo $state;
            //Filter out no included states
            if($state != "MP" &&$state != "GU" && $state != "AS" && $state != "PR" && $state != "VI" && $state != "FM" && $state != "MH" && $state != "PW" && $state != "CZ" && $state != "PI" && $state != "TT" && $state != "CM" ) {



                if(isset($m->govtrack_id) && $this->checkRemoteFile("http://www.govtrack.us/data/photos/".$m->govtrack_id."-200px.jpeg") == true){
                    $govtrack_id = $m->govtrack_id;
                    //Get The Image
                    $image_path = "http://www.govtrack.us/data/photos/".$govtrack_id."-200px.jpeg";

                }else{
                    $govtrack_id = $m->govtrack_id;
                    $image_path = "";
                }


                $district = (isset($m->district)) ? $m->district : "";
                $chamber = ($m->title == "Representative" ? "house" : "senate");
                //Build the Data Array
                $data = array(
                    'fName' => $m->first_name,
                    'mname' => $m->middle_name,
                    'lName' => $m->last_name,
                    'congID' => $m->id,
                    'govTrackID' => $govtrack_id,
                    'congress' => $congress,
                    'state' => $m->state,
                    'party' => $m->party,
                    'district' => $district,
                    'chamber' => $chamber,
                    'gender' => "",
                    'bio' => "",
                    'title' => "",
                    'image_path' => $image_path
                );

                $this->crud->use_table('afp_members');
                //Check if record exist
                $check =  $this->crud->retrieve(array('congID' => $m->id, 'congress' => $congress, 'chamber' => $chamber), '', 0, 0, array('id' => 'DESC'));
                if(count($check) > 0 ){
                    // $this->crud->update(array('congID' => $m['id'], 'congress' => $session),$data, 0, 0, array('id' => 'DESC'));

                      $this->crud->update(array('congID' => $m->id, 'congress' => $congress),$data, 0, 0, array('id' => 'DESC'));
                }else{
                    echo "new - ".$m->last_name;
                    $this->crud->create($data);
                }

//                echo $m->id.'<br/>';
           }
        }
        $this->cacheClear();
    }
    
    private function buildNYPath($chamber, $congress){
        return $this->nyApiLocation.$congress."/".$chamber."/members.json?api-key=".$this->config->item('NY_TIMES_API_KEY');
    }
    
    public function cacheClear(){
      
        $cache_path = $_SERVER['DOCUMENT_ROOT']."/api/api_1_1/cache/";
        echo $cache_path;
        foreach(glob($cache_path.'*.*') as $v){
            echo "here";
           unlink($v);
        }
    }

    public function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}