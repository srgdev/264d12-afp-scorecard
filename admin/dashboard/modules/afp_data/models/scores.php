<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class scores extends CI_Model
{
  
  function __construct(){
    parent::__construct();
    $this->congress = "";
  }

  public function calcScores($congress){
      $this->congress = $congress;
      echo $this->congress;
      //Pull each member for set congress
      $this->crud->use_table('afp_members');    
      $members = $this->crud->retrieve(array('congress' => $this->congress), '', 0, 0, array('id' => 'DESC'));
      
      $this->crud->use_table('afp_key_votes');
      
      //pull House votes
      $houseVotes = $this->crud->retrieve(array('congress' => $this->congress, 'chamber' => 'house'), '', 0, 0, array('id' => 'DESC'));
      
      //pull Senate votes
      $senateVotes = $this->crud->retrieve(array('congress' => $this->congress, 'chamber' => 'senate'), '', 0, 0, array('id' => 'DESC'));
      
             
      //Loop each member
      foreach($members as $mem){
          $memID = $mem->congID;
          echo " ".$mem->congID." ".$mem->chamber;
          $chamber = $mem->chamber;
          $party = $mem->party;
          
          //set starting values
          $budgetTotal = 0;
          $energyTotal = 0;
          $healthTotal = 0;
          $taxesTotal = 0;
          $laborTotal = 0;
          $limitedTotal = 0;
          $firstTotal = 0;
          
          $correct = 0;
          $budgetCorrect = 0;
          $energyCorrect = 0;
          $healthCorrect = 0;
          $taxesCorrect = 0;
          $laborCorrect = 0;
          $limitedCorrect = 0;
          $firstCorrect = 0;
          
          $wrong = 0;
          $budgetWrong = 0;
          $energyWrong = 0;
          $healthWrong = 0;
          $taxesWrong = 0;
          $laborWrong = 0;
          $limitedWrong = 0;
          $firstWrong = 0;
          
          $nv = 0;
          $budgetNv = 0;
          $energyNv = 0;
          $healthNv = 0;
          $taxesNv = 0;
          $laborNv = 0;
          $limitedNv = 0;
          $firstNv = 0;
          
          $score = "-";
          $budgetScore = "-";
          $energyScore = "-";
          $healthScore = "-";
          $taxesScore = "-";
          $laborScore = "-";
          $limitedScore = "-";
          $firstScore = "-";
          
          //loop chamber
          $loopArray = ($chamber == 'senate') ? $senateVotes : $houseVotes;
          
          //if the are chamber votes then calc scores for the member
          $total = count($loopArray);
          if($total > 0){
              
              //Loop the chamber
              foreach($loopArray as $vote){
                  $afpPosition = strtolower($vote->position);
                  
                  $voteID = $vote->id;
                  $issue = $vote->issue;
                  
                  //Pull the position for the member
                  $this->crud->use_table('afp_votes');
                  $membersVote = $this->crud->retrieve(array('congress' => $this->congress, 'memID' => $memID, 'voteID' => $voteID), 'row', 0, 0, array('id' => 'DESC'));
                  
                  if(count($membersVote) > 0){
                      
                      $memPosition = strtolower($membersVote->position);
                      // echo $memPosition."<br/>";
                      // echo $afpPosition."<br/>";
                      // echo "<br/>";
                      // echo "<br/>";
                      //calc right or wrong   
                      if($memPosition == 'not voting' || $memPosition == 'speaker'){
                          $nv++;
                          switch($issue){
                              case 'budget':
                                 $budgetNv++;
                                  break;
                              case 'energy':
                                 $energyNv++;
                                  break;
                              case 'health':
                                 $healthNv++;
                                  break;
                              case 'taxes':
                                 $taxesNv++;
                                  break;
                              case 'labor':
                                 $laborNv++;
                                  break; 
                              case 'limited':
                                  $limitedNv++;
                                  break;
                              case 'first':
                                  $firstNv++;
                                  break;
                          }
                      }elseif($memPosition == $afpPosition){
                          $correct++;
                          switch($issue){
                              case 'budget':
                                 $budgetCorrect++;
                                  break;
                              case 'energy':
                                 $energyCorrect++;
                                  break; 
                              case 'health':
                                 $healthCorrect++;
                                  break;
                              case 'taxes':
                                 $taxesCorrect++;
                                  break;
                              case 'labor':
                                 $laborCorrect++;
                                  break;
                               case 'limited':
                                  $limitedCorrect++;
                                  break;
                              case 'first':
                                  $firstCorrect++;
                                  break;
                          }
                      }else{
                          $wrong++;
                          switch($issue){
                              case 'budget':
                                 $budgetWrong++;
                                  break;
                              case 'energy':
                                 $energyWrong++;
                                  break; 
                              case 'health':
                                 $healthWrong++;
                                  break;
                              case 'taxes':
                                 $taxesWrong++;
                                  break;
                              case 'labor':
                                 $laborWrong++;
                                  break;
                               case 'limited':
                                  $limitedWrong++;
                                  break;
                              case 'first':
                                  $firstWrong++;
                                  break;
                          }
                          
                      }//end memposition else
                      
                      switch($issue){
                           case 'budget':
                             $budgetTotal++;
                              break;
                           case 'energy':
                             $energyTotal++;
                              break;
                           case 'health':
                             $healthTotal++;
                              break;
                           case 'taxes':
                             $taxesTotal++;
                              break;
                           case 'labor':
                             $laborTotal++;
                              break;
                           case 'limited':
                              $limitedTotal++;
                              break;
                            case 'first':
                              $firstTotal++;
                              break;
                      }

                  }
              }
              
              //reCalc total
              //$total = $total - $nv;
              $total = ($wrong + $correct);
          
              $budgetTotal = ($budgetWrong + $budgetCorrect);
          
              $energyTotal = ($energyWrong + $energyCorrect);
          
              $healthTotal = ($healthWrong + $healthCorrect);
          
              $taxesTotal = ($taxesWrong + $taxesCorrect);
          
              $laborTotal = ($laborWrong + $laborCorrect);

              $limitedTotal = ($limitedWrong + $limitedCorrect);

              $firstTotal = ($firstWrong + $firstCorrect);
              
          }
          
          
          
          // //finally calc the scores
          if($total > 0){
              $score = ($correct/$total) * 100;
              $score = round($score);
          }
          
          // echo $total;
          // echo "<br/>";
          // echo $correct;
          // echo "<br/>";
          // echo $score;
//           
          if($budgetTotal > 0){
              $budgetScore = ($budgetCorrect/$budgetTotal) * 100;
              $budgetScore = round($budgetScore);
          }
          
          if($energyTotal > 0){
              $energyScore = ($energyCorrect/$energyTotal) * 100;
              $energyScore = round($energyScore);
          }
          
          if($healthTotal > 0){
              $healthScore = ($healthCorrect/$healthTotal) * 100;
              $healthScore = round($healthScore);
          }
          
          if($taxesTotal > 0){
              $taxesScore = ($taxesCorrect/$taxesTotal) * 100;
              $taxesScore = round($taxesScore);
          }
          
          if($laborTotal > 0){
              $laborScore = ($laborCorrect/$laborTotal) * 100;
              $laborScore = round($laborScore);
          }

          if($limitedTotal > 0){
              $limitedScore = ($limitedCorrect/$limitedTotal) * 100;
              $limitedScore = round($limitedScore); 
          }

          if($firstTotal > 0){
            $firstScore = ($firstCorrect / $firstTotal) * 100;
            $firstScore = round($firstScore);
          }
          
          
          //At long last we record the score
          $this->crud->use_table('afp_'.$this->congress.'_scores');
          $data['score'] = $score;
          $data['memID'] = $memID;
          $data['budget'] = $budgetScore;
          $data['energy'] = $energyScore;
          $data['health'] = $healthScore;
          $data['taxes'] = $taxesScore;
          $data['labor'] = $laborScore;
          $data['limited'] = $limitedScore;
          $data['first'] = $firstScore;
          $data['chamber'] = $chamber;
          $data['party'] = $party;

          $check = $this->crud->retrieve(array('memID' => $memID, 'chamber' =>$chamber), 'row', 0, 0, array('id' => 'DESC'));
          if(count($check) > 0 ){
               $this->crud->update(array('memID' => $memID, 'chamber' =>$chamber),$data, 0, 0, array('id' => 'DESC'));
          }else{
              $this->crud->create($data);
          }
      }   
      
      $this->cacheClear();
  } 
    
  public function cacheClear(){
      
        $cache_path = $_SERVER['DOCUMENT_ROOT']."/api/api_1_1/cache/";
        echo $cache_path;
        foreach(glob($cache_path.'*.*') as $v){
            echo "here";
           unlink($v);
        }
    }


    
}