<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class afp_data extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        // $this->load->model('legislation');
        // $this->legislation->syncLegislation();
        $this->cat = "afp_data";
    }
    
    function index() {
        
        $this->template->title('Data Sync')->build('datasync');
    }
    
   
    
    function syncMem(){
        $this->load->model('members');
        $cong = $this->uri->segment(4);
        $this->members->syncMembers($cong, 'house');
        $this->members->syncMembers($cong, 'senate');
        return true;
    }
    
    function calcScores(){
        $this->load->model('scores');
        $cong = $this->uri->segment(3);
        $this->scores->calcScores($cong);
        return 'good';
    }
    
    function calcLifetime(){
        $this->load->model('lifetime');
        $cong = $this->uri->segment(4);
        $this->lifetime->calcLifetime($cong);
        return 'good';
    }
    
    function calcIssue(){
        $this->load->model('issueaverages');
        $this->issueaverages->calcIssues();
        return 'good';
    }
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        $parent = "Data Sync";
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $parent, 'Data Sync', 'Sync Data', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        
        
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
}