<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class alerts extends CI_Controller {
    
    
    function __construct()
    {
        parent::__construct();
        
        $this->cat = "alerts";
    }
    

    
    public function index(){
        // Set the title    
        $this->template->set('title', ucfirst($this->cat)." Section");
        
        //call the page list function to build the page
        $this->pagesModel->biuldList($this->cat."/index", $this->cat);
        
        $this->template->title($this->cat)->build('pages/listPages.php');
    }
    
    public function addPage(){
        //call the addPage function to build the page
        $this->pagesModel->addPage($this->cat);
        
        $this->template->title(ucfirst($this->cat).'/ New Page')->build('pages/newPage.php');
    }
    
    
    
    public function editPage(){
        // Call the edit page function to build the page
        $this->pagesModel->editPage($this->cat);
        
        $this->template->title(ucfirst($this->cat).'/ Edit Page')->build('pages/editPage');
    }
    
   
    
    
    
    
    
    
    //Used to load need DB data (I.E Nav structure)
    public function install(){
        //Install Module
        $this->module_install->install($this->cat, '5', $this->cat);
        
        //Install Navigation
        //structure  set_nav_item($module = '', $parent = '', $title = '', $helpText = "", $link = '', $icon = '', $active = "", $uri = "",acl = '', acg = '')
        $this->navigation->set_nav_item($this->cat, $this->cat, 'Key Vote Alerts', 'Mange Pages', $this->cat.'/index', 'file', 'index', '2', '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Add Page', 'Add Page', $this->cat.'/addPage', 'plus-sign', "addPage", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Home Page', 'Home Page', $this->cat.'/home', 'home', "home", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Menu', 'Menu', $this->cat.'/menu', 'th-list', "menu", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Quick Links', 'Quick Links', $this->cat.'/links', 'share', "links", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'News', 'News', $this->cat.'/news', 'tasks', "news", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'FAQ', 'FAQ', $this->cat.'/faq', 'question-sign', "faq", "2", '5', $this->cat);
        // $this->navigation->set_nav_item($this->cat, $this->cat, 'Settings', 'Settings', $this->cat.'/settings', 'cog', "settings", "2", '3', $this->cat);
//         
        $this->module_install->finish($this->cat);
    }
    
    //Remove DB data
    public function uninstall(){
        //Uninstall Module
        $this->module_install->uninstall($this->cat);
        
        //Uninstall navigation
        $this->navigation->unset_nav_items($this->cat);
        
        $this->module_install->finish($this->cat);
    }
    
}