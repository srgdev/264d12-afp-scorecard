<h1>Data Sync</h1>

<script>
    $(function(){
        var urler = ' + <?php echo base_url().index_page()."/" ?> + ';
        $('.legBtn').click(function(){
            $(this).button('loading')
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/cfimports/syncLeg',
               success: function(data){
                   $('.legBtn').button('reset');
               }
            });
        });
        
        
        $('.memBtn').click(function(){
            $(this).button('loading')
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/cfimports/syncMem',
               success: function(data){
                   $('.memBtn').button('reset');
               }
            });
        });
        
        
        $('.comBtn').click(function(){
            $(this).button('loading')
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/cfimports/syncCom',
               success: function(data){
                   $('.comBtn').button('reset');
               }
            });
        });
        
        $('.notBtn').click(function(){
            $(this).button('loading')
            $.ajax({
               url: '<?php echo base_url().index_page() ?>/cfimports/syncNotifications',
               success: function(data){
                   $('.notBtn').button('reset');
               }
            });
        });
    })
</script>


<h3>Sync Members Database</h3>
<br />
<button type="button" class="btn btn-primary loading memBtn" data-loading-text="Loading...">Sync Members</button>
<br /><br />
<h3>Sync Committees Database</h3>
<br />
<button type="button" class="btn btn-primary loading comBtn" data-loading-text="Loading...">Sync Committees</button>
<br /><br />
<h3>Sync Legislation Database</h3>
<br />
<button type="button" class="btn btn-primary loading legBtn" data-loading-text="Loading...">Sync Legislation</button>
<br /><br />
<h3>Sync User Notifications</h3>
<br />
<button type="button" class="btn btn-primary loading notBtn" data-loading-text="Loading...">Sync Notifications</button>


