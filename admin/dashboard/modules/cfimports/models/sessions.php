<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class sessions extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
		$this->client = new SoapClient("http://webservices.legis.ga.gov/GGAServices/Session/Service.svc?wsdl");
    }
	
	
	
	public function getCurrent(){
	    $result = $this->client->GetSessions();
        $sessions = $result->GetSessionsResult->Session;
        $current = "";
        foreach($sessions as $session){
            if($session->IsDefault == 1){
                $current = $session->Id;
            }
        }
        return $current;
	}
    
    
    
    
	
}