<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class notifications extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
    
  
  public function updateLeg(){
      $members = $this->getLegMembers();
      //Loop what each member is tracking
      foreach($members as $member){
          $leg = $member->legID;
          $this->crud->use_table('CF_legislation_updates');
          $query = $this->crud->retrieve(array('legID' => $leg), '', 0, 0, array('id' => 'DESC'));
          //Loop each updated that matches a track
          foreach($query as $legad){
              $data = array(
                  'legID' => $leg,
                  'message' => $legad->message,
                   'date' => date("M jS, Y"),
              );
              //add each sub item
              $this->crud->use_table('CF_notifications_subs');
              $this->crud->create($data);
              //add notification
              $this->crud->use_table('CF_notifications');
              $check = $this->crud->retrieve(array('legID' => $leg), '', 0, 0, array('id' => 'DESC'));
              if(count($check) > 0){
                  //Mark as not viewd
                  $this->crud->update(array('legID' => $leg),array('viewed' => ""), 0, 0, array('id' => 'DESC'));
              }else{
                  //add new
                  $this->crud->create(array('legID' => $leg, 'user' => $member->id));
              }
              
             
          }
          
      }
      
      $this->db->empty_table('CF_legislation_updates');
      
      return true;
  }
  
  
  private function getLegMembers(){
      $query = $this->db->query('SELECT CF_legislation_track.legID, CF_users.id
                FROM CF_users, CF_legislation_track  
                WHERE CF_legislation_track.user = CF_users.id
        ');
     return $query->result();
  }
  
  
  public function updateComs(){
      $members = $this->getComMembers();
      //Loop what each member is tracking
      foreach($members as $member){
          $leg = $member->comID;
          $this->crud->use_table('CF_committees_updates');
          $query = $this->crud->retrieve(array('comID' => $leg), '', 0, 0, array('id' => 'DESC'));
          //Loop each updated that matches a track
          echo count($query);
          foreach($query as $legad){
              $data = array(
                  'comID' => $leg,
                  'message' => $legad->message,
                   'date' => date("M jS, Y"),
              );
              //add each sub item
              $this->crud->use_table('CF_notifications_subs');
              $this->crud->create($data);
              //add notification
              $this->crud->use_table('CF_notifications');
              $check = $this->crud->retrieve(array('comID' => $leg), '', 0, 0, array('id' => 'DESC'));
              if(count($check) > 0){
                  //Mark as not viewd
                  $this->crud->update(array('comID' => $leg),array('viewed' => ""), 0, 0, array('id' => 'DESC'));
              }else{
                  //add new
                  $this->crud->create(array('comID' => $leg, 'user' => $member->id));
              }
          }
      }
      $this->db->empty_table('CF_committees_updates');
      return true;
  }


  private function getComMembers(){
      $query = $this->db->query('SELECT CF_committees_track.comID, CF_users.id
                FROM CF_users, CF_committees_track  
                WHERE CF_committees_track.user = CF_users.id
        ');
     return $query->result();
  }
  
  
  
  public function emailNotifications(){
      $this->crud->use_table('CF_notifications');
      $notes =  $this->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
      $users = array();
      foreach($notes as $note){
          if(!in_array($note->user, $users)){
              array_push($users, $note->user);
          }
      }
      
      foreach($users as $user){
          $this->crud->use_table('CF_notifications');
          $notes = $this->crud->retrieve(array('user' => $user, 'viewed' => ""), '', '', 0, 0, array('id' => 'DESC'));
          $noteCount = count($notes);
          $this->crud->use_table('CF_users');
          $userInfo = $this->crud->retrieve(array('id' => $user), 'row', 0, 0, array('id' => 'DESC'));
          $email = $userInfo->email;
          $message = "<br/><br/>Hello ".$userInfo->first."<br/><br/>You have <strong>".$noteCount."</strong> notifications on Legislation and Committees you are tracking on Capitol Hub<br/><br/> Log into your account to view them http://capitolfaces.com<br/><br/>Regards,<br/><br/>The Capitol Faces Team";
          if($noteCount > 0) {$this->cmsemail->sendMail($email,'', 'Capitol Faces Notifications', $message, 'notifications@capitolfaces.com');}
          
      }
  }
}