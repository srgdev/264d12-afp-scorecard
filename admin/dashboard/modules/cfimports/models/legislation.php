<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class legislation extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
		$this->client = new SoapClient("http://webservices.legis.ga.gov/GGAServices/Legislation/Service.svc?wsdl");
		$this->legIDs = array();
		//ADD GET CURRENT SESSION
		$this->load->model('sessions');
        $this->session = $this->sessions->getCurrent();
    }
	
	
	public function syncLegislation(){
	    // $result = $this->client->GetLegislationSearchResultsPaged(array('PageSize'=> 23, 'StartIndex' => 0, 'Constraints' => array( 'Session' => array('Id' => 'IsDefault'))));
        // $legs = $result->GetLegislationSearchResultsPagedResult->Page->LegislationSearchResult;
	    $result = $this->client->GetLegislationForSession(array('SessionId'=> $this->session));
        $legs = $result->GetLegislationForSessionResult->LegislationIndex;
        foreach($legs as $leg){
            $id = $leg->Id;
            array_push($this->legIDs, $id);
        }
        $this->syncEachLegislation();
        return true;
	}
    
    
    public function syncEachLegislation() {
        
        // echo count($this->legIDs);
        foreach($this->legIDs as $id){
             $result = $this->client->GetLegislationDetail(array('LegislationId'=>$id));
             $legs = $result->GetLegislationDetailResult;
             $legID = $legs->Id;
             
             
              //Write Legislation Data
             $data1 = array(
                  'caption' => $legs->Caption,
                  'DocumentType' => $legs->DocumentType,
                  'legID' => $legID,
                  'legType' => $legs->LegislationType,
                  'legNumber' => $legs->Number,
                  'session' => $legs->Session->Id,
                  'summary' => $legs->Summary,
                  'footnotes' => $legs->Footnotes
             );
             $this->writeLegislation($legID, $data1);
             
             $nspons = false;
             //Write Sponsors
             $sponsors = $legs->Authors->Sponsorship;
             $this->crud->use_table('CF_legislation_sponsors');
             if(count($sponsors) > 0){
                 if(count($sponsors) == 1){
                     $this->crud->use_table('CF_legislation_sponsors');
                     $check = $this->crud->retrieve(array('legID' =>  $legID, 'memID' => $sponsors->MemberId, 'type' => $sponsors->Type), '', 0, 0, array('id' => 'DESC'));
                     if(count($check) == 0) {$this->crud->create(array('legID' =>  $legID, 'memID' => $sponsors->MemberId, 'type' => $sponsors->Type)); $nspons = true;}
                 }else{
                     foreach($sponsors as $s){
                        $check = $this->crud->retrieve(array('legID' =>  $legID, 'memID' => $s->MemberId, 'type' => $s->Type), '', 0, 0, array('id' => 'DESC'));
                        if(count($check) == 0) {
                            $this->crud->use_table('CF_legislation_sponsors');
                            $this->crud->create(array('legID' =>  $legID, 'memID' => $s->MemberId, 'type' => $s->Type));
                            $nspons = true;
                            //$this->writeLegUpdate($legID, 'New Sponsor Added');
                        }
                     }
                 }
             }
             $sponser = $legs->Sponsor;
             if(count($sponser) > 0){
                 $check = $this->crud->retrieve(array('legID' =>  $legID, 'memID' => $sponser->MemberId, 'type' => $sponser->Type), '', 0, 0, array('id' => 'DESC'));
                 if(count($check) == 0) {
                     $this->crud->use_table('CF_legislation_sponsors');
                     $this->crud->create(array('legID' =>  $legID, 'memID' => $sponser->MemberId, 'type' => $sponser->Type));
                     $nspons = true;
                     //$this->writeLegUpdate($legID, 'New Sponsor Added');
                 }
             }
             
             if($nspons) {$this->writeLegUpdate($legID, 'New Sponsor Added');}
             //Write Committees
             if(count($legs->Committees) > 0){
                 $committees = $legs->Committees->CommitteeListing;
                 $committee = array();
                 if(count($committees) > 0){
                     if(count($committees) == 1){
                        array_push($committee, $committees->Id);
                     }else{
                         foreach($committees as $c){
                             array_push($committee, $c->Id);
                         }
                     }
                     $this->crud->use_table('CF_legislation_committees');
                     foreach($committee as $com){
                         $check = $this->crud->retrieve(array('legID' =>  $legID, 'comID' => $com), '', 0, 0, array('id' => 'DESC'));
                         if(count($check) == 0) {
                             $this->crud->create(array('legID' =>  $legID, 'comID' => $com));
                             $this->writeLegUpdate($legID, 'New Committee Added');
                             $this->writeLegUpdate($com, 'New Legislation Added');
                         }
                     }
                 }
             }
             
             //write status
             $status = $legs->Status;
             $this->crud->use_table('CF_legislation_status');
             $check = $this->crud->retrieve(array('legID' =>  $legID), '', 0, 0, array('id' => 'DESC'));
             $data = array('legID' => $legID, 'status' => $status->Description, 'code' => $status->Code, 'date' => $status->Date);
             if(count($check) == 0) {
                 $this->crud->create($data);
             }else{
                 $this->crud->update(array('legID' =>  $legID),$data, 0, 0, array('id' => 'DESC')); 
             }
             
             //write status history
             $history = $legs->StatusHistory->StatusListing;
             if(count($history) > 0){
                 if(count($history) == 1){
                     $this->writeStatusHistory($history->Description, $history->Code, $history->Date, $legID);
                 }else{
                     foreach($history as $hist){
                         $this->writeStatusHistory($hist->Description,$hist->Code, $hist->Date, $legID);
                     }
                 }
             }
             
             //write versions
             $versions = $legs->Versions->DocumentDescription;
             if(count($versions) > 0){
                 if(count($versions) == 1 ){
                     $this->writeVersions($versions->Description, $versions->Url, $versions->Id, $legID);
                 }else{
                     foreach($versions as $ver){
                         $this->writeVersions($ver->Description, $ver->Url, $ver->Id, $legID);
                     }
                 }
             }
             
             // write Votes
             $votes = $legs->Votes;
             if(count($votes) > 0){
                 $votes = $legs->Votes->VoteListing;
                 if(count($votes) > 0){
                     if(count($votes) == 1){
                        $this->writeVotes($votes->Branch, $votes->Caption, $votes->Date, $votes->Description, $votes->Yeas, $votes->Nays, $votes->NotVoting, $legID, $votes->VoteId, $votes->Excused);
                     }else{
                         foreach($votes as $vote){
                            $this->writeVotes($vote->Branch, $vote->Caption, $vote->Date, $vote->Description, $vote->Yeas, $vote->Nays, $vote->NotVoting, $legID, $vote->VoteId, $vote->Excused); 
                         }
                     }
                 }
             }
             
              
        }

    }

    private function writeStatusHistory($status, $code, $date, $legID){
        $this->crud->use_table('CF_legislation_status_history');
        $data = array('status' => $status, 'date' => $date,'code' => $code, 'legID' => $legID);
        $check = $this->crud->retrieve(array('legID' =>  $legID, 'status' => $status, 'code' => $code, 'date' => $date), '', 0, 0, array('id' => 'DESC'));
        if(count($check) == 0 ){
            $this->crud->create($data);
            $this->writeLegUpdate($legID, 'New Status Added');
        }
    }
    
    private function writeVersions($version, $url, $verID, $legID){
        $this->crud->use_table('CF_legislation_version');
        $data = array('legID' => $legID, 'version' => $version, 'url' => $url, 'verID' => $verID);
        $check = $this->crud->retrieve(array('legID' =>  $legID, 'version' => $version), '', 0, 0, array('id' => 'DESC'));
        if(count($check) == 0){
            $this->crud->create($data);
            $this->writeLegUpdate($legID, 'New Version Added');
        }
    }

    private function writeVotes($branch, $caption, $date, $description, $yes, $no, $not, $legID, $voteID, $excused){
        $this->crud->use_table('CF_legislation_votes');
        $data = array(
            'legID' => $legID,
            'branch' => $branch,
            'caption' => $caption,
            'date' => $date,
            'description' => $description,
            'yes' => $yes,
            'no' => $no,
            'not' => $not,
            'excused' => $excused,
            'voteID' => $voteID
        );
        $check = $this->crud->retrieve(array('legID' =>  $legID, 'voteID' => $voteID), '', 0, 0, array('id' => 'DESC'));
        if(count($check) == 0){
            $this->crud->create($data);
            $this->writeLegUpdate($legID, 'New Vote Added');
        }
    }
	
    private function writeLegislation($legID, $data){
         $this->crud->use_table('CF_legislation');
         $check = $this->crud->retrieve(array('legID' =>  $legID), '', 0, 0, array('id' => 'DESC'));
         if(count($check) != 0){
             $this->crud->update(array('legID' =>  $legID),$data, 0, 0, array('id' => 'DESC')); 
         }else{
             $this->crud->create($data);
         }     
    }

    private function writeLegUpdate($legID, $message){
        $this->crud->use_table('CF_legislation_updates');
        $data = array(
            'legID' => $legID,
            'message' => $message
        );
        $this->crud->create($data);
    }

    private function writeComUpdate($comID, $message){
        $this->crud->use_table('CF_committee_updates');
        $data = array(
            'comID' => $comID,
            'message' => $message
        );
        $this->crud->create($data);
    }
}