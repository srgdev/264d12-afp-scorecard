<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class members extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
		$this->client = new SoapClient("http://webservices.legis.ga.gov/GGAServices/Members/Service.svc?wsdl");
		$this->memberIDs = array();
		//ADD GET CURRENT SESSION
		$this->load->model('sessions');
		$this->session = $this->sessions->getCurrent();
    }
	
	
	
	public function syncMembers(){
		 $this->crud->use_table('CF_members');
		 $result = $this->client->GetMembersBySession(array('SessionId'=>$this->session));
		 $members = $result->GetMembersBySessionResult->MemberListing;
		 foreach($members as $member){
		 	$id = $member->Id;
			array_push($this->memberIDs, $id);
			$this->checkNewMember($id);
            $this->checkNewBio($id, $member);
		 }
		 $this->syncBios();
         return true;
	}
    
    public function checkNewMember($id){
        $this->crud->use_table('CF_members');
        $check = $this->crud->retrieve(array('memID' => $id, 'session' => $this->session), '', 0, 0, array('id' => 'DESC'));
        if(count($check)  == 0){
            $this->crud->create(array('memID' => $id, 'session' => $this->session));
        }
    }
    
    public function checkNewBio($id ,$member){
        $this->crud->use_table('CF_bios');
        $check2 = $this->crud->retrieve(array('memID' => $id), '', 0, 0, array('id' => 'DESC'));
        if(count($check2) == 0){
            $this->crud->create(array('memID' => $id, 'chamber' => $member->District->Type, 'party' => $member->Party, 'district' => $member->District->Number));
        }
    }
	
	public function syncBios(){
		$this->crud->use_table('CF_bios');
		foreach($this->memberIDs as $id){
			$result = $this->client->GetMember(array('MemberId'=>$id));
			$members = $result->GetMemberResult;
			
			$first = $members->Name->First; 
			$last = $members->Name->Last;
			$middle = $members->Name->Middle;
			
			$email = $members->Address->Email;
			$phone = $members->Address->Phone;
			$fax = $members->Address->Fax;
			$street = $members->Address->Street;
			$city = $members->Address->City;
			$zip = $members->Address->Zip;
            $img = $last.$first.$id.".jpg";
            $bio = "";
            
            $imgs = $members->FreeForm1;
            preg_match_all('/\shref="(?<href>[^"]+)"/',$imgs, $results);
            if(count($results) > 0){
                foreach($results['href'] as $href){
                    if($this->get_file_extension($href) == "jpg"){
                        //$img = $href;
                    }elseif($this->get_file_extension($href) == "pdf"){
                        $bio = $href;
                    }
                }
            } 
           
			$this->crud->update(array('memID' => $id),array( 'first' => $first, 'last' => $last, 'middle' => $middle, 'email' => $email,'phone' => $phone , 'fax' => $fax, 'street' => $street, 'city' => $city, 'zip' => $zip, 'photo' => $img, 'bio' => $bio), 0, 0, array('id' => 'DESC'));
		
		}
	}
    
    function get_file_extension($file_name) {
        return substr(strrchr($file_name,'.'),1);
    }
    
    
	
}