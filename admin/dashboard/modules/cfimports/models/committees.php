<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

 
class committees extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
        $this->client = new SoapClient("http://webservices.legis.ga.gov/GGAServices/Committees/Service.svc?wsdl");
        $this->comIDs = array();
        //ADD GET CURRENT SESSION
        $this->load->model('sessions');
        $this->session = $this->sessions->getCurrent();
    }

    public function syncCommittees(){
        $result = $this->client->GetCommitteesBySession(array('SessionId'=>$this->session));
        $coms = $result->GetCommitteesBySessionResult->CommitteeListing;
        foreach($coms as $com) {
            $id = $com->Id;
            array_push($this->comIDs, $id);
        }
        $this->sysEachCom();
        return true;
    }
    
    public function sysEachCom(){
        foreach($this->comIDs as $id){
            $result = $this->client->GetCommittee(array('CommitteeId'=>$id));
            $this->crud->use_table('CF_committees');
            $com = $result->GetCommitteeResult;
            $data = array(
                'code' => $com->Code,
                'comID' => $com->Id,
                'name' => $com->Name,
                'type' => $com->Type,
                'street' => $com->Address->Street,
                'city' => $com->Address->City,
                'zip' => $com->Address->Zip,
                'fax' => $com->Address->Fax,
                'phone' => $com->Address->Phone,
                'email' => $com->Address->Email,
                'description' => $com->Description,
                'session' => $this->session
            );
            
            $check = $this->crud->retrieve(array('comID' => $com->Id, 'session' => $this->session), '', 0, 0, array('id' => 'DESC')); 
            if(count($check) == 0) {
                $this->crud->create($data);
            }else{
                $this->crud->update(array('comID' => $com->Id),$data, 0, 0, array('id' => 'DESC'));
            }
            
            $members = $com->Members->CommitteeMember;
            
            foreach($members as $member){
                
                $mem = array(
                    'comID' => $com->Id,
                    'memID' => $member->Member->Id,
                    'role' => $member->Role,
                    'session' => $this->session
                );
                $this->crud->use_table('CF_committee_members');
                $checkMems = $this->crud->retrieve(array('comID' => $com->Id, 'memID' => $member->Member->Id, 'session' => $this->session), '', 0, 0, array('id' => 'DESC'));
                if(count($checkMems) == 0) {
                    $this->crud->use_table('CF_committee_members');
                    $this->crud->create($mem);
                    $this->writeComUpdate($com->Id, 'New Member Added');
                }else{
                    $this->crud->use_table('CF_committee_members');
                    $this->crud->update(array('comID' => $com->Id, 'memID' => $member->Member->Id, 'session' => $this->session ),$mem, 0, 0, array('id' => 'DESC'));
                }
            }
            
            
            
        }
    }
    
    
     private function writeComUpdate($comID, $message){
        $this->crud->use_table('CF_committees_updates');
        $data = array(
            'comID' => $comID,
            'message' => $message
        );
        $this->crud->create($data);
    }
}