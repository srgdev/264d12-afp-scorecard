<?php



function add_issues($page){
    $CI =& get_instance();
     $CI->crud->use_table('afp_issues');
     $issues = $CI->crud->retrieve_all( 0, 0, array('id' => 'DESC'));
     $list = "";
     $issue = (isset($page->issue)) ? $page->issue : '' ;
    foreach($issues as $cat){
            $selected = ($issue == $cat->issue_code) ? 'selected' : '';
            $list .= '<option value="'.$cat->issue_code.'" '.$selected.'>'.$cat->issue_name.'</optoin>';
    }
    echo '<br/><div class="sideBox">
        <div class="sideBoxTitle">AFP Issues</div>
        <select id="issue" name="issue" />
            <option value="">Select Issue</option>
            '.$list.'
        </select>
    </div>';
}

function saveIssue($id){
    $CI =& get_instance();
    if(isset($_REQUEST['issue'])){
        $issue = $_REQUEST['issue'];
        $CI->crud->update(array('id' => $id),array('issue' => $issue), 0, 0, array('id' => 'DESC'));
    }
}

function add_chamber($page){
    $CI =& get_instance();
    $chamber = (isset($page->chamber)) ? $page->chamber : '' ;
    $houseselected = ($chamber == 'house') ? 'selected' : '';
    $senateselected = ($chamber == 'senate') ? 'selected' : '';
    echo '<br/><div class="sideBox">
        <div class="sideBoxTitle">Chamber</div>
        <select id="chamber" name="chamber" />
            <option value="">Select Chamber</option>
            <option value="house" '.$houseselected.'>House</option>
            <option value="senate" '.$senateselected.'>Senate</option>
        </select>
    </div>';
}

function saveChamber($id){
    $CI =& get_instance();
    if(isset($_REQUEST['chamber'])){
        $chamber = $_REQUEST['chamber'];
        $CI->crud->update(array('id' => $id),array('chamber' => $chamber), 0, 0, array('id' => 'DESC'));
    }
}


function add_excerpt($page){
    if(isset($page->excerpt)){$content = $page->excerpt;}else{$content = "";}
        echo '<br/><div class="sideBox">
            <div class="sideBoxTitle">Excerpt</div>
            <textarea class="editor" style="height: 360px;" name="excerpt">'.$content.'</textarea>
        </div>';
}

function save_excerpt($id){
    $CI =& get_instance();
    if(isset($_REQUEST['excerpt'])){
        $excerpt = $_REQUEST['excerpt'];
        $CI->crud->update(array('id' => $id),array('excerpt' => $excerpt), 0, 0, array('id' => 'DESC'));
    }
}



function hideMeta($page) {
        return true;
}
// add_action('main', 'question');
// add_action('main', 'answer');
 add_action('main', 'add_excerpt');
// 
add_action('side', 'add_issues');
add_action('side', 'add_chamber');
// add_action('hide_title', 'faq_hide');
// add_action('hide_content', 'faq_hide');
add_action('hide_meta', 'hideMeta');
add_action('hide_slug', 'hideMeta');
// 
// add_action('side', 'hideSideBar');
// add_action('side', 'makePageGroup');
// add_action('side', 'pageGroup');
// add_action('side', 'pressRelease');
// 
// 
// add_action('side', 'newsCats');
// 
// add_action('save', 'savehideSideBar');
// add_action('save', 'saveIframe');
add_action('save', 'saveIssue');
add_action('save', 'saveChamber');
add_action('save', 'save_excerpt');