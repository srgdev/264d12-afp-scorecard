<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Hooks 
 * 
 * @package Hooks Helper for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
*/
$CI =& get_instance();
	
$hook_dir =  APPPATH."modules/hooks/"; 

$hook_files = glob($hook_dir."*.php");  


$hooks = new applyHooks;


function add_action($on,$func){
	
	 array_push(applyHooks::$hook[$on], $func);  
}
	

foreach($hook_files as $hook_file) {
	
  require_once($hook_file);  
}  


class applyHooks {
    
    static $hook = array();
    
    function __construct()
    {
        applyHooks::$hook['main'] = array();
        applyHooks::$hook['side'] = array();
        applyHooks::$hook['slug'] = array();
        applyHooks::$hook['save'] = array();
        applyHooks::$hook['update'] = array();
        applyHooks::$hook['hide_title'] = array();
        applyHooks::$hook['hide_content'] = array();
        applyHooks::$hook['hide_meta'] = array();
        applyHooks::$hook['hide_slug'] = array();
    }
    
    static function add_main($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['main'] as $hook) {  
          call_user_func($hook, $val);  
        }  
    }
    
    static function add_side($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['side'] as $hook) {  
          call_user_func($hook, $val);  
        }  
    }
    
    static function add_slug($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['slug'] as $hook) {  
          call_user_func($hook, $val);  
        }  
    }
    
    static function on_save($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['save'] as $hook) {  
          call_user_func($hook, $val);  
        }  
    }
    
    static function on_update($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['update'] as $hook) {  
          call_user_func($hook, $val);  
        }  
    }
    
    static function hide_title($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['hide_title'] as $hook) {  
          return call_user_func($hook, $val);  
        }  
    }
    
    static function hide_content($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['hide_content'] as $hook) {  
          return call_user_func($hook, $val);  
        }  
    }
    
    static function hide_meta($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['hide_meta'] as $hook) {  
          return call_user_func($hook, $val);  
        }  
    }
    
    static function hide_slug($val = ""){
        /* implement the hooks */  
        foreach(applyHooks::$hook['hide_slug'] as $hook) {  
          return call_user_func($hook, $val);  
        }  
    }
    

}
