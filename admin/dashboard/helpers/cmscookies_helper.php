<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


	function set_cms_cookie($name, $value, $time = 31104000){
		setcookie($name, $value, time()+$time); 
	}
	
	
	function get_cms_cookie($name){
		if(isset($_COOKIE[$name]))
			return $_COOKIE[$name]; 
		else
			return '';
	}
	
	function delete_cms_cookie($name){
		if(isset($_COOKIE[$name])) {
			setcookie($name, "", time()-3600);
		}
	}

?>