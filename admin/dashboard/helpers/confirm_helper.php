<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

    /**
    * confirm dialog helper
    * 
    * autoload helpers confirm, url 
    * example;
    * $attr = array( 'class' => 'delete' ) // just like Ci anchor();
    * $id = unique id to anchor
    * call this "confirm( 'item/delete' , 'Title' , $id ,$attr , array( ' dialog' => 'Confirm box text' ,' btrue' => 'true button text', bfalse => 'false button text'  ) );"
    * 
    * requiments:
    *     jQuery www.jquery.com
    *    plugin Impromptu   http://trentrichardson.com/Impromptu/
    *
    * @ access public
    * @ param string            //  url if confirm is true
    * @ param intger            // id
    * @ param string            // text to confirm box title
    * @ param arr[optional]         //  defaults array( ' dialog' => 'Confirm delete?' ,' btrue' => 'Ok', bfalse => Cancel'  )
    * @ return string            // return anchor
    **/
    function confirm( $uri , $text, $title, $message , $class = '')
    {
        return "<a href=\"#\" title=\"".$title."\" onclick=\"confirmMes('".base_url()."".index_page()."/".$uri."','".$message."')\" class=\"".$class."\">".$text."</a>";
    }
?>