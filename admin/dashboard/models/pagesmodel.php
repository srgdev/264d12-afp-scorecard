<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Pages 
 * 
 * @package Pages Model for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
class pagesModel extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->crud->use_table('cms_pages');
		$this->load->helper('applyHooks');
		$this->needsMod = $this->configs->get('moderation');
		$this->author = $this->auth->getUserName(); 
    }
	
	
	
	function getPageType(){
		return $_REQUEST['type'];
	}
	
	function getPageTitle(){
		return  $_REQUEST['title'];
	}
	
	function getPageCat(){
		return $_REQUEST['cat'];
	}
	
	function getStatus(){
		$moderation = $this->configs->get('moderation');
		if($moderation == "true"){$status = "pending";}else{$status = "published";}
		return $status;
	}
	
	function makeSlug(){
		$title = $this->getPageTitle();
		$slug  = str_replace(" ","_",$title);
		$slug  = str_replace("&","",$slug);
		$slug  = str_replace("'","",$slug);
		$slug  = str_replace(",","",$slug);
		$slug  = str_replace(".","",$slug);
		$slug  = str_replace("/","",$slug);
		$slug  = str_replace("\\","",$slug);
		$slug  = str_replace("\"","",$slug);
		$slug  = str_replace("%","",$slug);
		$slug  = str_replace("#","",$slug);
		$slug  = str_replace("$","",$slug);
		$slug  = str_replace("@","",$slug);
		$slug  = str_replace("*","",$slug);
		$slug  = str_replace(":","",$slug);
		$slug  = str_replace(";","",$slug);
		$slug = strtolower($slug);
		$query = $this->db->get_where('cms_pages', array('title' => $title));
		$num = $query->num_rows();
		//Itirate slug for dups
		if($num != 0){  $it = $num + 1; $slug = $slug.$it; }
		return $slug;
	}
	
	function savePage(){
		
		//Handle regular Pages
		if ($this->input->post('pageSubmit')){
			if($this->needsMod == 'true'){
				$last = $this->insertPageMod();
			}else{
				$last = $this->insertPage();
			}
		}
		
		//Handle PageGroups
		if ($this->input->post('groupSubmit')){
			$last = $this->insertGroup();
		}
		
		//Log the event
		$this->logevents->logEvent($this->getPageCat()." Page Added",$this->getPageTitle()." Added in ".$this->getPageCat());
		
		//handle modiration
		if($this->needsMod == "true"){$this->moderation->mark($last);}
		return $last;
	}	
	
	function insertPageMod(){
		$slug = $this->makeSlug();
		
		$this->crud->use_table('cms_pages');
		$insert = $this->crud->create(array('cat'=>$this->getPageCat(), 'page_type' => $this->getPageType(), 'hasDraft' => true, 'title' => $this->getPageTitle(), 'author' => $this->author));
		
		if($_REQUEST['makegroup'] == 'true'){
			$this->crud->update(array('id' => $insert),array('isGroup' => 'true'), 0, 0, array('id' => 'DESC'));
			$this->crud->use_table('cms_pagegroups');
			$pagess = $_REQUEST['pages'];
			$i = 2;
			foreach($pagess as $paged){
				$this->crud->create(array('page_id' => $paged, 'group_id' => $insert, 'order' => $i));
				$i++;
			}
		}
		$created = date("m/d/Y", strtotime($_REQUEST['created']));
		$data = array(
		   'pageID' => $insert,
           'cat' => $this->getPageCat(),
           'title' => $this->getPageTitle(),
           'content' => $_REQUEST['content'],
           'slug' => $slug,
           'date' => $created,
           'page_type' => $this->getPageType(),
           'status' => 'pending',
           'author' => $this->author ,
           'start' => $_REQUEST['start'],
           'end' => $_REQUEST['end'],
           'isGroup' => $_REQUEST['makegroup']               
	    );
		$this->db->insert('cms_pageversions', $data); 
		$insert1 = $this->db->insert_id();
		applyHooks::on_save($insert1);
		return $insert;
	}
	
	
	function insertPage(){
		$status = "published";
		$table = 'cms_pages';
		
			$created = date("m/d/Y", strtotime($_REQUEST['created']));	
		$data = array(
           'cat' => $this->getPageCat(),
           'title' => $this->getPageTitle(),
           'content' => $_REQUEST['content'],
           'slug' => $this->makeSlug(),
           'page_type' => $this->getPageType(),
           'status' => $status,
           'date' => $created,
           'start' => $_REQUEST['start'],
           'end' => $_REQUEST['end'],
           'author' => $this->author                     
	    );
		
		
		
		
		$this->db->insert($table, $data); 
	
		$insert = $this->db->insert_id();
		
		if(isset($_REQUEST['makegroup']) && $_REQUEST['makegroup'] == 'true'){
			$this->crud->use_table('cms_pages');
			$this->crud->update(array('id' => $insert),array('isGroup' => 'true'), 0, 0, array('id' => 'DESC'));
			$this->crud->use_table('cms_pagegroups');
			$pagess = $_REQUEST['pages'];
			$i = 2;
			foreach($pagess as $paged){
				$this->crud->create(array('page_id' => $paged, 'group_id' => $insert, 'order' => $i));
				$i++;
			}
		}
		
		$this->crud->use_table('cms_pages');
		applyHooks::on_save($insert);
		return $insert;
		
	}
	
	

	function archiveVersion($pageID){
		$this->crud->use_table('cms_pages');
		$old = $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC')); 
		$verDate = date('Y-m-d H:i:s');
		$version = array(
           'pageID' => $old->id,
           'title' => $old->title,
           'content' => $old->content,
           'slug' => $old->slug,
           'cat' => $old->cat,
           'author' => $old->author,
           'versionDate' => $verDate,
           'date' => $old->updated,
           'description' => $old->description,
           'start' => $old->start,
           'end' => $old->end,
           'status' => 'published',
           'page_type' => $old->page_type,
           'isGroup' => $old->isGroup                      
	    );
		if($old->content != ""){
			$this->crud->use_table('cms_pageversions');
			$this->crud->create($version);
		}
	}

	
	function update($id){
		
		$update = date('Y-m-d H:i:s');
		
		if ($this->input->post('RejectPage')) {
			$this->moderation->reject($id);
		}
			if(isset($_REQUEST['makegroup'])) {$group = $_REQUEST['makegroup'];}else{$group  = "";}
		      $created = date("m/d/Y", strtotime($_REQUEST['created']));
			$data = array(
               'cat' => $this->getPageCat(),
               'title' => $this->getPageTitle(),
               'content' => $_REQUEST['content'],
               //'slug' => $this->makeSlug(),
               'updated' => $update,
               'description' => $_REQUEST['description'],
               'date' => $created,
               'start' => $_REQUEST['start'],
               'end' => $_REQUEST['end'],
               'status' => $this->getStatus(),
               'author' => $this->author,
               'isGroup' => $group                    
		    );
			
			// if($_REQUEST['slug'] == ""){
				// $data['slug'] = $this->makeSlug();
			// }
			
			if(isset($_REQUEST['parent'])){
				//Update record to Versions table
				$this->crud->use_table('cms_pageversions'); 
				$insert1 = $this->crud->update(array('id'=>$id,'status'=>'pending'),$data, 0, 0, array('id' => 'DESC'));
				applyHooks::on_save($insert1);
			}else{
				if($this->needsMod == 'true'){
					$data['pageID'] = $id;
					$data['page_type'] = $_REQUEST['page_type'];
					$this->crud->use_table('cms_pages');
					$slug = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
					$this->crud->use_table('cms_pageversions'); 
					$data['slug'] = $slug->slug;
					$insert1 = $this->crud->create($data);
					applyHooks::on_save($insert1);
					$this->crud->use_table('cms_pages');
					$this->crud->update(array('id'=>$id,),array('hasDraft' => '1', 'author' => $this->author ), 0, 0, array('id' => 'DESC'));
				}else{
					$this->archiveVersion($id);
					$this->crud->use_table('cms_pages');
					$this->crud->update(array('id'=>$id),$data, 0, 0, array('id' => 'DESC'));
					applyHooks::on_save($id);
				}
			}
			
			
			//Approve the page
			if($this->input->post('ApprovePage')){
				$page = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
				$pageID = $page->pageID;
				if($this->auth->getLevel() == "4"){}else{$this->archiveVersion($id);}
				$this->moderation->approve($pageID);
			}
			
			
			//Mark for moderation
			if($this->input->post('submitPage')){
				$moderation = $this->configs->get('moderation');
				if($moderation == "true"){
					//$page = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
					if(isset($_REQUEST['parent'])){
						$modID = $_REQUEST['parent'];
					}else{
						$modID = $id;
					}	
					$this->moderation->mark($modID);}
			}
			
			
			//Set groups
			//$group  = $_REQUEST['makegroup'];
			if($group == 'true'){
				
				$parent = $_REQUEST['id'];
				if(isset($_REQUEST['parent'])) {
					$this->crud->use_table('cms_pageversions'); 
					$page = $this->crud->retrieve(array('id' => $id), 'row', 0, 0, array('id' => 'DESC'));
					$parent = $page->pageID;
				}
				$this->crud->use_table('cms_pagegroups');
				
				$this->crud->delete(array('group_id' => $parent), 0, 0, array('id' => 'DESC'));
				
				$pagess = $_REQUEST['pages'];
				$i = 2;
				foreach($pagess as $paged){
					$this->crud->create(array('page_id' => $paged, 'group_id' => $parent, 'order' => $i));
					$i++;
				}
			}
			
			
		
		
		
		//Log the event
		$this->logevents->logEvent($this->getPageCat()." Page Updated",$this->getPageTitle()." Updated in ".$this->getPageCat());	
	}

	function promote($id,$proId){
		$this->crud->use_table('cms_pages');
		//save current page as a version
		$old = $this->crud->retrieve(array('id'=>$id), 'row');
		
		if($old->updated){$verDate = $old->updated ; }else{$verDate = $old->date;}
		
		$version = array(
	               'pageID' => $old->id,
	               'title' => $old->title,
	               'content' => $old->content,
	               'slug' => $old->slug,
	               'cat' => $old->cat,
	               'author' => $old->author,
	               'versionDate' => $verDate,
	               'date' => $old->date,
	               'description' => $old->description,
	               'start' => $old->start,
	               'end' => $old->end,
	               'status' => 'published',
	               'page_type' => $old->page_type                     
	    );
		$this->crud->use_table('cms_pageVersions');
		$this->crud->create($version);
		
		//Promote the version to current
		$new = $this->crud->retrieve(array('id'=>$proId), 'row');
		
		$newversion = array(
	               'title' => $new->title,
	               'content' => $new->content,
	               'slug' => $new->slug,
	               'cat' => $new->cat,
	               'author' => $new->author,
	               'date' => $new->date,
	               'description' => $new->description,
	               'start' => $new->start,
	               'end' => $new->end,
	               'status' => 'published',
	               'page_type' => $new->page_type                         
	    );
		$this->crud->use_table('cms_pages');
		$this->crud->update(array('id'=>$id),$newversion);
		
		//Delete the promoted version
		$this->crud->use_table('cms_pageversions');
		$this->crud->delete(array('id' => $proId));
		
		//cleanup versions
		$oldLimit = $this->crud->count_all_where(array('pageID' => $id));
		$limit = $this->configs->get('version_limit');
		if($oldLimit > $limit){
			$delete = $oldLimit - $limit;
			$this->crud->delete(array('pageID' => $id), $delete);
		}
	}


	public function biuldList($base, $cat, $page_type = "page", $base_type = ""){
		$this->crud->use_table('cms_pages');
		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = base_url() .index_page()."/". $base;
        $config["total_rows"] = $this->crud->count_all_where(array('cat' => $cat, 'page_type' => $page_type));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
		$config['cur_tag_open'] = '<li class="active"><a href="#" >';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$links = $this->pagination->create_links();
		
		$pages = $this->crud->retrieve(array('cat' => $cat, 'page_type' => $page_type), '',$config["per_page"], $page);
		
		$this->template->set('links', $links);
		$this->template->set('pages', $pages);
		$this->template->set('type', $page_type);
		if($base_type != ""){
			$this->template->set('editpath', $base_type."/editPage");
			$this->template->set('addpath', $base_type."/addPage/".$page_type."/".$cat);
			$this->template->set('base_type', $cat);
		}else{
			$this->template->set('editpath', $cat."/editPage");
			$this->template->set('addpath', $cat."/addPage/".$page_type);	
		}		
		$this->template->set('deletepath', "pages_controller/delete");
	}
	
	public function buildGroupList($id){
		$query = $this->db->query('SELECT cms_pagegroups.id, cms_pages.title, cms_pages.slug, cms_pagegroups.page_id, cms_pagegroups.group_id, cms_pagegroups.order
						FROM cms_pages, cms_pagegroups  
						WHERE cms_pagegroups.group_id = "'.$id.'"
						AND cms_pagegroups.page_id = cms_pages.id
						ORDER BY cms_pagegroups.order
		');
		
		$menu = "";
		foreach($query->result() as $row) {
			$menu .= '<li class="dd-item" data-id="'.$row->id.'">';
			$menu .= '<div class="dd-handle">'.$row->title.'</div><div class="nestControls">'.confirm( "pages_controller/deleteGroupItem/".$row->id , "Delete", "Delete Page", "Are you sure you want to delete this Group Item?", "right" ) .' <a href="#" class="right editMenu" rel="'.$row->id.'" name="'.$row->title.'" link="'.$row->slug.'">Edit </a></div>';
			$menu .= '</li>';
		}
		return $menu;
	}
	
	public function getGroupList($id){
		$query = $this->db->query('SELECT cms_pagegroups.id, cms_pages.title, cms_pages.slug, cms_pagegroups.page_id, cms_pagegroups.group_id, cms_pagegroups.order
						FROM cms_pages, cms_pagegroups  
						WHERE cms_pagegroups.group_id = "'.$id.'"
						AND cms_pagegroups.page_id = cms_pages.id
						ORDER BY cms_pagegroups.order
		');
		
		return $query->result();
	}
	
	function getPage($id){
		$this->crud->use_table('cms_pages');
		$pages = $this->crud->retrieve(array('id' => $id), '', 0, 0, array('id' => 'DESC'));
		
		foreach($pages as $page){
			//Check For Draft
			if($page->hasDraft != ""){
				$this->crud->use_table('cms_pageversions');
				$pages = $this->crud->retrieve(array('pageID' => $id, 'status' => 'pending'), '', 0, 0, array('id' => 'DESC'));
			}
		}
		
		return $pages;
	}
	
	function getVersions($id){
		$this->crud->use_table('cms_pageversions');
		$query = $this->crud->retrieve(array('pageID' => $id, 'status' => 'published'), '', '','',array('versionDate' => 'DESC'));
		
		return $query;
	}


	public function addPage($cat = ""){
		$page_type = $this->uri->segment('3');
		$base_type = $this->uri->segment('4');
		if(isset($base_type) && $base_type != ""){
			$this->template->set('cat', $base_type);
			$this->template->set('base_type', $base_type);
		}else{
			$this->template->set('cat', $cat);
		}
		
		$this->template->set('type', $page_type);
		$this->template->set('path', "pages_controller/insert/".$cat."/".$base_type);
		$pages = $this->getPageList($cat);
		$this->template->set('pages', $pages);
		
	}
	
	public function editPage($cat){
		$page = $this->uri->segment(3);
		$pages = $this->getPage($page);
		foreach($pages as $page){
			$isGroup = $page->isGroup; 
			if(isset($page->pageID)){
				$pageID = $page->pageID;
			}else{
				$pageID = $page->id;
			}
		}
		
		if($this->configs->get('versioning') == "true" ){ 
			$versions = $this->getVersions($pageID);
			$this->template->set('versions',$versions);
			$this->template->set('promotepath', "pages_controller/promote");
		}
		
		if($isGroup == "true"){
			$group = $this->buildGroupList($pageID);
			$this->template->set('groupList', $group);
			$pageList = $this->getPageList($cat);
			$this->template->set('pageList', $pageList);
			
		}
		
		
		$this->template->set('cat', $cat);
		$this->template->set('pages', $pages);
		$this->template->set('path', "pages_controller/update");
		
	}
	
	public function getPageList($cat){
		$this->crud->use_table('cms_pages');
		return $this->crud->retrieve(array('cat' => $cat, 'status' => 'published', 'page_type' => 'page'), '', '','','');
	}

	

}
	