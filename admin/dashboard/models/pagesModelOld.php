<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Pages 
 * 
 * @package Pages Model for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
class pagesModel extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->crud->use_table('cms_pages');
    }
	
	
	
	function getPageType(){
		return $_REQUEST['type'];
	}
	
	function getPageTitle(){
		return  $_REQUEST['title'];
	}
	
	function getPageCat(){
		return $_REQUEST['cat'];
	}
	
	function getStatus(){
		$moderation = $this->configs->get('moderation');
		if($moderation == "true"){$status = "pending";}else{$status = "published";}
		return $status;
	}
	
	function makeSlug(){
		$title = $this->getPageTitle();
		$slug  = str_replace(" ","_",$title);
		$slug = strtolower($slug);
		$query = $this->db->get_where('cms_pages', array('title' => $title));
		$num = $query->num_rows();
		//Itirate slug for dups
		if($num != 0){  $it = $num + 1; $slug = $slug.$it; }
		return $slug;
	}
	
	function savePage(){
		
		//Handle regular Pages
		if ($this->input->post('pageSubmit')){
			$last = $this->insertPage();
		}
		//Handle PageGroups
		if ($this->input->post('groupSubmit')){
			$last = $this->insertGroup();
		}
		
		//Log the event
		$this->logevents->logEvent($this->getPageCat()." Page Added",$this->getPageTitle()." Added in ".$this->getPageCat());
		//handle modiration
		$moderation = $this->configs->get('moderation');
		if($moderation == "true"){$this->moderation->mark($last);}else{$status = "published";}
	}	
	
	function insertPage(){
		$content = $_REQUEST['content'];	
		$data = array(
           'cat' => $this->getPageCat(),
           'title' => $this->getPageTitle(),
           'content' => $content,
           'slug' => $this->makeSlug(),
           'page_type' => $this->getPageType(),
           'status' => $this->getStatus()                     
	    );
		$this->db->insert('cms_pages', $data); 
		return $this->db->insert_id();
	}
	
	function insertGroup(){
		$data = array(
           'cat' => $this->getPageCat(),
   		   'title' => $this->getPageTitle(),
           'isGroup' => 'true',
           'slug' => $this->makeSlug,
   		   'page_type' => $this->getPageType(),
   		   'status' => $this->getStatus()                       
	    );
		$this->db->insert('cms_pages', $data); 
		$last = $this->db->insert_id();
		
		$start = $_REQUEST['defaultPage'];
		$this->crud->use_table('cms_pagegroups');
		$this->crud->create(array('page_id' => $start, 'group_id' => $last, 'order' => '1'));
		$pages = $_REQUEST['pages'];
		$i = 2;
		foreach($pages as $page){
			$this->crud->create(array('page_id' => $page, 'group_id' => $last, 'order' => $i));
			$i++;
		}
		return $last;
	}
	
	function update($id){
		
		$update = date('Y-m-d H:i:s');
		
		if ($this->input->post('RejectPage')) {
			$this->moderation->reject($id);
		}
		
		$status = $this->getStatus();
		if ($this->input->post('submitPage') || $this->input->post('ApprovePage')){
			//Save old Version

			$query = $this->db->get_where('cms_pages', array('id' => $id));
			$olds = $query->result();
			
			if(!$this->input->post('ApprovePage')) {
				foreach($olds as $old){
					if($old->updated){$verDate = $old->updated ; }else{$verDate = $old->date;}
				
					$version = array(
				               'pageID' => $old->id,
				               'title' => $old->title,
				               'content' => $old->content,
				               'slug' => $old->slug,
				               'cat' => $old->cat,
				               'author' => $old->author,
				               'versionDate' => $verDate,
				               'date' => $old->date,
				               'description' => $old->description,
				               'start' => $old->start,
				               'end' => $old->end                        
				    );
					$this->crud->use_table('cms_pageVersions');
					$this->crud->create($version);
					
					
					$oldLimit = $this->crud->count_all_where(array('pageID' => $old->id));
					$limit = $this->configs->get('version_limit');
					if($oldLimit > $limit){
						$delete = $oldLimit - $limit;
						$this->crud->delete(array('pageID' => $old->id), $delete);
					}
				}
			}
			
			
			//Update record
			$cat = $_REQUEST['cat'];
			$title = $_REQUEST['title'];
			$content = $_REQUEST['content'];	
			$slug = $_REQUEST['slug'];
			$description = $_REQUEST['description'];
			$start = $_REQUEST['start'];
			$end = $_REQUEST['end'];
			
			$slug  = str_replace(" ","_",$slug);
			$slug = strtolower($slug);
				
			$data = array(
		               'cat' => $cat,
		               'title' => $title,
		               'content' => $content,
		               'slug' => $slug,
		               'updated' => $update,
		               'description' => $description,
		               'start' => $start,
		               'end' => $end,
		               'status' => $status                   
		    );
			
			$this->db->where('id', $id);
			$this->db->update('cms_pages', $data); 
			
			if($this->input->post('ApprovePage')){
				$this->moderation->approve($id);
			}
			
			if($this->input->post('submitPage')){
				if($moderation == "true"){$this->moderation->mark($id);}else{$status = "published";}
			}
			
			
		}
		
		if ($this->input->post('submitGroup')){
			//Update record
			$cat = $_REQUEST['cat'];
			$title = $_REQUEST['title'];	
			$slug = $_REQUEST['slug'];
			$description = $_REQUEST['description'];
			$start = $_REQUEST['start'];
			$end = $_REQUEST['end'];
			
			$slug  = str_replace(" ","_",$slug);
			$slug = strtolower($slug);
				
			$data = array(
		               'cat' => $cat,
		               'title' => $title,		               
		               'slug' => $slug,
		               'updated' => $update,
		               'description' => $description,
		               'start' => $start,
		               'end' => $end,
		                'status' => 'published'                   
		    );
			
			$this->db->where('id', $id);
			$this->db->update('cms_pages', $data);
			
		}
		
		
		
		//Log the event
		$this->logevents->logEvent($cat." Page Updated",$title." Updated in ".$cat);	
	}

	function promote($id,$proId){
		
		//save current page as a version
		$old = $this->crud->retrieve(array('id'=>$id), 'row');
		
		if($old->updated){$verDate = $old->updated ; }else{$verDate = $old->date;}
		
		$version = array(
	               'pageID' => $old->id,
	               'title' => $old->title,
	               'content' => $old->content,
	               'slug' => $old->slug,
	               'cat' => $old->cat,
	               'author' => $old->author,
	               'versionDate' => $verDate,
	               'date' => $old->date,
	               'description' => $old->description,
	               'start' => $old->start,
	               'end' => $old->end                       
	    );
		$this->crud->use_table('cms_pageVersions');
		$this->crud->create($version);
		
		//Promote the version to current
		$new = $this->crud->retrieve(array('id'=>$proId), 'row');
		
		$newversion = array(
	               'title' => $new->title,
	               'content' => $new->content,
	               'slug' => $new->slug,
	               'cat' => $new->cat,
	               'author' => $new->author,
	               'date' => $new->date,
	               'description' => $new->description,
	               'start' => $new->start,
	               'end' => $new->end                         
	    );
		$this->crud->use_table('cms_pages');
		$this->crud->update(array('id'=>$id),$newversion);
		
		//Delete the promoted version
		$this->crud->use_table('cms_pageVersions');
		$this->crud->delete(array('id' => $proId));
		
		//cleanup versions
		$oldLimit = $this->crud->count_all_where(array('pageID' => $id));
		$limit = $this->configs->get('version_limit');
		if($oldLimit > $limit){
			$delete = $oldLimit - $limit;
			$this->crud->delete(array('pageID' => $id), $delete);
		}
	}


	public function biuldList($base, $cat){
		$this->crud->use_table('cms_pages');
		$this->load->library("pagination");
		$config = array();
        $config["base_url"] = base_url() .index_page()."/". $base;
        $config["total_rows"] = $this->crud->count_all_where(array('cat' => $cat));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
		$config['cur_tag_open'] = '<li class="active"><a href="#" >';
		$config['cur_tag_close'] = '</a></li>';
		
        $this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$links = $this->pagination->create_links();
		
		$pages = $this->crud->retrieve(array('cat' => $cat, 'page_type' => 'page'), '',$config["per_page"], $page);
		
		$this->template->set('links', $links);
		$this->template->set('pages', $pages);
		
		$this->template->set('editpath', $cat."/editPage");
		$this->template->set('addpath', $cat."/addPage");
		$this->template->set('deletepath', "pages_controller/delete");
	}
	
	public function buildGroupList($id){
		$query = $this->db->query('SELECT cms_pagegroups.id, cms_pages.title, cms_pages.slug, cms_pagegroups.page_id, cms_pagegroups.group_id, cms_pagegroups.order
						FROM cms_pages, cms_pagegroups  
						WHERE cms_pagegroups.group_id = "'.$id.'"
						AND cms_pagegroups.page_id = cms_pages.id
						ORDER BY cms_pagegroups.order
		');
		
		$menu = "";
		foreach($query->result() as $row) {
			$menu .= '<li class="dd-item" data-id="'.$row->id.'">';
			$menu .= '<div class="dd-handle">'.$row->title.'</div><div class="nestControls">'.confirm( "pages_controller/deleteGroupItem/".$row->id , "Delete", "Delete Page", "Are you sure you want to delete this Group Item?", "right" ) .' <a href="#" class="right editMenu" rel="'.$row->id.'" name="'.$row->title.'" link="'.$row->slug.'">Edit </a></div>';
			$menu .= '</li>';
		}
		return $menu;
	}
	
	function getPage($id){
		$query = $this->db->get_where('cms_pages', array('id' => $id));
		return $query->result();
	}
	
	function getVersions($id){
		$this->crud->use_table('cms_pageVersions');
		$query = $this->crud->retrieve(array('pageID' => $id), '', '','',array('versionDate' => 'DESC'));
		
		return $query;
	}


	public function addPage($cat){
		$this->template->set('cat', $cat);
		$this->template->set('type', 'page');
		$this->template->set('path', "pages_controller/insert/".$cat);
		$pages = $this->getPageList($cat);
		$this->template->set('pages', $pages);
		
	}
	
	public function editPage($cat){
		$page = $this->uri->segment(3);
		$pages = $this->getPage($page);
		foreach($pages as $page){$isGroup = $page->isGroup; $pageID = $page->id;}
		
		if($this->configs->get('versioning') == "true" && $isGroup != "true"){ 
			$versions = $this->getVersions($pageID);
			$this->template->set('versions',$versions);
			$this->template->set('promotepath', "pages_controller/promote");
		}
		
		if($isGroup == "true"){
			$group = $this->buildGroupList($pageID);
			$this->template->set('groupList', $group);
			$pageList = $this->getPageList($cat);
			$this->template->set('pageList', $pageList);
			
		}
		
		
		$this->template->set('cat', $cat);
		$this->template->set('pages', $pages);
		$this->template->set('path', "pages_controller/update");
		
	}
	
	public function getPageList($cat){
		$this->crud->use_table('cms_pages');
		return $this->crud->retrieve(array('cat' => $cat, 'status' => 'published', 'page_type' => 'page'), '', '','','');
	}

	

}
	