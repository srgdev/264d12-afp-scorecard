<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * moderation 
 * 
 * @package moderation modules   for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
 
class moderation extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
		$this->crud->use_table('cms_moderation');
    }
	
	
	public function mark($pageID){
		
		$this->crud->use_table('cms_pages');
		$page = $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC'));
		$section = $page->cat;
		$this->crud->update(array('id' => $pageID),array('hasDraft' => '1'), 0, 0, array('id' => 'DESC'));
		$this->crud->use_table('cms_moderation');
		$this->crud->delete(array('page_id' => $pageID ), 0, 0, array('id' => 'DESC'));
		$this->crud->create(array('page_id' => $pageID, 'section' => $section));
	}
	
	public function approve($pageID){
		 $this->crud->use_table('cms_moderation');
		 if($this->auth->getLevel() == "3"){
		 	$this->crud->update(array('page_id' => $pageID),array('section_mod' => 'true'), 0, 0, array('id' => 'DESC'));
		 }else{
		 	$this->publish($pageID);
		 	$this->crud->use_table('cms_moderation');
		 	$this->crud->delete(array('page_id' => $pageID ), 0, 0, array('id' => 'DESC'));
		 }
	}

	public function publish($pageID) {
		$this->pagesModel->archiveVersion($pageID);
		$this->crud->use_table('cms_pageVersions');
		$new = $this->crud->retrieve(array('pageID' => $pageID, 'status' => 'pending'), 'row', 0, 0, array('id' => 'DESC'));
		$update = date('Y-m-d H:i:s');
		$data = array(
           'title' => $new->title,
           'content' => $new->content,
           'slug' => $new->slug,
           'updated' => $update,
           'description' => $new->description,
           'start' => $new->start,
           'end' => $new->end,
           'status' => 'published',
           'hasDraft' => ''                   
	    );
		$this->crud->use_table('cms_pages');
		$this->crud->update(array('id'=>$pageID),$data, 0, 0, array('id' => 'DESC'));
		$this->crud->use_table('cms_pageVersions');
		$this->crud->delete(array('pageID' => $pageID, 'status' => 'pending' ), 0, 0, array('id' => 'DESC'));
	}

	public function markSection($pageID) {
		$this->crud->update(array('page_id' => $pageID),array('section_mod' => 'true'), 0, 0, array('id' => 'DESC'));
	}
	
	public function reject($pageID){
		$this->crud->use_table('cms_pageVersions');
		$row = $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC'));
		$pageID = $row->pageID;
		$this->crud->use_table('cms_pages');
		$this->crud->update(array('id'=>$pageID),array('hasDraft' => '2'), 0, 0, array('id' => 'DESC'));
		$this->crud->use_table('cms_moderation');
		$this->crud->delete(array('page_id' => $pageID ), 0, 0, array('id' => 'DESC'));
		$this->notifyAuth($pageID , 'reject');
		$this->notifySecMod($pageID, 'reject');
	}
	
	public function notifyAuth($pageID, $type){
		$this->crud->use_table('cms_pages');
		$page = $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC'));
		//Email Auth
		if($type == 'approved'){
		    $message = 'A page you Authored has been approved by the moderator/n/n Page: '.$page->title;
		}
		elseif($type == 'reject'){
			$message = 'A page you Authored has been rejected by the moderator/n/n Page: '.$page->title;
		} 
	}
	
	public function notifySecMod($pageID, $type){
		 $this->crud->use_table('cms_pages');
		 $page = $this->crud->retrieve(array('id' => $pageID), 'row', 0, 0, array('id' => 'DESC'));
		 
		//Email Section Mod	
		if ($type == "notify"){
		    $message = 'A page is ready for your moderation/n/n Page: '.$page->title;	
		}
		elseif($type == 'reject'){
			$message = 'A page you moderated has been rejected by the global moderator and returned to the Author/n/n Page: '.$page->title;
		}
	}
	
	public function notifyMod($pageID){
		 $this->crud->use_table('cms_pages');
		 $this->crud->update(array('id' => $pageID),array('status' => 'published'), 0, 0, array('id' => 'DESC'));
		//Email Global Mod
		$message = 'A page is ready for your moderation/n/n Page: '.$page->title;
	}
}
	