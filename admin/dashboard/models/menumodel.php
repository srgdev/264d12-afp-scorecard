<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * modules 
 * 
 * @package modules manager  for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
 
class menuModel extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
	
  
  function menu($cat){
  	$this->crud->use_table('cms_menus');
	
	$rows = $this->crud->retrieve( array('isChild' => 'false', 'module' => $cat ), '',0, 0, array('order' => 'ASC'));
	$menu = "";
	foreach($rows as $row) {
		$menu .= '<li class="dd-item" data-id="'.$row->id.'">';
		$menu .= '<div class="dd-handle">'.$row->name.'</div><div class="nestControls">'.confirm( "menus/delete/".$row->id."/".$row->name , "Delete", "Delete Page", "Are you sure you want to delete this menu Header? All Sub Menus WILL BE DELETED!", "right" ) .' <a href="#" class="right editMenu" rel="'.$row->id.'" name="'.$row->name.'" link="'.$row->link.'">Edit </a></div>';
		if($row->isParent == 'true'){
			$id = $row->id;
			$children = $this->crud->retrieve( array('parent' => $id), '',0, 0, array('order' => 'ASC'));
			$menu .= ' <ol class="dd-list">';
			foreach($children as $child) {
				$menu .= '<li class="dd-item" data-id="'.$child->id.'">';
				$menu .= '<div class="dd-handle">'.$child->name.'</div><div class="nestControls">'.confirm( "menus/delete/".$child->id."/".$child->name , "Delete", "Delete Page", "Are you sure you want to delete this menu item?", "right" ) .' <a href="#"class="right editMenu" rel="'.$child->id.'" name="'.$child->name.'" link="'.$child->link.'">Edit </a></div>';
				$menu .= '</li>';
			}
			$menu .= '</ol>';
		}
		$menu .= '</li>';
	}
	$this->template->set('table', $menu);
	$parents = $rows = $this->crud->retrieve( array('isParent' => 'true', 'module' => $cat ), '',0, 0, array('order' => 'ASC'));
	$pageList = $this->pagesModel->getPageList($this->cat);
	$this->template->set('parents', $parents);
	$this->template->set('pages', $pageList);
  }
}
?>