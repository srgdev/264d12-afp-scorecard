<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Users 
 * 
 * @package Usr Auth  for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
class users extends CI_Model
{
  
  function __construct()
    {
        parent::__construct();
    }
	
  function getAll(){
  	
	// SELECT buyer_name, quantity, product_name FROM buyers LEFT JOIN products ON
 // buyers.pid=products.id;
 	$where = '';
 	if(isset($_REQUEST['filter'])) {
 		if($_REQUEST['filter'] != "All"){
 			$where  ='WHERE cms_acl.id = "'.$_REQUEST['filter'].'"';
 		}
 	}
  	$query = $this->db->query('SELECT cms_users.id, cms_users.name, cms_users.username, cms_users.email, cms_acl.acl_name FROM cms_users LEFT JOIN cms_acl ON cms_users.acl=cms_acl.id '.$where);
	return $query->result();
  }
  
  function getUser($id){
  	$query = $this->db->get_where('cms_users', array('id' => $id));
	return $query->result();
  }
  
  function update(){
  	$name = $_REQUEST['name'];
	$username = $_REQUEST['username'];
	$email = $_REQUEST['email'];
	$id = $_REQUEST['id'];
	$password = $_REQUEST['pass'];
	
	$acl = $_REQUEST['acl'];
	
	$acgList = "";
	
	if(isset($_REQUEST['acg'])){
		$acg = $_REQUEST['acg'];
		
		foreach($acg as $group){
			$acgList .= $group.",";
		}
		
	}
	
	if($acl == "1" || $acl == "2"){
			$acgList = "all";
	}
	//$password = do_hash($password, 'md5');
	$data = array(
               'name' => $name,
               'email' => $email,
               'acl' => $acl,
               'acg' => $acgList
                //'password' => $password        
            );
	
	if($password != ""){
		$password = do_hash($password, 'md5');
		$data['password'] = $password;
	}
	
	$this->db->where('id', $id);
	$this->db->update('cms_users', $data); 
	
	
	
	//Log the event
	$this->logevents->logEvent("User Updated","User ".$username." Updated");
	
	
	
  }
  
  function insert(){
  	$name = $_REQUEST['name'];
	$username = $_REQUEST['username'];
	$email = $_REQUEST['email'];	
	$password = $_REQUEST['pass'];
	$acl = $_REQUEST['acl'];
	
	$acgList = "";
	
	if(isset($_REQUEST['acg'])){
		$acg = $_REQUEST['acg'];
		
		foreach($acg as $group){
			$acgList .= $group.",";
		}
		
	}
	
	if($acl == "1" || $acl == "2"){
			$acgList = "all";
	}
	
	$password = do_hash($password, 'md5');
	//Log the event
	$this->logevents->logEvent("User Added","User ".$username." Added");
	
	$data = array(
               'name' => $name,
               'username' => $username,
               'email' => $email,
               'password' => $password,
               'acl' => $acl,
               'acg' => $acgList         
            );
			
	$this->db->insert('cms_users', $data); 
	
	if(isset($_REQUEST['sendEmail'])){
		$subject = "Your account has been created in the CMS";
		$message = "Your account has been set up in the CMS \r\n \r\n Username: ".$username."\r\n Password: ".$_REQUEST['pass']."\r\n \r\n Please log in here ".base_url().index_page();
		$this->cmsemail->sendMail($email,'',$subject,$message);
	}
	
	return $this->db->insert_id();
  }
  
  function delete($id){
  	$query = $this->db->get_where('cms_users', array('id' => $id));
	
	$row = $query->row(); 
	
	$username = $row->username;
	
  	$this->db->where('id', $id);
	$this->db->delete('cms_users');
	
	//Log the event
	$this->logevents->logEvent("User Deleted","User ".$username." Deleted");
	
  }
  
  function search(){
  	$term = $_REQUEST['term'];
	$query = $this->db->query("SELECT cms_users.id, cms_users.name, cms_users.username, cms_users.email, cms_acl.acl_name FROM cms_users LEFT JOIN cms_acl ON cms_users.acl=cms_acl.id WHERE cms_users.name LIKE '%$term%' or cms_users.username LIKE '$term'");
	//$query = $this->db->query("SELECT * from cms_users WHERE name LIKE '%$term%' or username LIKE '$term' ");
	$num = $query->num_rows();
	if($num != 0){
		return $query->result();
	}else{
		return "no";
	}
  }
  
  function permissions(){
  	return "sadmin/Super Admin,admin/Admin,faq/FAQ User,archive/Archive User";
  }
  
  function checkUsername($user) {
  	$query = $this->db->get_where('cms_users', array('username' => $user));
	$num = $query->num_rows();
	if($num > 0){
		return FALSE;
	}else{
		return TRUE;
	}
  }
  
  function checkPassword($pass){
  	$user = $this->auth->getUserName();
	$pass5 = do_hash($pass, 'md5');
	$query = $this->db->get_where('cms_users', array('username' => $user, 'password' => $pass5));
	$num = $query->num_rows();
	if($num > 0){
		
		return TRUE;
	}else{
		return FALSE;
	}
  }
 
}
?>