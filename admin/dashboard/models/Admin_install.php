<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
Copyright (c) 2012, Stoneridge Group
All rights reserved.
*/

/**
 * Users 
 * 
 * @package Usr Auth  for SRG CMS 
 * @copyright Copyright (c) 2012, Stoneridge Group
 * @author Paul Radich @ Stoneridge Group
 */
 
class Admin_install extends CI_Model
{
  
	 function __construct()
    {
        parent::__construct();
    }
	
	function installer(){
		
			$this->db->query("CREATE TABLE IF NOT EXISTS `cms_modules` (
			  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
			  `module` varchar(255) NOT NULL,
			  `acl` varchar(5) NOT NULL,
			  `acg` varchar(255) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
				
		echo "cms_modules Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_navigation` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `module` varchar(255) NOT NULL,
		  `parent` varchar(255) NOT NULL,
		  `title` varchar(255) NOT NULL,
		  `helpText` varchar(255) NOT NULL,
		  `link` varchar(255) NOT NULL,
		  `icon` varchar(100) DEFAULT NULL,
		  `active` varchar(255) NOT NULL,
		  `uri` varchar(5) NOT NULL,
		  `acl` varchar(255) NOT NULL,
		  `acg` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_navigation Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_log` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `action` varchar(255) NOT NULL,
		  `desc` varchar(255) NOT NULL,
		  `user` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_log Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_config` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `key` varchar(255) NOT NULL,
		  `value` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_config Intalled";
		echo "<br/>";
		
		$this->crud->use_table('cms_config');
		$configCount = $this->crud->count_all('cms_config');
		if($configCount == 0){
			$this->db->query("INSERT INTO `cms_config` VALUES(3, 'logging', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(4, 'googleAnalytics', 'false')");
			$this->db->query("INSERT INTO `cms_config` VALUES(5, 'analyticsID', '')");
			$this->db->query("INSERT INTO `cms_config` VALUES(7, 'logKeepDays', '11')");
			$this->db->query("INSERT INTO `cms_config` VALUES(8, 'version_limit', '5')");
			$this->db->query("INSERT INTO `cms_config` VALUES(9, 'versioning', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(10, 'useGroups', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(11, 'use_smtp', 'false')");
			$this->db->query("INSERT INTO `cms_config` VALUES(12, 'SMTP', '')");
			$this->db->query("INSERT INTO `cms_config` VALUES(13, 'send_sysMes', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(14, 'sysmes_to', 'psradich@gmail.com')");
			$this->db->query("INSERT INTO `cms_config` VALUES(15, 'mes_from', 'info@cms.com')");
			$this->db->query("INSERT INTO `cms_config` VALUES(16, 'moderation', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(17, 'globalModeration', 'true')");
			$this->db->query("INSERT INTO `cms_config` VALUES(18, 'groupModeration', 'false')");
						
		}
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_users` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL,
		  `email` varchar(255) NOT NULL,
		  `username` varchar(255) NOT NULL,
		  `password` varchar(255) NOT NULL,
		  `acl` varchar(255) NOT NULL,
		  `acg` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		$this->crud->use_table('cms_users');
		$userCount = $this->crud->count_all('cms_users');
		if($userCount == 0){
			$this->db->query("INSERT INTO `cms_users` VALUES(1, 'admin', 'admin', 'admin', '".do_hash('admin', 'md5')."', '1', 'all')");
			$this->db->query("INSERT INTO `cms_users` VALUES(2, 'global', 'paul@stoneridgegroup.com', 'global', 'c144e818facc37605b4c31b8056b3f93', '0', 'all')");
		}
		
		echo "cms_users Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_pageVersions` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `pageID` varchar(255) NOT NULL,
		  `title` varchar(255) NOT NULL,
		  `content` blob NOT NULL,
		  `slug` varchar(255) NOT NULL,
		  `cat` varchar(255) NOT NULL,
		  `author` varchar(255) NOT NULL,
		  `date` varchar(255) DEFAULT NULL,
		  `versionDate` varchar(255) NOT NULL,
		  `updated` varchar(255) NOT NULL,
		  `start` varchar(255) NOT NULL,
		  `end` varchar(255) NOT NULL,
		  `description` varchar(255) NOT NULL,
		  `status` varchar(100) NOT NULL,
		  `page_type` varchar(100) NOT NULL,
		  `isGroup` varchar(100) NOT NULL,
		  `hasSidebar` varchar(100) NOT NULL,
		  `hasIframe` varchar(255) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");		
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_pages` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `title` varchar(255) NOT NULL,
		  `content` blob NOT NULL,
		  `slug` varchar(255) NOT NULL,
		  `cat` varchar(255) NOT NULL,
		  `author` varchar(255) NOT NULL,
		  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `updated` varchar(255) DEFAULT NULL,
		  `start` varchar(255) NOT NULL,
		  `end` varchar(255) NOT NULL,
		  `description` varchar(255) NOT NULL,
		  `status` varchar(255) NOT NULL DEFAULT 'draft',
		  `page_type` varchar(255) NOT NULL DEFAULT 'page',
		  `isGroup` varchar(10) NOT NULL,
		  `hasDraft` varchar(100) NOT NULL,
		  `hasSidebar` varchar(100) NOT NULL,
		  `hasIframe` varchar(255) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_pages and cms_pageVersions Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_acl` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `acl_name` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		$this->crud->use_table('cms_acl');
		$aclCount = $this->crud->count_all('cms_acl');
		if($aclCount == 0){
			$this->db->query("INSERT INTO `cms_acl` VALUES(1, 'Super Admin');");
			$this->db->query("INSERT INTO `cms_acl` VALUES(2, 'Admin');");
			$this->db->query("INSERT INTO `cms_acl` VALUES(3, 'Moderator');");
			$this->db->query("INSERT INTO `cms_acl` VALUES(4, 'User');");
		}
		
		echo "cms_acl Intalled";
		echo "<br/>";


		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_acg` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `group` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_acg Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_images` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `image` varchar(255) NOT NULL,
		  `thumb` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_images Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_files` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `file` varchar(255) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
		
		echo "cms_files Intalled";
		echo "<br/>";
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_moderation` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `page_id` varchar(100) NOT NULL,
		  `type` varchar(100) NOT NULL DEFAULT 'page',
		  `section_mod` varchar(5) NOT NULL,
		  `global_mod` varchar(5) NOT NULL,
		  `section` varchar(100) NOT NULL,
		  `rejected` varchar(100) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;");
		
		echo "cms_moderation Intalled";
		echo "<br/>";
		
		
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_menus` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `module` varchar(30) NOT NULL,
		  `name` varchar(255) NOT NULL,
		  `link` varchar(255) NOT NULL,
		  `order` varchar(5) NOT NULL,
		  `parent` varchar(5) NOT NULL,
		  `isParent` varchar(20) NOT NULL,
		  `isChild` varchar(20) NOT NULL,
		  `published` varchar(255) NOT NULL DEFAULT 'fasle',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;");
		
		echo "cms_menus Intalled";
		echo "<br/>";
		
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `cms_pagegroups` (
		  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
		  `page_id` varchar(100) NOT NULL,
		  `group_id` varchar(100) NOT NULL,
		  `name_overide` varchar(255) NOT NULL,
		  `order` varchar(100) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
		
		echo "cms_pagegroups Intalled";
		echo "<br/>";
	}
	
}