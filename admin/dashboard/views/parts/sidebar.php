<?php ?>



<!-- <ul class="nav nav-list">
	
	<li <?php if($this->uri->segment(1) === "admin" && $this->uri->segment(2) != "logs"){ echo "class=\"active\""; } ?> >
		<?php echo anchor("admin/dash", "<i class='icon-home'></i> Dashboard", 'title="Dashboard"'); ?>
	</li>
</ul> -->
	<?php
	
	$data =  $this->nav_model->makeNav();
	
	if ($data) {
		
	
	for($i = 0; $i < count($data); $i++){?>
		<div class="navHead">
			<?php $title = str_replace('_', ' ', $data[$i]['parent']); ?>
			<div class="navTitle"><?php echo $title; ?></div>
			<div class="navInner <?php if($this->uri->segment(1) === $data[$i]['parent'] ) {echo "active";} ?>">
				<ul class="nav nav-list">
		
		<?php
		for($j = 0; $j < count($data[$i]['children']); $j++){?>
			
			<li <?php if($this->uri->segment($data[$i]['children'][$j]['uri']) === $data[$i]['children'][$j]['active'] && $this->uri->segment(1) === $data[$i]['parent']){ echo "class=\"active\""; } ?> > <?php
				$link = $data[$i]['children'][$j]['link'];
				$title = $data[$i]['children'][$j]['title'];
				$icon = $data[$i]['children'][$j]['icon'];
				$helpText = $data[$i]['children'][$j]['helpText'];
				?>
			<?php echo anchor($link, "<i class='icon-".$icon."'></i> ".$title."", 'title="'.$helpText.'"'); ?>
			</li>
		<?php } ?>
			</ul>
			</div>
		</div>
		<?php
	}
	}
	
	 ?>

	
	
	
	
	
	
	
	<?php if($this->auth->checkLevel("1")): ?>
		<!-- <div class="navHead">
			<div class="navTitle">Configuration</div>
			<div class="navInner <?php if($this->uri->segment(1) === "adminusers" || $this->uri->segment(1) === "settings" || $this->uri->segment(2) === "logs") {echo "active";} ?>">
				<ul class="nav nav-list">
				<li <?php if($this->uri->segment(1) === "adminusers"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("adminusers/allusers", "<i class='icon-user'></i> Users", 'title="Manage Users"'); ?>
				</li>
				<li <?php if($this->uri->segment(1) === "settings" && $this->uri->segment(2) === "index"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("settings/index", "<i class='icon-cog'></i> Settings", 'title="System Settings"'); ?>
				</li>
				<li <?php if($this->uri->segment(2) === "logs"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("admin/logs", "<i class='icon-file'></i> Event Logs", 'title="Event Logs"'); ?>
				</li>
				</ul>
			</div>
		</div> -->
		<?php endif; ?>
		<?php if($this->auth->checkLevel("0")){ ?>
		<div class="navHead">
			<div class="navTitle">System Settings</div>
			<div class="navInner <?php if($this->uri->segment(1) === "mange_modules" || $this->uri->segment(2) === "global_settings"){ echo "active"; } ?>">
				<ul class="nav nav-list">
				<li <?php if($this->uri->segment(1) === "mange_modules"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("mange_modules", "<i class='icon-wrench'></i> Modules", 'title="Manage Modules"'); ?>
				</li>
				<li <?php if($this->uri->segment(2) === "global_settings"){ echo "class=\"active\""; } ?> >
					<?php echo anchor("admin/global_settings", "<i class='icon-cog'></i> System Settings", 'title="System Settings"'); ?>
				</li>
				</ul>
			</div>
		</div>
		
	<?php } ?>
