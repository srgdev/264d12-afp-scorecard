<?php
$secMod  =  $this->configs->get('groupModeration');

if($secMod == "true"){
	$group = $this->auth->getACG();
	$this->crud->use_table('cms_moderation');
	$group  = str_replace(",", " ", $group);
	$group = explode(" ", $group);
	
	$count = 0;
	foreach ($group as $section){
	 $num = $this->crud->count_all_where(array('section' => $section, 'section_mod' => null));
	 $count = $count + $num;
	}
	
	if($count > 0){$count = "<span class='badge badge-important'>".$count."</span>";}else{$count = "<span class='badge'>".$count."</span>";}
	?>
	<li <?php if($this->uri->segment(2) === "moderation"){ echo "class=\"active\""; } ?> >
		<?php echo anchor("admin/sectionModeration", "<i class='icon-check icon-white'></i> Moderation ".$count, 'title="Event Logs"'); ?>
	</li>

<?php } ?>