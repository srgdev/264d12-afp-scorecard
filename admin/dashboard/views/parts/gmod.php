<?php

$secMod  =  $this->configs->get('groupModeration');
$this->crud->use_table('cms_moderation');


//No Group Moderatoin
if($secMod == "false"){	
	$count = $this->crud->count_all();
}
else{
	$count = $this->crud->count_all_where(array('section_mod' => 'true'));	
}

if($count > 0){$count = "<span class='badge badge-important'>".$count."</span>";}else{$count = "<span class='badge'>".$count."</span>";}
?>
<li <?php if($this->uri->segment(2) === "moderation"){ echo "class=\"active\""; } ?> >
	<?php echo anchor("admin/globalModeration", "<i class='icon-check icon-white'></i> Moderation ".$count, 'title="Event Logs"'); ?>
</li>