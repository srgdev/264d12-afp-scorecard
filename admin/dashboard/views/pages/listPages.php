<h1 class="left"><span class="ifont">K </span>Pages in <?php echo $title; ?> <?php echo anchor($addpath, "<i class='icon-plus'></i> Add ".ucfirst($type), 'class="btn "'); ?></h1>


 <!-- <?php 
 	$attributes = array('class' => 'form-search right');

	echo form_open('adminusers/searchUsers', $attributes);
?>
    <input type="text" class="input-medium search-query" name="term" id="term" placeholder="Search">
    <input type="submit" name="mysubmit" value="Search" class="btn" />
    </form>-->
    <br class="clear" /> 

<?php if($this->session->flashdata('flash')) { ?>
<div class="alert alert-info">
    	<?php echo $this->session->flashdata('flash'); ?>
</div>
<?php } ?>

<table  class="table table-striped ">
		<thead>
		<tr>
			<th>Title</th>
			<th>Author</th>
			<!-- <th>Slug</th> -->
			<th>State</th>
			<th>Date</th>		
		</tr>
		</thead>
<?php foreach($pages as $page){?>
	<?php $status = $page->status;
		$state = "";
		if($status == "published") { $state = "label-success";} 
		if($status == "rejected") { $state = "label-important";} 
	 ?>
	 <?php $draft = ''; ?>
	 <?php if(isset($page->hasDraft)) {
	 	if($page->hasDraft == "1"){	
	 		$draft = '<span class="label label-warning">Update Pending</span>';
		}elseif($page->hasDraft == "2"){
			$draft = '<span class="label label-important">Update Rejected</span>';
		} 
	 } ?>
	<tr>
		
		<td>
			<?php echo stripslashes($page->title); ?>
			<div class="editsOuter">
				<div class="edits">
					<?php $dtitle = str_replace("&", "", $page->title); ?>
					<?php $editID = $page->id;
						  if(isset($base_type)){$editID = $editID."/".$base_type;}
					?>
					<i class="icon-pencil"></i> <?php echo anchor($editpath."/".$editID, "Edit", 'title="Edit Page"'); ?> | <i class="icon-remove"></i> <?php echo confirm($deletepath."/".$page->id."/".$page->cat."/","Delete","Delete Page","Permently Delete ".addslashes($page->title)."?"); ?>
				</div>
			</div>
		</td>
		<td><?php echo $page->author; ?></td>
		<!-- <td><?php echo $page->slug; ?></td> -->
		<td><span class="label <?php echo $state ?>"><?php echo $status ?></span><?php echo $draft ?></td>
		<td><?php echo $page->date; ?></td>
	</tr>
<?php } ?>
</table>

<div class="pagination">
<ul>
	<?php echo $links ?>
</ul>
</div>