<?php foreach($pages as $page){ ?>
<h1><span class="ifont">e </span>Edit <?php echo $page->page_type ?> <?php if($cat != ""){?> <small>In <?php echo $cat ?></small><?php } ?></h1>
<?php $page->parentCat = $cat ?>


<?php if($this->session->flashdata('flash')) { ?>
<div class="alert alert-info">
    	<?php echo $this->session->flashdata('flash'); ?>
</div>
<?php } ?>

<?php
$attributes = array('class' => 'form-horizontal well', 'id' => 'pageEdit');
echo form_open($path, $attributes);

?>
<div class="container-fluid">
		<div class="row-fluid">
			<div id="mainLeft" class="span8">
				
				<input type="hidden" id="cat" name="cat" value="<?php echo $page->cat; ?>" />
				<input type="hidden" id="id" name="id" value="<?php echo $page->id; ?>" />
				<input type="hidden" id="pageid" name="pageid" value="<?php echo $page->id; ?>" />
				<input type="hidden" id="slug" name ="slug" value="<?php echo $page->slug; ?>" />
				<input type="hidden" id="page_type" name ="page_type" value="<?php echo $page->page_type; ?>" />
				<input type="hidden" name="description" id="description" value="" />
				<?php if(isset($page->pageID)) { ?>
					<input type="hidden" id="parent" name ="parent" value="<?php echo $page->pageID; ?>" />
				<?php } ?>
				<?php if(applyHooks::hide_title($page) != "true") { ?>
					<input type="text" name="title" id="title" class="pageTitle" value="<?php echo stripslashes($page->title); ?>" />
				<?php } ?>
				
				<?php if(applyHooks::hide_content($page) != "true") { ?>
				    <?php applyHooks::hide_slug($page) ?>
                        <?php if(applyHooks::hide_slug($page) != "true") {?>
						<br/><br/>
						<?php $pageURL =  base_url()."index.php/".$cat."/".$page->slug;
							
							if(isset($base_type) && $base_type != ""){
								$pageURL =  base_url()."index.php/".$base_type."/".$cat."/".$page->slug;
								//$pageURL = "";
							}
						
						 ?>
						Page URL : <span class="pagepath"><?php echo $pageURL; ?></span>
					<?php } ?>
					<?php applyHooks::add_slug($page) ?>
						<br/><br/>
						<textarea class="editor" name="content" style="height: 460px;"><?php echo stripslashes($page->content); ?></textarea>
				<?php } ?>
				<?php 
					
					applyHooks::add_main($page);
					?>
				<br/>
				<?php if(applyHooks::hide_meta($page) != "true") { ?>
					<div class="sideBox">
						<div class="sideBoxTitle">Meata Data <a href="#" rel="tooltip" class="tip" title="Information to Help Search Engines find your Page"><i class="icon-info-sign"></i></a></div>
						<div class="subTitle">Description</div>
						<input type="text" name="description" id="description" value="<?php echo $page->description; ?>" class="metaBoxes"/>
					</div>
				<?php } ?>
			</div>

			<div id="mainRight" class="span4">
				
				<div class="sideBox">
					<div class="sideBoxTitle">Publish</div>
					<table class="table">
						<tr>
							<td>Status :</td>
							<td><?php echo $page->status; if($page->status == "pending"){echo " moderation";} ?></td>
						</tr>
						<tr>
							<td>Created :</td>
							<td><input type="text" id="created" name="created" value="<?php echo date_to_human($page->date) ?>" class="datepicker1 " /></td>
						</tr>
						<tr>
							<td>Updated  :</td>
							<td><?php if($page->updated){ echo date_to_human($page->updated); }?></td>
						</tr> 
						<tr>
							<td>Start :</td>
							<td><input type="text" id="start" name="start" value="<?php if($page->start){echo $page->start;} ?>" class="datepicker input-small" /></td>
						</tr>
						<tr>
							<td>End :</td>
							<td><input type="text" id="end" name="end" value="<?php if($page->end){echo $page->end;} ?>" class="datepicker input-small" /></td>
						</tr>
					</table>
					<?php if(!isset($mod)){ ?> 
							<input type="submit" name="submitPage" value="Update" class="left btn btn-primary" />
					<?php }elseif(isset($mod)){  ?>
							<input type="submit" name="ApprovePage" value="Approve" class="left btn btn-success" style="margin-left: 10px;" /> 
							<input type="submit" name="RejectPage" value="Reject" class="left btn btn-danger" style="margin-left: 10px;" /> 
					<?php } ?>
					<br class="clear"/>
				</div>
				<br/>
				<?php
				if($this->configs->get('versioning') == "true" &&  !isset($mod)){ ?>
					<div class="sideBox">
						<div class="sideBoxTitle">Versions</div>
						<table class="table table-striped">
						<?php if(count($versions) != 0){
						foreach($versions as $version){?> 
							<tr>
								<td><?php echo $version->author; ?></td>
								<td><?php echo date_to_human($version->versionDate); ?></td>
								<td><a href="#<?php echo $version->id; ?>" class="lightBox">View</a></td>
							</tr>
							
						<?php }} ?>
						</table>
						
						
						<?php if(count($versions) != 0){
						foreach($versions as $version){?> 
						<div style="display: none;">
								<div id="<?php echo $version->id; ?>" >
									<?php echo confirm($promotepath."/".$page->id."/".$version->id, "<i class='icon-hand-up icon-white'></i> Promote to Current", 'promote', 'Are you sure you want to make this version the current version?','btn btn-primary');?> Version Saved on: <?php echo date_to_human($version->date); ?><br/><hr/>
									<?php echo "<h2>".$version->title."</h2>"; ?>
									<br/>
									<div><?php echo stripslashes($version->content); ?></div>
								</div>
							</div>	
						<?php }} ?>	
						
					</div>
				<?php } ?>
				<br/>
				<!-- <div class="sideBox">
						<div class="sideBoxTitle">Notes</div>
						<textarea id="notes" name="notes"></textarea>
						
					</div> -->
					
				<?php 
					applyHooks::add_side($page);
				?>
			</div>
	</div>
</div>
<?php } ?>

