<h1><span class="ifont">Z </span><?php echo $cat ?> Menu</h1>

<br/>
<?php echo getMessage(); ?> 
<div class="well">

<div class="container-fluid">
		<div class="row-fluid">
			
		
		
		<div id="mainRight" class="span4">
			<div class="sideBox">
				<div class="sideBoxTitle">Add Parent Element</div>
				<?php 
					$attributes = array('class' => '');
					echo form_open('menus/addParent', $attributes);
				?>
				<div class="control-group">
					<!-- <label class="control-label" for="name"><?php echo $cat ?> Pages</label> -->
					<input type="hidden" id="cat" name="cat" value="<?php echo $cat ?>" />
					<div class="control-group">
					<label class="control-label" for="title">Title</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="title" name="title" value="">
					</div>
				</div>
				</div>
				
				<input type="submit" name="mysubmit" value="Add to Menu" class="btn btn-primary" />
				</form>
			</div>
			<br/>
			
			<div class="sideBox">
				<div class="sideBoxTitle">Add Existing <?php echo $cat ?>  Pages</div>
				<?php 
					$attributes = array('class' => '');
					echo form_open('menus/addPage', $attributes);
				?>
				<div class="control-group">
					<label class="control-label" for="parent">Parent Tab</label>
					<select name="parent">
						<?php foreach($parents as $parent){?>
							<option value="<?php  echo $parent->id ?>"><?php echo $parent->name ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="control-group">
					<!-- <label class="control-label" for="name"><?php echo $cat ?> Pages</label> -->
					<input type="hidden" id="cat" name="cat" value="<?php echo $cat ?>" />
					<div class="pageSelBox">
						<?php
						foreach($pages as $page){?>
							  <label>
						      	<input type="radio" name="page" value="<?php echo $page->id ?>" id="RadioGroup1_0"  />
						      <?php echo $page->title; ?></label>
						<?php } ?>
					</div>
				</div>
				
				<input type="submit" name="mysubmit" value="Add to Menu" class="btn btn-primary" />
				</form>
			</div>
			<br/>
			
			<div class="sideBox">
				<div class="sideBoxTitle">Add External Link</div>
				<?php 
					$attributes = array('class' => '');
					echo form_open('menus/external', $attributes);
				?>
				<input type="hidden" id="cat" name="cat" value="<?php echo $cat ?>" />
				<div class="control-group">
										
					<div class="control-group">
						<label class="control-label" for="parent">Parent Tab</label>
						<select name="parent" id="parent">
							<?php foreach($parents as $parent){?>
								<option value="<?php  echo $parent->id ?>"><?php echo $parent->name ?></option>
							<?php } ?>
						</select>
					</div>
				</div>	
				<div class="control-group">	

					<div class="controls">
						<label class="control-label" for="name">Title</label>
						<input type="text" class="input-xlarge validate[required]" id="name" name="name" value="">
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="link">Link</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="link" name="link" value="">
					</div>
				</div>
				<input type="submit" name="mysubmit" value="Add to Menu" class="btn btn-primary" />
			</form>
			</div>
			
			
			
			
		</div>
		
		<div id="mainLeft" class="span8">
			<div class="sideBox">
			<div class="sideBoxTitle"><?php echo $cat ?> Menu</div>
			<?php 
				$attributes = array('class' => 'saveMenu');
				echo form_open('menus/', $attributes);
			?>
				<div class="dd" id="nestable">
				    <ol class="dd-list">
						<?php echo $table ?>
				    </ol>
				</div>
				<br class="clear"/>
				<input type="hidden" name='json' id='json' class="json" /> 
				<br/>
				<input type="submit" name="mysubmit" value="Save Menu" class="btn btn-primary" />
			</form>
			</div>        
		</div>
		
</div>
</div>




<div id="menuModel" class="modal">
	<?php 
		$attributes = array('class' => 'introForm');
		echo form_open('menus/edit', $attributes);
	?>
	 <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Edit Menu Item</h3>
	</div>
	<div class="modal-body">
		
			<input type="hidden" id="modalId" name="id" value="" />
			<label>Edit Name</label>
			<input type="text" class="input-xlarge" id="modalName" name="modalName" value="" />
			<label>Link</label>
			<span class="input-xlarge uneditable-input modalLink"></span>
		
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Close</a>
		<input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />
	</div>
	</form>
</div>