<?php ?>

<h1 class="left"><span class="ifont">g </span>Manage Users <?php echo anchor("adminusers/addUser", "<i class='icon-plus'></i> Add User", 'class="btn "'); ?></h1>    
 
 
 
 <?php 
 	$attributes = array('class' => 'form-search right');

	echo form_open('adminusers/searchUsers', $attributes);
?>
    <input type="text" class="input-medium search-query" name="term" id="term" placeholder="Search Users">
    <input type="submit" name="mysubmit" value="Search" class="btn" />
    </form>
    <br class="clear"/>

    
    <?php if(isset($message)){?>
 	<div class="alert alert-info">
    	<?php echo $message; ?>
    </div>
 <?php } ?>
 
 <?php if(isset($error)){?>
 	<div class="alert alert-error">
    	<?php echo $error; ?>
    </div>
 <?php } ?>
 
 <?php if(isset($succes)){?>
 	<div class="alert alert-success">
    	<?php echo $succes; ?>
    </div>
 <?php } ?>
 
 
 <br />

<?php if($search == "yes"){ ?>
	<h3>Search Results for : <?php echo $term; ?> <?php echo anchor("adminusers/allusers", "<i class='icon-remove'></i> clear", 'class="btn btn-mini"'); ?></h3>
	<br/>     
<?php }else{?>
		<div id="sort">
			<?php $attributes = array('class' => ' form-search Left'); ?>
			<?php echo form_open('adminusers/allusers',$attributes); ?>
			Show only:
			<select name="filter">
				<option value="All">All</option>
				<?php
					$selected = FALSE;
					 foreach($acl as $access){
					if(isset($_REQUEST['filter'])){
						if($_REQUEST['filter'] == $access->id){ $selected = true;} 
					}
					?>
					<option value="<?php echo $access->id; ?>" <?php echo set_select('filter', $access->id, $selected); ?>  ><?php echo $access->acl_name; ?></option>
				<?php } ?>
			</select>
			<input type="submit" name="mysubmit" value="Filter" class="btn" />

		</div>
<?php } ?>
<?php if($users != "no"){?>
	<table  class="table table-striped ">
		<thead>
		<tr>
			<th>Username</th>
			<th>Name</th>
			<th>Email</th>
			<th>Permisions</th>
		</tr>
		</thead>
	<?php
	
	foreach($users as $user){
		if($user->name != "global"){
		?>
		<tr>
			<td>
				<?php echo $user->username; ?>
				<div class="editsOuter">
					<div class="edits">
						<i class="icon-pencil"></i> <?php echo anchor("adminusers/editUser/$user->id", "Edit", 'title="Edit User"'); ?> | <i class="icon-remove"></i> <?php echo confirm("adminusers/deleteUser/$user->id","Delete","Delete User","Permently Delete $user->username?"); ?> 
					</div>
				</div>
			</td>
			<td><?php echo $user->name; ?></td>
			<td><?php echo $user->email; ?></td>
			<td><?php echo $user->acl_name; ?></td>
		</tr>
		<?php
		}
	}
	?>
	</table>
<?php }else{ ?>
	<div class="alert alert-error">
    	No Results Found
    </div>	
<?php } ?>


