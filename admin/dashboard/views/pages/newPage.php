<h1><span class="ifont">e </span>New <?php echo $type ?> <?php if($cat != ""){?><small>In <?php echo $cat ?></small> <?php } ?></h1>
<?php if(!isset($page)){$page = new stdclass();}?>
<?php $page->page_type = $type; ?>
<?php $page->cat = $cat; ?>
<?php $page->isGroup = 'false'; ?>

<script>
	$(function(){
		$('.pageBtn').hide();
		$('.groupBtn').click(function(){
			$('.groupBtn').hide();
			$('.pageBtn').show();
			$('.newPage').fadeOut();
			$('.pageGroup').fadeIn();
			return false;
		})
		
		$('.pageBtn').click(function(){
			$(this).hide();
			$('.groupBtn').show();
			$('.pageGroup').fadeOut();
			$('.newPage').fadeIn();
			
			return false;
		})
	})
</script>

<div class="container-fluid newPage">
		<?php
		$attributes = array('class' => 'form-horizontal well', 'id' => 'pageFrom');
		echo form_open($path, $attributes);
		?>
		<div class="row-fluid">
			<div id="mainLeft" class="span8">
				<input type="hidden" id="type" name ="type" value="<?php echo $type; ?>" />
				<input type="hidden" id="cat" name ="cat" value="<?php echo $cat; ?>" />
				<?php if(applyHooks::hide_title($page) != "true") { ?>
					<input type="text" name="title" id="title" class="pageTitle" placeholder="Enter Title Here" />
					<br/><br/>
				<?php } ?>
				
				<?php if(applyHooks::hide_content($page) != "true") { ?>
					<textarea class="editor" name="content" style="height: 460px;"></textarea>
					<br/>
				<?php } ?>
				
				<?php 
					applyHooks::add_main($page);
				?>
				<?php if(applyHooks::hide_meta($page) != "true") { ?>
					<div class="sideBox">
						<div class="sideBoxTitle">Meata Data <a href="#" rel="tooltip" class="tip" title="Information to Help Search Engines find your Page"><i class="icon-info-sign"></i></a></div>
						<div class="subTitle">Description</div>
						<input type="text" name="description" id="description" value="" class="metaBoxes"/>
					</div>
				<?php } ?>
			</div>
			
			<div id="mainRight" class="span4">
				<div class="sideBox">
					<div class="sideBoxTitle">Publish</div>
					<table class="table">
						<tr>
							<td>Status :</td>
							<td>Draft</td>
						</tr>
						<tr>
							<td>Created :</td>
							<td><input type="text" id="created" name="created" value="<?php echo date('M jS Y   g:ia'); ?>" class="datepicker input-small" /></td>
						</tr>
						<tr>
							<td>Updated  :</td>
							<td></td>
						</tr> 
						<tr>
							<td>Start :</td>
							<td><input type="text" id="start" name="start" value="<?php  echo date('M jS Y   g:ia'); ?>" class="datepicker" /></td>
						</tr>
						<tr>
							<td>End :</td>
							<td><input type="text" id="end" name="end" value="" class="datepicker" /></td>
						</tr>
					</table>
					<input type="submit" name="pageSubmit" value="Save" class="btn btn-primary" />
					<br class="clear"/>
				</div>
				
				<?php 
					applyHooks::add_side($page);
				?>
				
		</div>
	</div>
</form>
</div>



