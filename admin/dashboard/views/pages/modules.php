<h1>Modules</h1>


<?php if(isset($message)){?>
 	<div class="alert alert-info">
    	<?php echo $message; ?>
    </div>
 <?php } ?>
 
 <?php if(isset($error)){?>
 	<div class="alert alert-error">
    	<?php echo $error; ?>
    </div>
 <?php } ?>
 
 <?php if(isset($succes)){?>
 	<div class="alert alert-success">
    	<?php echo $succes; ?>
    </div>
 <?php } ?>
 
<?php

	$dir = APPPATH.'modules';  
    $scan = scandir($dir);  
  	$this->crud->use_table('cms_modules');
    for ($i=0; $i<count($scan); $i++) {  
     if ($scan[$i] != '.' && $scan[$i] != '..' && $scan[$i] != '.DS_Store') {
     	 
     	$num = 0;
		 $num = $this->crud->count_results(array('module' => $scan[$i]));
     	?>
     	<?php $title = str_replace('_', ' ', $scan[$i]); ?>
     	<div class="<?php if($num == 0){ echo "module"; } else{ echo "module installed";} ?>">
     	
     	<strong><?php echo $title ?></strong><br/>
     	<?php
     	
		if($num == 0){
     		echo "<a href=\"".base_url()."".index_page()."/".$scan[$i]."/install \">Install</a>";
		}else{
			 echo "<a href=\"".base_url()."".index_page()."/".$scan[$i]."/uninstall \">Uninstall</a>";
			 // echo " | ";
			 // echo "<a href=\"".base_url()."".index_page()."/".$scan[$i]."/install \">Rerun Install</a>";
		}
		 echo "<br/>";
		 $file = $dir."/".$scan[$i]."/controllers/info.php";
		 $fp = fopen( $file, 'r' );

	// Pull only the first 8kiB of the file in.
	$file_data = fread( $fp, filesize($file) );
	echo "<div class=\"modBody\">";
	echo $file_data;
	echo "</div>";
	?>
	</div>
	<?php
	// PHP will close file handle, but we are good citizens.
	fclose( $fp );
	
	
         }  
        }  