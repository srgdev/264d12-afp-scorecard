<?php

?>

<h1>Change User Password</h1>

<?php echo getMessage(); ?>


<?php 
	
	$attributes = array('class' => 'form-horizontal well', 'id' => 'addUser');
	echo form_open('admin/changePassword', $attributes);
?>	

				<div class="control-group">
					<label class="control-label" for="old">Old Password</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,ajax[ajaxPasswordCallPhp]]" id="old" name="old" value="" onfocus="this.type='password';" >
					</div>
				</div>
				
				
				<br/>
				
				<div class="control-group">
					<label class="control-label" for="password">New Password</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="password" name="password" value="" onfocus="this.type='password';" >
					</div>
				</div>
				
				 
				<div class="control-group">
					<label class="control-label" for="confirm">Confirm</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,equals[password]] text-input" id="confirm" name="confirm" value="" onfocus="this.type='password';" >
					</div>
				</div>
				
	
				<input type="submit" name="mysubmit" value="Save" class="btn btn-primary" />
				<?php echo anchor("admin/dash", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>

			
