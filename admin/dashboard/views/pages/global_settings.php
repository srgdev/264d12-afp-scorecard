<h1>System Settings</h1>

<?php if($this->session->flashdata('flash')) { ?>
<div class="alert alert-info">
    	<?php echo $this->session->flashdata('flash'); ?>
</div>
<?php } ?>

<?php 
$attributes = array('class' => 'form-horizontal well');
echo form_open('settings/update', $attributes); ?>
	<h3>Groups</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="useGroups">Use Group Auth</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="useGroups" name="useGroups" value="false" />
				<input name="useGroups" id="useGroups" type="checkbox" value="true" <?php if($this->configs->get('useGroups') == "true"){ echo "checked=\"checked\"";} ?> />
			</label> 
		</div>
	</div>
	
	<h3>System Messaging</h3><br/>
	
	<div class="control-group">
		<label class="control-label" for="send_sysMes">Send System Messages</label>
		<div class="controls">
			<label class="checkbox">
				<input type="hidden" id="send_sysMes" name="send_sysMes" value="false" />
				<input name="send_sysMes" id="send_sysMes" type="checkbox" value="true" <?php if($this->configs->get('send_sysMes') == "true"){ echo "checked=\"checked\"";} ?> />
			</label> 
		</div>
	</div>
	
	
	<div class="control-group">
		<label class="control-label" for="sysmes_to">System Messages Email</label>
		<div class="controls">
			<input type="text" class="input-xlarge" id="sysmes_to" name="sysmes_to" value="<?php echo $this->configs->get('sysmes_to'); ?>">
		</div>
	</div>
	
	
	<input type="submit" name="mysubmit" value="Save Changes" class="btn btn-primary" />