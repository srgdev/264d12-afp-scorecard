

	<?php echo getMessage();  ?>
    
	
	<div id="dashLogs">
		<h3>Recent System Events</h3>
		<table  class="table table-striped">
			<thead>
			<tr>
				<th>Username</th>
				<th>Event</th>
				<th>Date/Time</th>
			</tr>
			</thead>
			<?php
			foreach($logData as $logLine){ ?> 
				<tr class="popParent"  >
					<td><?php echo $logLine->user ?></td>
					<td class="pop" data-content="<?php echo $logLine->desc ?>" rel="popover"><?php echo $logLine->action ?></td>
					<td><?php echo date_to_human($logLine->date) ?></td>
				</tr>	
			<?php } ?>
		
		</table>
	</div>
