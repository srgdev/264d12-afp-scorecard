<?php

?>

<h1><span class="ifont">f </span>Add User</h1>

<?php if($change == "yes") { ?>
	<div class=" alert-success well">
   		<i class="icon-ok "></i> User was Sucesfully Added 
    </div>
	
<?php } ?>


<?php 
	
	$attributes = array('class' => 'form-horizontal well', 'id' => 'addUser');
	echo form_open('adminusers/insertUser', $attributes);
?>	
<div class="container-fluid">
		<div class="row-fluid">
			<div id="mainLeft" class="span8">
				
				<div class="control-group">
					<label class="control-label" for="name">Name</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="name" name="name" value="">
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="email">Email</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,custom[email]]" id="email" name="email" value="">
					</div>
				</div>
				 <br/><br/>
				<div class="control-group">
					<label class="control-label" for="username">Username</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,custom[onlyLetterNumber],maxSize[20],ajax[ajaxUserCallPhp]] text-input" id="username" name="username" value="">
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="pass">Password</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required]" id="pass" name="pass" onfocus="this.type='password';" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="conpass">Confirm Password</label>
					<div class="controls">
						<input type="text" class="input-xlarge validate[required,equals[pass]] " id="conpass" name="conpass" onfocus="this.type='password';" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="use_smtp">Send Username and Password to the User</label>
					<div class="controls">
						<label class="checkbox">
							<input name="sendEmail" id="sendEmail" type="checkbox" value="true"  />  
						</label>
					</div>
				</div>
	
				<input type="submit" name="mysubmit" value="Add User" class="btn btn-primary" />
				<?php echo anchor("adminusers/allusers", "<i class='icon-remove'></i> Cancel", 'class="btn "'); ?>
				
	</div>
			
			<div id="mainRight" class="span4">
				
					
					
				<div class="sideBox">
					<div class="sideBoxTitle">Accout Permissions</div>
						<?php foreach($acl as $access){?>
							
								<input type="radio" id="radio<?php echo $access->id; ?>" class="validate[required] " name="acl" value="<?php echo $access->id; ?>"  /> <?php echo $access->acl_name; ?><br />
													 <?php } ?>	
				</div>	
				<br/>
				<?php if($this->configs->get('useGroups') == "true"){?>
				<div class="sideBox">
					<div class="sideBoxTitle">Group(s)</div>
					
						
						<?php foreach($acg as $group){?>
							<label class="checkbox">
								<input name="acg[]" type="checkbox" value="<?php echo $group->acg; ?>"  /> <?php echo $group->acg; ?>
							</label>
						 <?php } ?>
						
					
				</div>
				<?php } ?>
				
	
	</div>
	</div>
