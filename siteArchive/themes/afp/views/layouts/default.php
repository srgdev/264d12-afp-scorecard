<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi">
<meta property="og:image" content="http://afpscorecard.org/site/themes/afp/images/logo-afp.png" />
<title>AFP: SCORECARD</title>

<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Pontano+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo cssPath(); ?>/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/responsive.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/selectyze.jquery.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/add.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

<?php if ($this->agent->is_browser()){ ?>
  <link href="<?php echo cssPath(); ?>/desktop.css" rel="stylesheet" type="text/css" />    
<?php } ?>


<script type="text/javascript" src="//use.typekit.net/qdc5wfq.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/clearInput.jquery.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/selectyze.jquery.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/blockui.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/app.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/init.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/routes.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo jsPath(); ?>/jquery.validationEngine.js"></script>
<script src="<?php echo jsPath(); ?>/languages/jquery.validationEngine-en.js" type="text/javascript" ></script>

<!--[if < IE 10]>
<script type="text/javascript" src="<?php echo jsPath(); ?>/PIE.js"></script>
<![endif]-->

</head>

<body>
<div id="stickyOuter">
<div id="stickyInner">

    <?php echo $template['partials']['header']; ?>
            
    <div id="rowOuter" class="contentRow">
        <?php echo $template['partials']['index']; ?>
    </div><!-- end contentRow -->
    


</div><!-- end stickyInner -->
</div><!-- end stickyOuter -->

<?php echo $template['partials']['footer']; ?>

<script type="text/javascript" src="<?php echo jsPath(); ?>/globalFunctions.js"></script>


<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38701395-1']);
  _gaq.push(['_setDomainName', 'afpscorecard.org']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

</body>
</html>
