<div id="rowOuter" class="footer">
    <div id="rowInner">
        <div id="conBox" class="footer">
            
            <div id="footBox">
                <a href="http://americansforprosperity.org/about/">About AFP</a> <a href="http://americansforprosperity.org/terms-of-use/">Terms of Use</a> <a href="http://americansforprosperity.org/privacy-policy/">Privacy Policy</a>
                <div class="horzRule"></div>
                <span class="footInfo">© AMERICANS FOR PROSPERITY. ALL RIGHTS RESERVED.<br />2111 Wilson BLvd - SUITE 350 - Arlington, VA 22201 | 866-730-0150</span> 
            </div>
            
            <div id="sidebar" class="footer">
                <img class="left" src="<?php echo imagesPath(); ?>/icon-usa.jpg" width="69" height="43" /> 
                <div id="footBtn" class="button btnDkGreen noHover">
                    <select  class="chapter chapterList">
                        <option value="#">Select Chapter</option>
                        <option value="http://americansforprosperity.org/arizona/">Arizona</option>
                        <option value="http://americansforprosperity.org/arkansas/">Arkansas</option>
                        <option value="http://americansforprosperity.org/california/">California</option>
                        <option value="http://americansforprosperity.org/colorado/">Colorado</option>
                        <option value="http://americansforprosperity.org/connecticut/">Connecticut</option>
                        <option value="http://americansforprosperity.org/florida/">Florida</option>
                        <option value="http://americansforprosperity.org/georgia/">Georgia</option>
                        <option value="http://americansforprosperity.org/illinois/">Illinois</option>
                        <option value="http://americansforprosperity.org/indiana/">Indiana</option>
                        <option value="http://americansforprosperity.org/iowa/">Iowa</option>
                        <option value="http://americansforprosperity.org/kansas/">Kansas</option>
                        <option value="http://americansforprosperity.org/louisiana/">Louisiana</option>
                        <option value="http://americansforprosperity.org/maine/">Maine</option>
                        <option value="http://americansforprosperity.org/maryland/">Maryland</option>
                        <option value="http://americansforprosperity.org/michigan/">Michigan</option>
                        <option value="http://americansforprosperity.org/minnesota/">Minnesota</option>
                        <option value="http://americansforprosperity.org/missouri/">Missouri</option>
                        <option value="http://americansforprosperity.org/montana/">Montana</option>
                        <option value="http://americansforprosperity.org/nebraska/">Nebraska</option>
                        <option value="http://americansforprosperity.org/nevada/">Nevada</option>
                        <option value="http://americansforprosperity.org/new-hampshire/">New Hampshire</option>
                        <option value="http://americansforprosperity.org/new-jersey/">New Jersey</option>
                        <option value="http://americansforprosperity.org/new-mexico/">New Mexico</option>
                        <option value="http://americansforprosperity.org/new-york/">New York</option>
                        <option value="http://americansforprosperity.org/north-carolina/">North Carolina</option>
                        <option value="http://americansforprosperity.org/ohio/">Ohio</option>
                        <option value="http://americansforprosperity.org/oklahoma/">Oklahoma</option>
                        <option value="http://americansforprosperity.org/oregon/">Oregon</option>
                        <option value="http://americansforprosperity.org/pennsylvania/">Pennsylvania</option>
                        <option value="http://americansforprosperity.org/south-carolina/">South Carolina</option>
                        <option value="http://americansforprosperity.org/tennessee/">Tennessee</option>
                        <option value="http://americansforprosperity.org/texas/">Texas</option>
                        <option value="http://americansforprosperity.org/virginia/">Virginia</option>
                        <option value="http://americansforprosperity.org/washington/">Washington</option>
                        <option value="http://americansforprosperity.org/wisconsin/">Wisconsin</option>
                    </select>
                                    </div>
            </div>
            
        </div>
    </div>
</div> <!-- end stickyFooter -->



<map name="Map" id="Map">
  <area shape="poly" coords="375,195,399,193,400,196,415,209,423,218,429,225,426,232,423,240,422,243,420,243,419,246,414,246,390,247,385,235,386,228" href="#" class="routeState" alt="state#ga" />
  <area shape="poly" coords="167,237,202,239,208,184,236,184,236,208,249,213,263,219,273,219,284,215,295,222,297,238,302,250,300,265,293,271,288,270,287,276,274,282,264,293,259,301,264,314,244,310,241,298,235,291,226,277,219,268,205,265,200,276,184,264,185,255" href="#" class="routeState" alt="state#tx" />
  <area shape="poly" coords="294,226,321,226,327,234,321,248,340,249,344,258,336,259,337,261,345,261,343,267,348,272,337,270,329,272,319,264,318,269,307,267,298,266,302,252,297,240" href="#" class="routeState" alt="state#la" />
  <area shape="poly" coords="359,250,415,245,424,243,435,264,439,272,447,289,447,294,450,307,442,308,436,303,426,291,418,285,420,281,417,277,417,268,407,261,398,256,388,262,379,254,371,254,361,257" href="#" class="routeState" alt="state#fl" />
  <area shape="poly" coords="351,199,376,197,387,225,387,237,391,246,363,249,362,256,355,256,353,243,352,224,353,213" href="#" class="routeState" alt="state#al" />
  <area shape="poly" coords="331,201,352,200,353,207,352,225,356,255,343,257,340,250,323,247,326,236,323,224,323,217,327,207" href="#" class="routeState" alt="state#ms" />
  <area shape="poly" coords="400,194,408,190,421,188,426,193,434,189,449,199,442,208,442,213,434,218,428,225,409,205" href="#" class="routeState" alt="state#sc" />
  <area shape="poly" coords="388,193,401,183,408,179,413,171,468,161,462,168,471,169,471,173,462,175,464,177,462,182,462,186,455,193,455,197,449,198,436,188,427,191,421,188,411,188,402,194" href="#" class="routeState" alt="state#nc" />
  <area shape="poly" coords="332,199,332,196,335,192,336,188,338,182,351,181,351,178,362,178,394,175,415,171,411,179,404,182,397,188,389,194,378,194,378,196,352,198,352,200,334,200" href="#" class="routeState" alt="state#tn" />
  <area shape="poly" coords="394,173,402,166,407,159,412,164,413,161,418,161,423,158,428,143,431,146,433,140,438,134,439,131,443,133,445,131,450,133,452,138,461,144,462,148,462,151,462,154,458,154,467,156,469,160,436,166,413,170" href="#" class="routeState" alt="state#va" />
  <area shape="poly" coords="359,111,362,106,362,100,359,92,359,89,358,85,359,81,360,76,363,71,363,73,367,68,369,66,369,62,366,57,359,59,357,62,352,63,347,71,344,65,342,62,326,60,323,56,330,52,340,44,336,51,342,50,346,55,354,56,357,52,362,51,365,48,370,53,376,57,371,58,372,62,377,63,382,65,384,71,384,76,381,80,380,84,383,87,387,80,391,83,395,92,394,97,389,104,388,110,375,110,364,112" href="#" class="routeState" alt="state#mi" />
  <area shape="poly" coords="417,98,424,92,424,88,421,85,430,81,433,82,439,81,446,76,446,70,444,67,450,61,452,57,466,53,469,62,470,69,471,71,474,83,474,90,476,94,476,100,475,101,470,100,463,97,458,93,451,94,421,101" href="#" class="routeState" alt="state#ny" />
  <area shape="poly" coords="494,69,484,44,487,41,489,36,488,23,492,13,495,16,500,13,505,14,508,30,513,32,515,36,518,37,520,40,514,42,507,47,504,53,500,56,497,60" href="#" class="routeState" alt="state#me" />
  <area shape="poly" coords="475,53,491,47" href="#" class="routeState" alt="state#vt" />
  <area shape="poly" coords="489,84,491,83,494,88,524,94,530,90,537,91,537,102,530,102,525,97,498,91,491,92" href="#" class="routeState" alt="state#ri" />
  <area shape="poly" coords="474,89,489,84,491,91,486,94,508,102,519,103,530,105,530,112,517,115,513,109,484,95,479,98" href="#" class="routeState" alt="state#ct" />
  <area shape="poly" coords="412,103,417,129,460,122,460,118,465,119,468,114,463,109,465,98,458,92,421,102,419,99" href="#" class="routeState" alt="state#pa" />
  <area shape="poly" coords="466,98,474,101,473,107,475,110,476,118,472,128,471,127,465,126,463,122,463,118,465,118,470,115,464,108" href="#" class="routeState" alt="state#nj" />
  <area shape="poly" coords="462,120,467,131,500,137,504,135,519,134,520,148,506,148,503,141,470,134,470,137,465,136,461,124" href="#" class="routeState" alt="state#de" />
  <area shape="poly" coords="59,0,82,7,103,12,97,41,97,47,84,44,64,44,56,41,52,42,48,40,48,36,40,30,41,23,42,21,42,14,41,5,53,11,52,15,50,21,55,21,58,15,59,9,59,6" href="#" class="routeState" alt="state#wa" />
  <area shape="poly" coords="103,12,112,14,110,22,110,26,111,31,113,36,121,45,117,57,122,56,125,64,128,72,144,74,140,105,86,95,93,70,91,67,101,53,96,47" href="#" class="routeState" alt="state#id" />
  <area shape="poly" coords="111,15,170,25,209,27,205,76,145,70,143,74,128,74,121,58,116,57,120,45,112,36,109,22" href="#" class="routeState" alt="state#mt"/>
  <area shape="poly" coords="209,28,265,29,265,37,267,45,268,58,269,66,206,64,206,46" href="#" class="routeState" alt="state#nd" />
  <area shape="poly" coords="204,64,202,99,251,102,257,106,261,104,269,108,269,100,269,96,270,74,267,70,268,66" href="#" class="routeState" alt="state#sd" />
  <area shape="poly" coords="263,29,279,29,281,26,283,31,293,34,299,33,304,36,311,39,325,39,307,55,305,64,300,68,302,69,302,79,315,90,317,95,269,97,269,73,268,68,270,63,266,55,267,50" href="#" class="routeState" alt="state#mn" />
  <area shape="poly" coords="305,58,316,53,316,55,322,55,326,59,334,60,342,62,346,69,344,78,351,71,348,81,347,86,347,92,347,99,348,105,323,106,319,103,318,95,314,87,308,83,302,78,301,70,299,66" href="#" class="routeState" alt="state#wi" />
  <area shape="poly" coords="322,106,345,105,349,111,351,114,353,142,353,145,356,151,352,157,351,166,347,168,348,172,342,171,338,172,338,167,328,159,329,151,327,151,324,147,319,141,317,137,318,132,321,127,319,121,324,119,327,113" href="#" class="routeState" alt="state#il" />
  <area shape="poly" coords="352,115,376,110,380,146,375,147,375,150,372,155,371,157,367,157,365,159,361,160,357,160,352,162,352,155,356,150,353,144,351,127" href="#" class="routeState" alt="state#in" />
  <area shape="poly" coords="351,178,341,179,341,171,346,173,347,168,349,165,351,162,356,162,358,162,364,159,370,157,373,149,378,148,379,144,382,144,385,147,395,148,397,146,402,149,402,152,407,159,396,173,387,175,365,177" href="#" class="routeState" alt="state#ky" />
  <area shape="poly" coords="412,163,401,150,404,142,404,142,408,137,413,133,415,121,417,131,426,129,428,134,436,129,439,128,443,131,438,132,432,145,427,143,423,157,418,160" href="#" class="routeState" alt="state#wv" />
  <area shape="poly" coords="23,80,60,90,51,126,93,185,92,189,96,195,91,202,88,207,90,211,87,214,62,211,61,202,56,193,50,189,42,183,34,179,34,173,27,160,27,152,26,143,26,136,24,130,22,125,18,117,20,109,16,100,19,96,22,89" href="#" class="routeState" alt="state#ca" />
  <area shape="poly" coords="61,90,114,101,100,174,95,172,93,183,51,125" href="#" class="routeState" alt="state#nv" />
  <area shape="poly" coords="41,30,29,61,25,68,24,79,87,95,93,69,91,68,101,54,97,47,84,43,66,44,57,40,52,41" href="#" class="routeState" alt="state#or" />
  <area shape="poly" coords="114,101,141,105,139,118,157,122,151,171,103,163" href="#" class="routeState" alt="state#ut" />
  <area shape="poly" coords="102,163,150,171,141,241,122,239,85,216,89,212,86,207,95,196,91,190,94,174,99,175" href="#" class="routeState" alt="state#az" />
  <area shape="poly" coords="151,171,209,177,203,238,168,236,168,237,152,237,150,241,141,240" href="#" class="routeState" alt="state#nm" />
  <area shape="poly" coords="376,111,388,109,395,113,414,103,416,126,409,137,404,149,398,146,392,147,386,147,383,145,378,144" href="#" class="routeState" alt="state#oh" />
  <area shape="poly" coords="270,96,269,105,273,115,278,125,277,131,317,130,322,127,320,121,325,120,328,114,321,106,319,104,318,96" href="#" class="routeState" alt="state#ia" />
  <area shape="poly" coords="275,131,316,130,317,138,326,149,328,150,328,156,336,167,337,170,342,174,342,180,339,181,336,188,331,188,332,182,326,182,288,183,288,151,284,145,284,142,280,141" href="#" class="routeState" alt="state#mo" />
  <area shape="poly" coords="290,183,333,181,331,185,337,186,334,198,325,217,326,225,296,226,292,218,293,201" href="#" class="routeState" alt="state#ar" />
  <area shape="poly" coords="207,177,246,178,290,178,290,182,294,201,293,220,287,214,271,218,266,218,237,208,236,184,207,183" href="#" class="routeState" alt="state#ok" />
  <area shape="poly" coords="218,139,283,139,282,144,288,150,288,177,217,177" href="#" class="routeState" alt="state#ks" />
  <area shape="poly" coords="203,100,252,103,256,107,260,104,268,108,277,124,277,130,279,138,221,141,223,129,201,125" href="#" class="routeState" alt="state#ne" />
  <area shape="poly" coords="145,69,207,77,202,124,138,116" href="#" class="routeState" alt="state#wy" />
  <area shape="poly" coords="156,119,221,127,224,127,223,138,221,139,220,175,210,175,150,169" href="#" class="routeState" alt="state#co" />
  <area shape="poly" coords="7,247,13,248,30,255,33,255,37,259,46,263,55,263,61,269,67,278,79,284,84,289,78,293,70,299,69,294,67,286,68,283,60,273,56,269,36,263,30,260" href="#" class="routeState" alt="state#hi" />
  <area shape="poly" coords="109,297,115,297,120,291,131,284,143,287,153,287,159,286,164,288,184,345,187,342,195,345,204,342,221,356,228,359,222,366,215,360,210,360,207,353,192,347,177,346,174,348,163,343,159,346,162,349,154,355,153,349,154,342,147,349,144,357,146,362,140,368,134,376,122,383,100,393,110,388,113,387,124,380,131,369,132,369,135,363,121,361,119,356,113,353,109,350,109,348,107,344,113,336,120,334,121,333,121,331,121,331,120,324,113,324,109,322,107,318,111,315,115,317,124,314" href="#" class="routeState" alt="state#ak" />
  <area shape="poly" coords="501,161,508,162,514,156,512,149,500,149,496,150,469,140,467,136,459,127,456,124,427,128,427,131,436,127,444,129,447,132,453,140,454,132,458,132,460,138,468,142,494,153" href="#" class="routeState" alt="state#md" />
  <area shape="poly" coords="510,118,520,118,527,119,524,132,514,132,504,128" href="#" class="routeState" alt="state#dc" />
  <area shape="poly" coords="473,80,474,88" href="#" class="routeState" alt="state#ma" />
  <area shape="poly" coords="473,79,473,87,490,83,493,86,497,86,502,85,504,82,524,83,527,87,535,87,542,88,541,82,540,77,532,77,523,77,519,80,498,79,493,79,495,76,494,74,484,76" href="#" class="routeState" alt="state#ma" />
  <area shape="poly" coords="481,76,488,75,493,72,498,69,523,70,525,73,534,74,542,73,543,67,539,63,527,63,523,65,512,66,493,68,483,47,483,53,480,57" href="#" class="routeState" alt="state#nh" />
  <area shape="poly" coords="466,52,481,47,482,53,479,60,521,54,525,48,538,48,538,56,525,60,519,57,480,63,480,76,473,78" href="#" class="routeState" alt="state#vt" />
</map>