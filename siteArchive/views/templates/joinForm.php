    <script>
        jQuery(document).ready( function() {
            // binds form submission and fields to the validation engine
            jQuery(".joinForm").validationEngine();
        });
    </script>
    <h1>Join AFP Scorecard</h1>
    <form class="joinForm">
        <label>First Name</label><br />
        <input name="fname" id="fname" class="joinField rounded validate[required] text-input" type="text" /><br />
        <label>Last Name</label><br />
        <input name="lname" id="lname" class="joinField rounded validate[required] text-input" type="text" /><br />
        <label>Email Address</label><br />
        <input name="email" id="email" class="joinField rounded required validate[required,custom[email]] text-input" type="text" /><br />
        <label>Zip Code</label><br />
        <input name="zip" id="zipcode" class=" joinField rounded validate[required] text-input" type="text" /><br />
        <div id="submitOuter" class="rounded"><input name="submit" type="submit" class="button btnGold btnSend" id="submit" value="SUBMIT" /></div>
    </form>
