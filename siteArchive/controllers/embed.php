<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class embed extends CI_Controller {
 
    function __construct()
    {
        parent::__construct();
        ini_set('memory_limit', '-1');
    }
   
    public function member(){
        $congress = $_REQUEST['congress'];
        $memID = $_REQUEST['memID'];
        $query = $this->db->query('SELECT *
            FROM afp_members, AFP_'.$congress.'_scores, afp_lifetime_scores 
            WHERE afp_members.congID = "'.$memID.'"
            AND afp_members.congress = "'.$congress.'"
            AND afp_members.congID = AFP_'.$congress.'_scores.memID
            AND afp_members.congID = afp_lifetime_scores.memID
        ');
        $this->template->set('members', $query->result());
         $this->template->set_layout('memEmbed') ;
        $this->template->build('templates/memEmbed');
    }
 
    public function state(){
       
        $state = $_REQUEST['state'];
        $congress = $_REQUEST['congress'];
        $query = $this->db->query('SELECT *
            FROM afp_members, AFP_'.$congress.'_scores 
            WHERE afp_members.state = "'.$state.'"
            AND afp_members.congress = "'.$congress.'"
            AND afp_members.congID = AFP_'.$congress.'_scores.memID
        ');
        $this->template->set('members', $query->result());
        
        $rTotal = 0;
        $dTotal = 0;
        $rscore = 0;
        $dscore = 0;
        foreach($query->result() as $mem){
            if($mem->party == 'R'){
              $rTotal++; 
              $rscore = $rscore + $mem->score; 
            }else{
              $dTotal++; 
              $dscore = $dscore + $mem->score; 
            }
        }

        $ravg = round(($rscore/$rTotal));
        $davg = round(($dscore/$dTotal));
        
        $this->template->set('ravg', $ravg);
        $this->template->set('davg', $davg);
        $this->template->set('state', $state);
         $this->template->set_layout('stateEmbed') ;
        $this->template->build('templates/stateEmbed');
    }
    
}
