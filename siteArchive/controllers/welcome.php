<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{
	    $this->crud->use_table('cms_pages');
        $data =  $this->crud->retrieve(array('cat' => 'alerts'), '', 4, 0, array('date' => 'DESC'));
        $this->template->set('data', $data);
        
	    $this->template->set_partial('header', 'templates/header');
        $this->template->set_partial('footer', 'templates/footer');
        $this->template->set_partial('index', 'templates/index');
	    $this->template->build('templates/index');
    }
    
    public function aboutPage(){
        $this->crud->use_table('cms_pages');
        $pages['data'] = $this->crud->retrieve(array('cat' => 'pages', 'id' => '6'), '', 4, 0, array('id' => 'DESC'));
        echo $this->load->view('templates/about',$pages , true);
    }
	
	public function homePage(){
	    $this->crud->use_table('cms_pages');
        $pages['data'] = $this->crud->retrieve(array('cat' => 'alerts'), '', 4, 0, array('date' => 'DESC'));
	    echo $this->load->view('templates/index',$pages , true);
	}

    public function single(){
        $id = $_REQUEST['id'];
        $this->crud->use_table('cms_pages');
        $pages['data'] = $this->crud->retrieve(array('id' => $id), '', 0, 0, array('id' => 'DESC'));
        echo $this->load->view('templates/singleAlert',$pages , true);
    }
    
    public function alerts(){
        $array = $this->uri->uri_to_assoc(3);
        $perpage = 6;
        $curpage = $array['p'];
        $offest = ($curpage - 1)*$perpage;
        
        $data = array(
            'cat' => 'alerts'
        );
        
        
        $this->crud->use_table('cms_pages');
        $count = $this->crud->retrieve($data, '', 0, 0, array('date' => 'ASC'));
        $pages['data'] = $this->crud->retrieve($data, '', $perpage, $offest, array('date' => 'DESC'));
        
        
        $pageTotal = count($count)/$perpage;
        $pages['total'] = $pageTotal;
        $pages['page'] = $curpage;
        echo $this->load->view('templates/voteAlerts',$pages , true);
    }
    
    public function alertSearch(){
        $search = $_REQUEST['search'];
        $this->crud->use_table('cms_pages');
        $pages['data'] = $this->crud->search($search, 'title', 0, 0, array('date' => 'DESC'));
        $pages['search'] = $search;
        echo $this->load->view('templates/voteAlertsSearch',$pages , true);
        //echo $search;
    }
    
    public function alertissue(){
        $array = $this->uri->uri_to_assoc(3);
        $perpage = 6;
        $curpage = $array['p'];
        $offest = ($curpage - 1)*$perpage;
        $data = array(
            'cat' => 'alerts',
            'issue' => $array['issue']
        );
        
        
        $this->crud->use_table('cms_pages');
        $count = $this->crud->retrieve($data, '', 0, 0, array('date' => 'DESC'));
        $pages['data'] = $this->crud->retrieve($data, '', $perpage, $offest, array('date' => 'DESC'));
        $pageTotal = count($count)/$perpage;
        $pages['total'] = $pageTotal;
        $pages['page'] = $curpage;
        echo $this->load->view('templates/voteAlerts',$pages , true);
    }
    
    public function alertChamber(){
        $array = $this->uri->uri_to_assoc(3);
        $data = array(
            'cat' => 'alerts',
            'chamber' => $array['chamber']
        );
        $this->crud->use_table('cms_pages');
        $pages['data'] = $this->crud->retrieve($data, '', 0, 0, array('date' => 'DESC'));
        $pages['search'] = $array['chamber'];
        echo $this->load->view('templates/voteAlertsSearch',$pages , true);
        
    }
	
    
    public function formSubmit(){
        $data = $this->crud->array_from_input();
        $data['date'] = date("n/j/Y");
        $this->crud->use_table('afp_join');
        $this->crud->create($data);
        echo 'Good';    
    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */