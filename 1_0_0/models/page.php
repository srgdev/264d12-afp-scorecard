<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class page extends CI_Model {
	
	
	
	
	public function buildPage($method) {
		$this->crud->use_table('cms_pages');
	 	$page = $this->crud->retrieve(array('slug' => $method));
		$good = true;
		if(count($page) > 0) {
			foreach($page as $pag) {
				$id = $pag->id;
				$isGroup = $pag->isGroup;
				$start = $pag->start;
				$end = $pag->end;
			}
			
			if($start){
				if(time() < strtotime($start)){
					$good = false;
				}
			}
			
			if($end){
				if(time() > strtotime($end)){
					$good = false;
				}
			}
		}
		
		if(count($page) > 0 && $good == true) {
		    $this->template->set('pages', $page);
			$this->template->title('Capitol Faces')->build('templates/error');
		}else{
			$this->template->set_layout('404');
			$this->template->title('404 Error')->build('templates/error');
		}
	}
}
