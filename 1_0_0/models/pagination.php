<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class pagination extends CI_Model {
        
    public function makePagi($table, $orderby = ""){
        $this->crud->use_table($table);
        $segment_array=$this->uri->segment_array();
        $segment_count=$this->uri->total_segments();
        $segment_array1=$this->uri->segment_array();
        
        $this->load->library("pagination");
        $config = array();
        $currentNum = 1;
        if( is_numeric($segment_array[$segment_count])){array_pop($segment_array1); $currentNum = $segment_array[$segment_count];}
        
        $config["base_url"] = base_url() .index_page()."/".join("/",$segment_array1);
        $config["total_rows"] =  $this->crud->count_all_where(array('session' => $this->cursession));
        $config["per_page"] = 10;
        $config["uri_segment"] = count($segment_array);
        $config['cur_tag_open'] = '<span class="active">';
        $config['cur_tag_close'] = '</span>';
        $config['num_tag_open'] = '';
        $config['num_tag_close'] = '';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        
        $this->pagination->initialize($config);
        $page = $segment_array[$segment_count];
        $links = $this->pagination->create_links();
        
        
        $current1 = $currentNum;
        $current2 = ($current1 + $config["per_page"] <= $config["total_rows"]) ? $current1 + $config["per_page"] : $config["total_rows"] ; 
        $current3 = $current1 + $current2;
        $current =  $current1."-".$current2;
        
        
        $this->template->set('links', $links);
        $this->template->set('total_rows', $config["total_rows"]);
        $this->template->set('current', $current);
        
        return $this->crud->retrieve(array('session' => $this->cursession),'', $config["per_page"], $segment_array[$segment_count], $orderby);
    }
        
}

    