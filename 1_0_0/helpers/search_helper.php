<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


/***********************
 Sphider configuration file
***********************/


/*********************** 
General settings 
***********************/

// Sphider version 
$version_nr			= '1.3.5';

//Language of the search page 
$language			= 'en';

// Template name/directory in templates dir
$template	= 'dark';

//Administrators email address (logs can be sent there)	
$admin_email		= 'admin@localhost';

// Print spidering results to standard out
$print_results		= 1;

// Temporary directory, this should be readable and writable
$tmp_dir	= 'tmp';


/*********************** 
Logging settings 
***********************/

// Should log files be kept
$keep_log			= 0;

//Log directory, this should be readable and writable
$log_dir	= 'log';

// Log format
$log_format			= 'html';

//  Send log file to email 
$email_log			= 0;


/*********************** 
Spider settings 
***********************/

// Min words per page required for indexing 
$min_words_per_page = 10;

// Words shorter than this will not be indexed
$min_word_length	= 3;

// Keyword weight depending on the number of times it appears in a page is capped at this value
$word_upper_bound	= 100;

// Index numbers as well
$index_numbers		= 1;

// if this value is set to 1, word in domain name and url path are also indexed,// so that for example the index of www.php.net returns a positive answer to query 'php' even 	// if the word is not included in the page itself.
$index_host		 = 0;


// Wether to index keywords in a meta tag 
$index_meta_keywords = 1;

// Index pdf files
$index_pdf	= 0;

// Index doc files
$index_doc	= 0;

// Index xls files
$index_xls	= 0;

// Index ppt files
$index_ppt	= 0;

//executable path to pdf converter
$pdftotext_path	= 'c:\temp\pdftotext.exe';

//executable path to doc converter
$catdoc_path	= 'c:\temp\catdoc.exe';

//executable path to xls converter
$xls2csv_path	= 'c:\temp\xls2csv';

//executable path to ppt converter
$catppt_path	= 'c:\temp\catppt';

// User agent string 
$user_agent			 = 'Sphider';

// Minimal delay between page downloads 
$min_delay			= 0;

// Use word stemming (e.g. find sites containing runs and running when searching for run) 
$stem_words			= 0;

// Strip session ids (PHPSESSID, JSESSIONID, ASPSESSIONID, sid) 
$strip_sessids			= 1;


/*********************** 
Search settings 
***********************/

// default for number of results per page
$results_per_page	= 10;

// Number of columns for categories. If you increase this, you might also want to increase the category table with in the css file
$cat_columns		= 2;

// Can speed up searches on large database (should be 0)
$bound_search_result = 0;

// The length of the description string queried when displaying search results. // If set to 0 (default), makes a query for the whole page text, // otherwise queries this many bytes. Can significantly speed up searching on very slow machines 
$length_of_link_desc	= 0;

// Number of links shown to next pages
$links_to_next		 = 9;

// Show meta description in results page if it exists, otherwise show an extract from the page text.
$show_meta_description = 1;

// Advanced query form, shows and/or buttons
$advanced_search	= 0;

// Query scores are not shown if set to 0
$show_query_scores	 = 1;	



 // Display category list
$show_categories	 = 1;

// Length of page description given in results page
$desc_length		= 250;

// Show only the 2 most relevant links from each site (a la google)
$merge_site_results		= 0;

// Enable spelling suggestions (Did you mean...)
$did_you_mean_enabled	= 1;

// Enable Sphider Suggest 
$suggest_enabled		= 1;

// Search for suggestions in query log 
$suggest_history		= 1;

// Search for suggestions in keywords 
$suggest_keywords		= 0;

// Search for suggestions in phrases 
$suggest_phrases		= 0;

// Limit number of suggestions 
$suggest_rows		= 10;


/*********************** 
Weights
***********************/

// Relative weight of a word in the title of a webpage
$title_weight  = 20;

// Relative weight of a word in the domain name
$domain_weight = 60;

// Relative weight of a word in the path name
$path_weight	= 10;

// Relative weight of a word in meta_keywords
$meta_weight	= 5;

function get_categories_view() {
	global $mysql_table_prefix;
	$categories['main_list'] = sql_fetch_all('SELECT * FROM '.$mysql_table_prefix.'categories WHERE parent_num=0 ORDER BY category');
		
	if (is_array($categories['main_list'])) {
		foreach ($categories['main_list'] as $_key => $_val) {
			$categories['main_list'][$_key]['sub'] =  sql_fetch_all('SELECT * FROM '.$mysql_table_prefix.'categories WHERE parent_num='.$_val['category_id']);
		}
	}
	return $categories;
}

function get_category_info($catid) {
	global $mysql_table_prefix;
	$categories['main_list'] = sql_fetch_all("SELECT * FROM ".$mysql_table_prefix."categories ORDER BY category");
	
	if (is_array($categories['main_list'])) {
		foreach($categories['main_list'] as $_val) {
			$categories['categories'][$_val['category_id']] = $_val;
			$categories['subcats'][$_val['parent_num']][] = $_val;
		}
	}
	
	$categories['subcats'] = $categories['subcats'][$_REQUEST['catid']];
	
	/* count sites */
	if (is_array($categories['subcats'])) {
		foreach ($categories['subcats'] as $_key => $_val) {
			$categories['subcats'][$_key]['count'] = sql_fetch_all('SELECT count(*) FROM '.$mysql_table_prefix.'site_category WHERE 	category_id='.(int)$_val['category_id']);
		}
	}
		
	/* make tree */	
	$_parent = $catid;
	while ($_parent) {
		$categories['cat_tree'][] = $categories['categories'][$_parent];
		$_parent = $categories['categories'][$_parent]['parent_num'];
	}
	$categories['cat_tree'] = array_reverse($categories['cat_tree']);
	
	
	/* list category sites */
	$categories['cat_sites'] = sql_fetch_all('SELECT url, title, short_desc FROM '.$mysql_table_prefix.'sites, '.$mysql_table_prefix.'site_category WHERE category_id='.$catid.' AND '.$mysql_table_prefix.'sites.site_id='.$mysql_table_prefix.'site_category.site_id order by title');
	
	return $categories;
}


	/**
	* Returns the result of a query as an array
	* 
	* @param string $query SQL p�ring stringina
	* @return array|null massiiv
	 */
	function sql_fetch_all($query) {
		$result = mysql_query($query);
		if($mysql_err = mysql_errno()) {
			print $query.'<br>'.mysql_error();
		} else {
			while($row=mysql_fetch_array($result)) {
				$data[]=$row;
			}	
		}		
		return $data;
	}



	/*
	Removes duplicate elements from an array
	*/
	function distinct_array($arr) {
		rsort($arr);
		reset($arr);
		$newarr = array();
		$i = 0;
		$element = current($arr);

		for ($n = 0; $n < sizeof($arr); $n++) {
			if (next($arr) != $element) {
				$newarr[$i] = $element;
				$element = current($arr);
				$i++;
			}
		}

		return $newarr;
	}

	function get_cats($parent) {
		global $mysql_table_prefix;
		$query = "SELECT * FROM ".$mysql_table_prefix."categories WHERE parent_num=$parent";
		echo mysql_error();
		$result = mysql_query($query);
		$arr[] = $parent;
		if (mysql_num_rows($result) <> '') {
			while ($row = mysql_fetch_array($result)) {
				$id = $row[category_id];
				$arr = add_arrays($arr, get_cats($id));
			}
		}

		return $arr;
	}
	
	function add_arrays($arr1, $arr2) {
		foreach ($arr2 as $elem) {
			$arr1[] = $elem;
		}
		return $arr1;
	}

	$entities = array
		(
		"&amp" => "&",
		"&apos" => "'",
		"&THORN;"  => "�",
		"&szlig;"  => "�",
		"&agrave;" => "�",
		"&aacute;" => "�",
		"&acirc;"  => "�",
		"&atilde;" => "�",
		"&auml;"   => "�",
		"&aring;"  => "�",
		"&aelig;"  => "�",
		"&ccedil;" => "�",
		"&egrave;" => "�",
		"&eacute;" => "�",
		"&ecirc;"  => "�",
		"&euml;"   => "�",
		"&igrave;" => "�",
		"&iacute;" => "�",
		"&icirc;"  => "�",
		"&iuml;"   => "�",
		"&eth;"    => "�",
		"&ntilde;" => "�",
		"&ograve;" => "�",
		"&oacute;" => "�",
		"&ocirc;"  => "�",
		"&otilde;" => "�",
		"&ouml;"   => "�",
		"&oslash;" => "�",
		"&ugrave;" => "�",
		"&uacute;" => "�",
		"&ucirc;"  => "�",
		"&uuml;"   => "�",
		"&yacute;" => "�",
		"&thorn;"  => "�",
		"&yuml;"   => "�",
		"&THORN;"  => "�",
		"&szlig;"  => "�",
		"&Agrave;" => "�",
		"&Aacute;" => "�",
		"&Acirc;"  => "�",
		"&Atilde;" => "�",
		"&Auml;"   => "�",
		"&Aring;"  => "�",
		"&Aelig;"  => "�",
		"&Ccedil;" => "�",
		"&Egrave;" => "�",
		"&Eacute;" => "�",
		"&Ecirc;"  => "�",
		"&Euml;"   => "�",
		"&Igrave;" => "�",
		"&Iacute;" => "�",
		"&Icirc;"  => "�",
		"&Iuml;"   => "�",
		"&ETH;"    => "�",
		"&Ntilde;" => "�",
		"&Ograve;" => "�",
		"&Oacute;" => "�",
		"&Ocirc;"  => "�",
		"&Otilde;" => "�",
		"&Ouml;"   => "�",
		"&Oslash;" => "�",
		"&Ugrave;" => "�",
		"&Uacute;" => "�",
		"&Ucirc;"  => "�",
		"&Uuml;"   => "�",
		"&Yacute;" => "�",
		"&Yhorn;"  => "�",
		"&Yuml;"   => "�"
		);

	//Apache multi indexes parameters
	$apache_indexes = array (  
		"N=A" => 1,
		"N=D" => 1,
		"M=A" => 1,
		"M=D" => 1,
		"S=A" => 1,
		"S=D" => 1,
		"D=A" => 1,
		"D=D" => 1,
		"C=N;O=A" => 1,
		"C=M;O=A" => 1,
		"C=S;O=A" => 1,
		"C=D;O=A" => 1,
		"C=N;O=D" => 1,
		"C=M;O=D" => 1,
		"C=S;O=D" => 1,
		"C=D;O=D" => 1);


	function remove_accents($string) {
		return (strtr($string, "�������������������������������������������������������������",
					  "aaaaaaaaaaaaaaoooooooooooooeeeeeeeeecceiiiiiiiiuuuuuuuunntsyy"));
	}

	$common = array
		(
		);

	$lines = @file($include_dir.'/common.txt');

	if (is_array($lines)) {
		while (list($id, $word) = each($lines))
			$common[trim($word)] = 1;
	}

	$ext = array
		(
		);

	$lines = @file('ext.txt');

	if (is_array($lines)) {
		while (list($id, $word) = each($lines))
			$ext[] = trim($word);
	}

	function is_num($var) {
	   for ($i=0;$i<strlen($var);$i++) {
		   $ascii_code=ord($var[$i]);
		   if ($ascii_code >=49 && $ascii_code <=57){
			   continue;
		   } else {
			   return false;
		   }
	   }
  		   return true;
	}

	function getHttpVars() {
		$superglobs = array(
			'_POST',
			'_GET',
			'HTTP_POST_VARS',
			'HTTP_GET_VARS');

		$httpvars = array();

		// extract the right array
		foreach ($superglobs as $glob) {
			global $$glob;
			if (isset($$glob) && is_array($$glob)) {
				$httpvars = $$glob;
			 }
			if (count($httpvars) > 0)
				break;
		}
		return $httpvars;

	}
function countSubstrs($haystack, $needle) {
	$count = 0;
	while(strpos($haystack,$needle) !== false) {
	   $haystack = substr($haystack, (strpos($haystack,$needle) + 1));
	   $count++;
	}
	return $count;
}

function quote_replace($str) {

		$str = str_replace("\"",
					  "&quot;", $str);
		return str_replace("'","&apos;", $str);
}


function fst_lt_snd($version1, $version2) {

	$list1 = explode(".", $version1);
	$list2 = explode(".", $version2);

	$length = count($list1);
	$i = 0;
	while ($i < $length) {
		if ($list1[$i] < $list2[$i])
			return true;
		if ($list1[$i] > $list2[$i])
			return false;
		$i++;
	}
	
	if ($length < count($list2)) {
		return true;
	}
	return false;

}

function get_dir_contents($dir) {
	$contents = Array();
	if ($handle = opendir($dir)) {
		while (false !== ($file = readdir($handle))) {
			if ($file != "." && $file != "..") {
				$contents[] = $file;
			}
		}
		closedir($handle);
	}
	return $contents;
}

function replace_ampersand($str) {
	return str_replace("&", "%26", $str);
}



    /**
	* Stemming algorithm
    * Copyright (c) 2005 Richard Heyes (http://www.phpguru.org/)
    * All rights reserved.
    * This script is free software.
	* Modified to work with php versions prior 5 by Ando Saabas
    */

	/**
	* Regex for matching a consonant
	*/
	$regex_consonant = '(?:[bcdfghjklmnpqrstvwxz]|(?<=[aeiou])y|^y)';


	/**
	* Regex for matching a vowel
	*/
	$regex_vowel = '(?:[aeiou]|(?<![aeiou])y)';

	/**
	* Stems a word. Simple huh?
	*
	* @param  string $word Word to stem
	* @return string       Stemmed word
	*/
	function stem($word)
	{
		if (strlen($word) <= 2) {
			return $word;
		}

		$word = step1ab($word);
		$word = step1c($word);
		$word = step2($word);
		$word = step3($word);
		$word = step4($word);
		$word = step5($word);

		return $word;
	}


	/**
	* Step 1
	*/
	function step1ab($word)
	{
		global $regex_vowel, $regex_consonant;
		// Part a
		if (substr($word, -1) == 's') {

			   replace($word, 'sses', 'ss')
			OR replace($word, 'ies', 'i')
			OR replace($word, 'ss', 'ss')
			OR replace($word, 's', '');
		}

		// Part b
		if (substr($word, -2, 1) != 'e' OR !replace($word, 'eed', 'ee', 0)) { // First rule
			$v = $regex_vowel;
			// ing and ed
			if (   preg_match("#$v+#", substr($word, 0, -3)) && replace($word, 'ing', '')
				OR preg_match("#$v+#", substr($word, 0, -2)) && replace($word, 'ed', '')) { // Note use of && and OR, for precedence reasons

				// If one of above two test successful
				if (    !replace($word, 'at', 'ate')
					AND !replace($word, 'bl', 'ble')
					AND !replace($word, 'iz', 'ize')) {

					// Double consonant ending
					if (    doubleConsonant($word)
						AND substr($word, -2) != 'll'
						AND substr($word, -2) != 'ss'
						AND substr($word, -2) != 'zz') {

						$word = substr($word, 0, -1);

					} else if (m($word) == 1 AND cvc($word)) {
						$word .= 'e';
					}
				}
			}
		}

		return $word;
	}


	/**
	* Step 1c
	*
	* @param string $word Word to stem
	*/
	function step1c($word)
	{
		global $regex_vowel, $regex_consonant;
		$v = $regex_vowel;

		if (substr($word, -1) == 'y' && preg_match("#$v+#", substr($word, 0, -1))) {
			replace($word, 'y', 'i');
		}

		return $word;
	}


	/**
	* Step 2
	*
	* @param string $word Word to stem
	*/
	function step2($word)
	{
		switch (substr($word, -2, 1)) {
			case 'a':
				   replace($word, 'ational', 'ate', 0)
				OR replace($word, 'tional', 'tion', 0);
				break;

			case 'c':
				   replace($word, 'enci', 'ence', 0)
				OR replace($word, 'anci', 'ance', 0);
				break;

			case 'e':
				replace($word, 'izer', 'ize', 0);
				break;

			case 'g':
				replace($word, 'logi', 'log', 0);
				break;

			case 'l':
				   replace($word, 'entli', 'ent', 0)
				OR replace($word, 'ousli', 'ous', 0)
				OR replace($word, 'alli', 'al', 0)
				OR replace($word, 'bli', 'ble', 0)
				OR replace($word, 'eli', 'e', 0);
				break;

			case 'o':
				   replace($word, 'ization', 'ize', 0)
				OR replace($word, 'ation', 'ate', 0)
				OR replace($word, 'ator', 'ate', 0);
				break;

			case 's':
				   replace($word, 'iveness', 'ive', 0)
				OR replace($word, 'fulness', 'ful', 0)
				OR replace($word, 'ousness', 'ous', 0)
				OR replace($word, 'alism', 'al', 0);
				break;

			case 't':
				   replace($word, 'biliti', 'ble', 0)
				OR replace($word, 'aliti', 'al', 0)
				OR replace($word, 'iviti', 'ive', 0);
				break;
		}

		return $word;
	}


	/**
	* Step 3
	*
	* @param string $word String to stem
	*/
	function step3($word)
	{
		switch (substr($word, -2, 1)) {
			case 'a':
				replace($word, 'ical', 'ic', 0);
				break;

			case 's':
				replace($word, 'ness', '', 0);
				break;

			case 't':
				   replace($word, 'icate', 'ic', 0)
				OR replace($word, 'iciti', 'ic', 0);
				break;

			case 'u':
				replace($word, 'ful', '', 0);
				break;

			case 'v':
				replace($word, 'ative', '', 0);
				break;

			case 'z':
				replace($word, 'alize', 'al', 0);
				break;
		}

		return $word;
	}


	/**
	* Step 4
	*
	* @param string $word Word to stem
	*/
	function step4($word)
	{
		switch (substr($word, -2, 1)) {
			case 'a':
				replace($word, 'al', '', 1);
				break;

			case 'c':
				   replace($word, 'ance', '', 1)
				OR replace($word, 'ence', '', 1);
				break;

			case 'e':
				replace($word, 'er', '', 1);
				break;

			case 'i':
				replace($word, 'ic', '', 1);
				break;

			case 'l':
				   replace($word, 'able', '', 1)
				OR replace($word, 'ible', '', 1);
				break;

			case 'n':
				   replace($word, 'ant', '', 1)
				OR replace($word, 'ement', '', 1)
				OR replace($word, 'ment', '', 1)
				OR replace($word, 'ent', '', 1);
				break;

			case 'o':
				if (substr($word, -4) == 'tion' OR substr($word, -4) == 'sion') {
				   replace($word, 'ion', '', 1);
				} else {
					replace($word, 'ou', '', 1);
				}
				break;

			case 's':
				replace($word, 'ism', '', 1);
				break;

			case 't':
				   replace($word, 'ate', '', 1)
				OR replace($word, 'iti', '', 1);
				break;

			case 'u':
				replace($word, 'ous', '', 1);
				break;

			case 'v':
				replace($word, 'ive', '', 1);
				break;

			case 'z':
				replace($word, 'ize', '', 1);
				break;
		}

		return $word;
	}


	/**
	* Step 5
	*
	* @param string $word Word to stem
	*/
	function step5($word)
	{
		// Part a
		if (substr($word, -1) == 'e') {
			if (m(substr($word, 0, -1)) > 1) {
				replace($word, 'e', '');

			} else if (m(substr($word, 0, -1)) == 1) {

				if (!cvc(substr($word, 0, -1))) {
					replace($word, 'e', '');
				}
			}
		}

		// Part b
		if (m($word) > 1 AND doubleConsonant($word) AND substr($word, -1) == 'l') {
			$word = substr($word, 0, -1);
		}

		return $word;
	}


	/**
	* Replaces the first string with the second, at the end of the string. If third
	* arg is given, then the preceding string must match that m count at least.
	*
	* @param  string $str   String to check
	* @param  string $check Ending to check for
	* @param  string $repl  Replacement string
	* @param  int    $m     Optional minimum number of m() to meet
	* @return bool          Whether the $check string was at the end
	*                       of the $str string. True does not necessarily mean
	*                       that it was replaced.
	*/
	function replace(&$str, $check, $repl, $m = null)
	{
		$len = 0 - strlen($check);

		if (substr($str, $len) == $check) {
			$substr = substr($str, 0, $len);
			if (is_null($m) OR m($substr) > $m) {
				$str = $substr . $repl;
			}

			return true;
		}

		return false;
	}


	/**
	* What, you mean it's not obvious from the name?
	*
	* m() measures the number of consonant sequences in $str. if c is
	* a consonant sequence and v a vowel sequence, and <..> indicates arbitrary
	* presence,
	*
	* <c><v>       gives 0
	* <c>vc<v>     gives 1
	* <c>vcvc<v>   gives 2
	* <c>vcvcvc<v> gives 3
	*
	* @param  string $str The string to return the m count for
	* @return int         The m count
	*/
	function m($str)
	{
		global $regex_vowel, $regex_consonant;
		$c = $regex_consonant;
		$v = $regex_vowel;

		$str = preg_replace("#^$c+#", '', $str);
		$str = preg_replace("#$v+$#", '', $str);

		preg_match_all("#($v+$c+)#", $str, $matches);

		return count($matches[1]);
	}


	/**
	* Returns true/false as to whether the given string contains two
	* of the same consonant next to each other at the end of the string.
	*
	* @param  string $str String to check
	* @return bool        Result
	*/
	function doubleConsonant($str)
	{
		global $regex_consonant;
		$c = $regex_consonant;

		return preg_match("#$c{2}$#", $str, $matches) AND $matches[0]{0} == $matches[0]{1};
	}


	/**
	* Checks for ending CVC sequence where second C is not W, X or Y
	*
	* @param  string $str String to check
	* @return bool        Result
	*/
	function cvc($str)
	{
		$c = $regex_consonant;
		$v = $regex_vowel;

		return     preg_match("#($c$v$c)$#", $str, $matches)
			   AND strlen($matches[1]) == 3
			   AND $matches[1]{2} != 'w'
			   AND $matches[1]{2} != 'x'
			   AND $matches[1]{2} != 'y';
	}
	
	function swap_max (&$arr, $start, $domain) {
		$pos  = $start;
		$maxweight = $arr[$pos]['weight'];
		for  ($i = $start; $i< count($arr); $i++) {
			if ($arr[$i]['domain'] == $domain) {
				$pos = $i;
				$maxweight = $arr[$i]['weight'];
				break;
			}
			if ($arr[$i]['weight'] > $maxweight) {
				$pos = $i;
				$maxweight = $arr[$i]['weight'];
			}
		}
		$temp = $arr[$start];
		$arr[$start] = $arr[$pos];
		$arr[$pos] = $temp;
	}

	function sort_with_domains (&$arr) {
		$domain = -1;
		for  ($i = 0; $i< count($arr)-1; $i++) {
			swap_max($arr, $i, $domain);
			$domain = $arr[$i]['domain'];
		}
	}
	
	function cmp($a, $b) {
		if ($a['weight'] == $b['weight'])
			return 0;

		return ($a['weight'] > $b['weight']) ? -1 : 1;
	}

	function addmarks($a) {
		$a = preg_replace("/[ ]+/", " ", $a);
		$a = str_replace(" +", "+", $a);
		$a = str_replace(" ", "+", $a);
		return $a;
	}

	function makeboollist($a) {
		global $entities, $stem_words;
		$entities = array
		(
		"&amp" => "&",
		"&apos" => "'",
		"&THORN;"  => "�",
		"&szlig;"  => "�",
		"&agrave;" => "�",
		"&aacute;" => "�",
		"&acirc;"  => "�",
		"&atilde;" => "�",
		"&auml;"   => "�",
		"&aring;"  => "�",
		"&aelig;"  => "�",
		"&ccedil;" => "�",
		"&egrave;" => "�",
		"&eacute;" => "�",
		"&ecirc;"  => "�",
		"&euml;"   => "�",
		"&igrave;" => "�",
		"&iacute;" => "�",
		"&icirc;"  => "�",
		"&iuml;"   => "�",
		"&eth;"    => "�",
		"&ntilde;" => "�",
		"&ograve;" => "�",
		"&oacute;" => "�",
		"&ocirc;"  => "�",
		"&otilde;" => "�",
		"&ouml;"   => "�",
		"&oslash;" => "�",
		"&ugrave;" => "�",
		"&uacute;" => "�",
		"&ucirc;"  => "�",
		"&uuml;"   => "�",
		"&yacute;" => "�",
		"&thorn;"  => "�",
		"&yuml;"   => "�",
		"&THORN;"  => "�",
		"&szlig;"  => "�",
		"&Agrave;" => "�",
		"&Aacute;" => "�",
		"&Acirc;"  => "�",
		"&Atilde;" => "�",
		"&Auml;"   => "�",
		"&Aring;"  => "�",
		"&Aelig;"  => "�",
		"&Ccedil;" => "�",
		"&Egrave;" => "�",
		"&Eacute;" => "�",
		"&Ecirc;"  => "�",
		"&Euml;"   => "�",
		"&Igrave;" => "�",
		"&Iacute;" => "�",
		"&Icirc;"  => "�",
		"&Iuml;"   => "�",
		"&ETH;"    => "�",
		"&Ntilde;" => "�",
		"&Ograve;" => "�",
		"&Oacute;" => "�",
		"&Ocirc;"  => "�",
		"&Otilde;" => "�",
		"&Ouml;"   => "�",
		"&Oslash;" => "�",
		"&Ugrave;" => "�",
		"&Uacute;" => "�",
		"&Ucirc;"  => "�",
		"&Uuml;"   => "�",
		"&Yacute;" => "�",
		"&Yhorn;"  => "�",
		"&Yuml;"   => "�"
		);
		while ($char = each($entities)) {
			$a = preg_replace("/".$char[0]."/i", $char[1], $a);
		}
		$a = trim($a);

		$a = preg_replace("/&quot;/i", "\"", $a);
		$returnWords = array();
		//get all phrases
		$regs = Array();
		while (preg_match("/([-]?)\"([^\"]+)\"/", $a, $regs)) {
			if ($regs[1] == '') {
				$returnWords['+s'][] = $regs[2];
				$returnWords['hilight'][] = $regs[2];
			} else {
				$returnWords['-s'][] = $regs[2];
			}
			$a = str_replace($regs[0], "", $a);
		}
		$a = strtolower(preg_replace("/[ ]+/", " ", $a));
//		$a = remove_accents($a);
		$a = trim($a);
		$words = explode(' ', $a);
		if ($a=="") {
			$limit = 0;
		} else {
		$limit = count($words);
		}


		$k = 0;
		//get all words (both include and exlude)
		$includeWords = array();
		while ($k < $limit) {
			if (substr($words[$k], 0, 1) == '+') {
				$includeWords[] = substr($words[$k], 1);
				if (!ignoreWord(substr($words[$k], 1))) {
					$returnWords['hilight'][] = substr($words[$k], 1);
					if ($stem_words == 1) {
						$returnWords['hilight'][] = stem(substr($words[$k], 1));
					}
				}
			} else if (substr($words[$k], 0, 1) == '-') {
				$returnWords['-'][] = substr($words[$k], 1);
			} else {
				$includeWords[] = $words[$k];
				if (!ignoreWord($words[$k])) {
					$returnWords['hilight'][] = $words[$k];
					if ($stem_words == 1) {
						$returnWords['hilight'][] = stem($words[$k]);
					}
				}
			}
			$k++;
		}
		//add words from phrases to includes
		if (isset($returnWords['+s'])) {
			foreach ($returnWords['+s'] as $phrase) {
				$phrase = strtolower(preg_replace("/[ ]+/", " ", $phrase));
				$phrase = trim($phrase);
				$temparr = explode(' ', $phrase);
				foreach ($temparr as $w)
					$includeWords[] = $w;
			}
		}

		foreach ($includeWords as $word) {
			if (!($word =='')) {
				if (ignoreWord($word)) {

					$returnWords['ignore'][] = $word;
				} else {
					$returnWords['+'][] = $word;
				}	
			}

		}
		
		return $returnWords;

	}

	function ignoreword($word) {
		global $common;
		global $min_word_length;
		global $index_numbers;
		if ($index_numbers == 1) {
			$pattern = "[a-z0-9]+";
		} else {
			$pattern = "[a-z]+";
		}
		if (strlen($word) < $min_word_length || (!preg_match("/".$pattern."/i", remove_accents($word))) || ($common[$word] == 1)) {
			return 1;
		} else {
			return 0;
		}
	}

	function search($searchstr, $category, $start, $per_page, $type, $domain) {
		global $length_of_link_desc,$mysql_table_prefix, $show_meta_description, $merge_site_results, $stem_words, $did_you_mean_enabled ;
		
		$possible_to_find = 1;
		$result = mysql_query("select domain_id from ".$mysql_table_prefix."domains where domain = '$domain'");
		if (mysql_num_rows($result)> 0) {
			$thisrow = mysql_fetch_array($result);
			$domain_qry = "and domain = ".$thisrow[0];
		} else {
			$domain_qry = "";
		}

		//find all sites that should not be included in the result
		if (count($searchstr['+']) == 0) {
			return null;
		}
		$wordarray = $searchstr['-'];
		$notlist = array();
		$not_words = 0;
		while ($not_words < count($wordarray)) {
			if ($stem_words == 1) {
				$searchword = addslashes(stem($wordarray[$not_words]));
			} else {
				$searchword = addslashes($wordarray[$not_words]);
			}
			$wordmd5 = substr(md5($searchword), 0, 1);

            $query1 = "SELECT link_id from ".$mysql_table_prefix."link_keyword$wordmd5, ".$mysql_table_prefix."keywords where ".$mysql_table_prefix."link_keyword$wordmd5.keyword_id= ".$mysql_table_prefix."keywords.keyword_id and keyword='$searchword'";

			$result = mysql_query($query1);

			while ($row = mysql_fetch_row($result)) {	
				$notlist[$not_words]['id'][$row[0]] = 1;
			}
			$not_words++;
		}
		

		//find all sites containing the search phrase
		$wordarray = $searchstr['+s'];
		$phrase_words = 0;
		while ($phrase_words < count($wordarray)) {

			$searchword = addslashes($wordarray[$phrase_words]);
			$query1 = "SELECT link_id from ".$mysql_table_prefix."links where fulltxt like '% $searchword%'";
			echo mysql_error();
			$result = mysql_query($query1);
			$num_rows = mysql_num_rows($result);
			if ($num_rows == 0) {
				$possible_to_find = 0;
				break;
			}
			while ($row = mysql_fetch_row($result)) {	
				$phraselist[$phrase_words]['id'][$row[0]] = 1;
			}
			$phrase_words++;
		}
		

		if (($category> 0) && $possible_to_find==1) {
			$allcats = get_cats($category);
			$catlist = implode(",", $allcats);
			$query1 = "select link_id from ".$mysql_table_prefix."links, ".$mysql_table_prefix."sites, ".$mysql_table_prefix."categories, ".$mysql_table_prefix."site_category where ".$mysql_table_prefix."links.site_id = ".$mysql_table_prefix."sites.site_id and ".$mysql_table_prefix."sites.site_id = ".$mysql_table_prefix."site_category.site_id and ".$mysql_table_prefix."site_category.category_id in ($catlist)";
			$result = mysql_query($query1);
			echo mysql_error();
			$num_rows = mysql_num_rows($result);
			if ($num_rows == 0) {
				$possible_to_find = 0;
			}
			while ($row = mysql_fetch_row($result)) {	
				$category_list[$row[0]] = 1;
			}
		}


		//find all sites that include the search word		
		$wordarray = $searchstr['+'];
		$words = 0;
		$starttime = getmicrotime();
		while (($words < count($wordarray)) && $possible_to_find == 1) {
			if ($stem_words == 1) {
				$searchword = addslashes(stem($wordarray[$words]));
			} else {
				$searchword = addslashes($wordarray[$words]);
			}
			$wordmd5 = substr(md5($searchword), 0, 1);
			$query1 = "SELECT distinct link_id, weight, domain from ".$mysql_table_prefix."link_keyword$wordmd5, ".$mysql_table_prefix."keywords where ".$mysql_table_prefix."link_keyword$wordmd5.keyword_id= ".$mysql_table_prefix."keywords.keyword_id and keyword='$searchword' $domain_qry order by weight desc";
			echo mysql_error();
			$result = mysql_query($query1);
			$num_rows = mysql_num_rows($result);
			if ($num_rows == 0) {
				if ($type != "or") {
					$possible_to_find = 0;
					break;
				}
			}
			if ($type == "or") {
				$indx = 0;
			} else {
				$indx = $words;
			}

			while ($row = mysql_fetch_row($result)) {	
				$linklist[$indx]['id'][] = $row[0];
				$domains[$row[0]] = $row[2];
				$linklist[$indx]['weight'][$row[0]] = $row[1];
			}
			$words++;
		}


		if ($type == "or") {
			$words = 1;
		}
		$result_array_full = Array();

		if ($possible_to_find !=0) {
			if ($words == 1 && $not_words == 0 && $category < 1) { //if there is only one search word, we already have the result
				$result_array_full = $linklist[0]['weight'];
			} else { //otherwise build an intersection of all the results
				$j= 1;
				$min = 0;
				while ($j < $words) {
					if (count($linklist[$min]['id']) > count($linklist[$j]['id'])) {
						$min = $j;
					}
					$j++;
				}

				$j = 0;


				$temp_array = $linklist[$min]['id'];
				$count = 0;
				while ($j < count($temp_array)) {
					$k = 0; //and word counter
					$n = 0; //not word counter
					$o = 0; //phrase word counter
					$weight = 1;
					$break = 0;
					while ($k < $words && $break== 0) {
						if ($linklist[$k]['weight'][$temp_array[$j]] > 0) {
							$weight = $weight + $linklist[$k]['weight'][$temp_array[$j]];
						} else {
							$break = 1;
						}
						$k++;
					}
					while ($n < $not_words && $break== 0) {
						if ($notlist[$n]['id'][$temp_array[$j]] > 0) {
							$break = 1;
						}
						$n++;
					}				

					while ($o < $phrase_words && $break== 0) {
						if ($phraselist[$n]['id'][$temp_array[$j]] != 1) {
							$break = 1;
						}
						$o++;
					}
					if ($break== 0 && $category > 0 && $category_list[$temp_array[$j]] != 1) {
						$break = 1;
					}

					if ($break == 0) {
						$result_array_full[$temp_array[$j]] = $weight;
						$count ++;
					}
					$j++;
				}
			}
		}
		$end = getmicrotime()- $starttime;


		if ((count($result_array_full) == 0 || $possible_to_find == 0) && $did_you_mean_enabled == 1) {
			reset ($searchstr['+']);
			foreach ($searchstr['+'] as $word) {
				$word = addslashes($word);
				$result = mysql_query("select keyword from ".$mysql_table_prefix."keywords where soundex(keyword) = soundex('$word')");
				$max_distance = 100;
				$near_word ="";
				while ($row=mysql_fetch_row($result)) {
					
					$distance = levenshtein($row[0], $word);
					if ($distance < $max_distance && $distance <4) {
						$max_distance = $distance;
						$near_word = $row[0];
					}
				}

				if ($near_word != "" && $word != $near_word) {
					$near_words[$word] = $near_word;
				}

			}
			$res['did_you_mean'] = $near_words;
			return $res;
		}
		if (count($result_array_full) == 0) {
			return null;
		}
		arsort ($result_array_full);


		if ($merge_site_results == 1 && $domain_qry == "") {
			while (list($key, $value) = each($result_array_full)) {
				if (!isset($domains_to_show[$domains[$key]])) {
					$result_array_temp[$key] = $value;
					$domains_to_show[$domains[$key]] = 1;
				} else if ($domains_to_show[$domains[$key]] ==  1) {
					$domains_to_show[$domains[$key]] = Array ($key => $value);
				}
			}
		} else {
			$result_array_temp = $result_array_full;
		}
	
		
		while (list($key, $value) = each ($result_array_temp)) {
			$result_array[$key] = $value;
			if (isset ($domains_to_show[$domains[$key]]) && $domains_to_show[$domains[$key]] != 1) {
				list ($k, $v) = each($domains_to_show[$domains[$key]]);
				$result_array[$k] = $v;
			}
		}

		$results = count($result_array);

		$keys = array_keys($result_array);
		$maxweight = $result_array[$keys[0]];


		for ($i = ($start -1)*$per_page; $i <min($results, ($start -1)*$per_page + $per_page) ; $i++) {
			$in[] = $keys[$i];

		}
		if (!is_array($in)) {
			$res['results'] = $results;
			return $res;
		}

		$inlist = implode(",", $in);


		if ($length_of_link_desc == 0) {
			$fulltxt = "fulltxt";
		} else {
			$fulltxt = "substring(fulltxt, 1, $length_of_link_desc)";
		}

		$query1 = "SELECT distinct link_id, url, title, description,  $fulltxt, size FROM ".$mysql_table_prefix."links WHERE link_id in ($inlist)";

		$result = mysql_query($query1);
		echo mysql_error();

		$i = 0;
		while ($row = mysql_fetch_row($result)) {
			$res[$i]['title'] = $row[2];
			$res[$i]['url'] = $row[1];
			if ($row[3] != null && $show_meta_description == 1)
				$res[$i]['fulltxt'] = $row[3];
			else 
				$res[$i]['fulltxt'] = $row[4];
			$res[$i]['size'] = $row[5];
			$res[$i]['weight'] = $result_array[$row[0]];
			$dom_result = mysql_query("select domain from ".$mysql_table_prefix."domains where domain_id='".$domains[$row[0]]."'");
			$dom_row = mysql_fetch_row($dom_result);
			$res[$i]['domain'] = $dom_row[0];
			$i++;
		}



		if ($merge_site_results  && $domain_qry == "") {
			sort_with_domains($res);
		} else {
			usort($res, "cmp"); 	
		}
		echo mysql_error();
		$res['maxweight'] = $maxweight;
		$res['results'] = $results;
		return $res;
	/**/
	}

function get_search_results($query, $start, $category, $searchtype, $results, $domain) {
	global $sph_messages, $results_per_page,
		$links_to_next,
		$show_query_scores,
		$mysql_table_prefix,
		$desc_length;
	if ($results != "") {
		$results_per_page = $results;
	}

	if ($searchtype == "phrase") {
	   $query=str_replace('"','',$query);
	   $query = "\"".$query."\"";
	}

	$starttime = getmicrotime();
	// catch " if only one time entered
        if (substr_count($query,'"')==1){
           $query=str_replace('"','',$query);
        }   
	$words = makeboollist($query);
	$ignorewords = $words['ignore'];

	
	$full_result['ignore_words'] = $words['ignore'];

	if ($start==0) 
		$start=1;
	$result = search($words, $category, $start, $results_per_page, $searchtype, $domain);
	$query= stripslashes($query);

	$entitiesQuery = htmlspecialchars($query);
	$full_result['ent_query'] = $entitiesQuery;

	$endtime = getmicrotime() - $starttime;
	$rows = $result['results'];
	$time = round($endtime*100)/100;

	
	$full_result['time'] = $time;
	
	$did_you_mean = "";


	if (isset($result['did_you_mean'])) {
		$did_you_mean_b=$entitiesQuery;
		$did_you_mean=$entitiesQuery;
		while (list($key, $val) = each($result['did_you_mean'])) {
			if ($key != $val) {
				$did_you_mean_b = str_replace($key, "<b>$val</b>", $did_you_mean_b);
				$did_you_mean = str_replace($key, "$val", $did_you_mean);
			}
		}
	}

	$full_result['did_you_mean'] = $did_you_mean;
	$full_result['did_you_mean_b'] = $did_you_mean_b;

	$matchword = $sph_messages["matches"];
	if ($rows == 1) {
		$matchword= $sph_messages["match"];
	}

	$num_of_results = count($result) - 2;
	
	
	
	$full_result['num_of_results'] = $num_of_results;


	if ($start < 2)
		//saveToLog(addslashes($query), $time, $rows);
	$from = ($start-1) * $results_per_page+1;
	$to = min(($start)*$results_per_page, $rows);

	
	$full_result['from'] = $from;
	$full_result['to'] = $to;
	$full_result['total_results'] = $rows;

	if ($rows>0) {
		$maxweight = $result['maxweight'];
		$i = 0;
		while ($i < $num_of_results && $i < $results_per_page) {
			$title = $result[$i]['title'];
			$url = $result[$i]['url'];
			$fulltxt = $result[$i]['fulltxt'];
			$page_size = $result[$i]['size'];
			$domain = $result[$i]['domain'];
			if ($page_size!="") 
				$page_size = number_format($page_size, 1)."kb";
			
			
			$txtlen = strlen($fulltxt);
			if ($txtlen > $desc_length) {
				$places = array();
				foreach($words['hilight'] as $word) {
					$tmp = strtolower($fulltxt);
					$found_in = strpos($tmp, $word);
					$sum = -strlen($word);
					while (!($found_in =='')) {
						$pos = $found_in+strlen($word);
						$sum += $pos;  //FIX!!
						$tmp = substr($tmp, $pos);
						$places[] = $sum;
						$found_in = strpos($tmp, $word);

					}
				}
				sort($places);
				$x = 0;
				$begin = 0;
				$end = 0;
				while(list($id, $place) = each($places)) {
					while ($places[$id + $x] - $place < $desc_length && $x+$id < count($places) && $place < strlen($fulltxt) -$desc_length) {
						$x++;
						$begin = $id;
						$end = $id + $x;
					}
				}

				$begin_pos = max(0, $places[$begin] - 30);
				$fulltxt = substr($fulltxt, $begin_pos, $desc_length);

				if ($places[$begin] > 0) {
					$begin_pos = strpos($fulltxt, " ");
				}
				$fulltxt = substr($fulltxt, $begin_pos, $desc_length);
				$fulltxt = substr($fulltxt, 0, strrpos($fulltxt, " "));
				$fulltxt = $fulltxt;
			}

			$weight = number_format($result[$i]['weight']/$maxweight*100, 2);
			if ($title=='')
				$title = $sph_messages["Untitled"];
			$regs = Array();

			if (strlen($title) > 80) {
				$title = substr($title, 0,76)."...";
			}
			foreach($words['hilight'] as $change) {
				while (preg_match("/[^\>](".$change.")[^\<]/i", " ".$title." ", $regs)) {
					$title = preg_replace("/".$regs[1]."/i", "<b>".$regs[1]."</b>", $title);
				}

				while (preg_match("/[^\>](".$change.")[^\<]/i", " ".$fulltxt." ", $regs)) {
					$fulltxt = preg_replace("/".$regs[1]."/i", "<b>".$regs[1]."</b>", $fulltxt);
				}
				$url2 = $url;
				while (preg_match("/[^\>](".$change.")[^\<]/i", $url2, $regs)) {
					$url2 = preg_replace("/".$regs[1]."/i", "<b>".$regs[1]."</b>", $url2);
				}
			}


			$num = $from + $i;

			$full_result['qry_results'][$i]['num'] =  $num;
			$full_result['qry_results'][$i]['weight'] =  $weight;
			$full_result['qry_results'][$i]['url'] =  $url;
			$full_result['qry_results'][$i]['title'] =  $title;
			$full_result['qry_results'][$i]['fulltxt'] =  $fulltxt;
			$full_result['qry_results'][$i]['url2'] =  $url2;
			$full_result['qry_results'][$i]['page_size'] =  $page_size;
			$full_result['qry_results'][$i]['domain_name'] =  $domain;
			$i++;
		}
	}



	$pages = ceil($rows / $results_per_page);
	$full_result['pages'] = $pages;
	$prev = $start - 1;
	$full_result['prev'] = $prev;
	$next = $start + 1;
	$full_result['next'] = $next;
	$full_result['start'] = $start;
	$full_result['query'] = $entitiesQuery;

	if ($from <= $to) {

		$firstpage = $start - $links_to_next;
		if ($firstpage < 1) $firstpage = 1;
		$lastpage = $start + $links_to_next;
		if ($lastpage > $pages) $lastpage = $pages;

		for ($x=$firstpage; $x<=$lastpage; $x++)
			$full_result['other_pages'][] = $x;

	}

	return $full_result;

}


function getmicrotime(){
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
    }
