<div id="rowInner">
    <div id="conBox" class="hasSidebar">
        <div id="mainBox" class="rounded column">
             <?php foreach($data as $alert){ ?>
            <h1><?php echo stripslashes($alert->title); ?></h1>
            <span class="date"><?php echo date_to_human($alert->date , $format = 'F j, Y') ?></span><img class="smallTag" src="<?php echo imagesPath() ?>/smallTag.png" />
            <span class="date"><?php echo ucwords(get_issue($alert->issue)) ?></span>
            <div class="newsItem">
                <?php echo stripslashes($alert->content); ?>
            </div>
            <?php } ?>
            
        </div>
        
         <div id="sidebar" class="rounded column">
            <?php $this->load->view('templates/joinForm') ?>
            
        </div>
        <br class="clear" />
    </div> <!--end conBox -->
    
</div>

