<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

h2 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 14px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

th {text-align:left}
td.one {width:25%}

</style>
</head>
<body>

<h1>Americans For Prosperity Scorecard API</h1>
<p>All Api request must include a congress number format example: <strong>congress/111</strong></p>
<h2>members</h2>
<p><strong>URL:</strong> api/index.php/scorecard/members/</p>
<table width="100%">
	<tr>
		<th class="one">Method</th>
		<th>Description</th>
	</tr>
	<tr>
		<td class="one">zip</td>
		<td>A zip code search will only return zip searches no other method will be read</td>
	</tr>
	<tr>
		<td class="one">first</td>
		<td>First name search</td>
	</tr>
	<tr>
		<td class="one">last</td>
		<td>Last name search</td>
	</tr>
	<tr>
		<td class="one">state</td>
		<td>State search. value passed as 2 letter abbrv.</td>
	</tr>
	<tr>
		<td class="one">party</td>
		<td>Party search. value passed as 1 letter abbrv</td>
	</tr>
	<tr>
		<td class="one">chamber</td>
		<td>chamber search</td>
	</tr>
</table>

<p><strong>Example:</strong> api/index.php/scorecard/members/congress/111/party/r/chamber/house</p>
</body>
</html>