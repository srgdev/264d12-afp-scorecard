<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
        
        $this->slApiLocation = "http://services.sunlightlabs.com/api/legislators.get?apikey=";
        $this->slZipApiLocation = 'http://congress.api.sunlightfoundation.com/legislators/locate?apikey=';
    }
        
    public function findByZip($zip, $congress){
        $this->crud->use_table('afp_members');
        $results = file_get_contents($this->slZipApiLocation.$this->config->item('SUNLIGHT_LABS_API_KEY')."&zip=".$zip);
        $results = json_decode($results, true);
        $data = $results['results'];
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        $rows = array();
        $rows['results'] = "";
        $rows['zip'] = $zip;
        $states = array();
        $houseUsers = array();
        $i = 0;
        foreach($data as $m){
             // print_r($m['bioguide_id']);   
            $this->crud->use_table('afp_members');
            
            $data = array(
                'congID' => $m['bioguide_id'],
                'congress' => $congress
            );
            
            $member = $this->crud->retrieve($data, 'row', 0, 0, array('id' => 'DESC'));
            if(isset($member->congID)){
                // print_r($member->congID);
            
                
                $memID = $member->congID;
                $rows['results'][$i]['member'] = $member;
                $this->crud->use_table('afp_'.$congress.'_scores');
                $rows['results'][$i]['scores'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
                $this->crud->use_table('afp_lifetime_scores');
                $rows['results'][$i]['lifetime'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
            }
            $i++;
        }
       
        return $rows;
    }  

   public function addScores($members, $congress, $addvotes, $sortVotes){
       $results = array();
       $i = 0;
       $rTotal = 0;
       $dTotal = 0;
       $rscore = 0;
       $dscore = 0;
       $results['results'] = "";
       foreach($members as $member){
           $memID = $member->congID;
           $chamber = $member->chamber;
           $party = $member->party;
           $results['results'][$i]['member'] = $member;
           $this->crud->use_table('afp_'.$congress.'_scores');
           $scores = $this->crud->retrieve(array('memID' => $memID, 'chamber' => $chamber), 'row', 0, 0, array('id' => 'DESC'));
           $results['results'][$i]['scores'] = $scores;
           if($addvotes){
             $results['results'][$i]['votes'] = $this->addVotes($memID, $chamber, $congress, $sortVotes);
           }
           $this->crud->use_table('afp_lifetime_scores');
           $results['results'][$i]['lifetime'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
           
            
            if($party == 'R'){
              $rTotal++; 
              $rscore = $rscore + $scores->score; 
            }else{
              $dTotal++; 
              $dscore = $dscore + $scores->score; 
            }
           $i++;
       }
       if($rTotal > 0){
          $results['ravg'] = round(($rscore/$rTotal));
       }else{
          $results['ravg'] = 0; 
       }

       if($dTotal > 0){
          $results['davg'] = round(($dscore/$dTotal));
       }else{
          $results['davg'] = 0; 
       }
       
       return $results;
   }
   
   public function addVotes($mem, $chamber, $congress, $sortVotes  ){
       $votesArray = array();
       $this->crud->use_table('afp_key_votes');
       if($sortVotes) {$sortVotes = $sortVotes;}else{$sortVotes = 'id';}
       
       $votes = $this->crud->retrieve(array('chamber' => $chamber, 'congress' => $congress), '', 0, 0, array($sortVotes => 'DESC'));
       $i = 0;
       foreach($votes as $vote){
           $votesArray[$i]['vote'] = $vote;
           $this->crud->use_table('afp_votes');
           $voted = $this->crud->retrieve(array('voteID' => $vote->id, 'memID' => $mem,  'congress' => $congress), 'row', 0, 0, array('id' => 'DESC'));
           if(count($voted) > 0 ) {
            $votesArray[$i]['voted'] = $voted->position;
           }
           $i++;
       }
       return $votesArray;
   }
}
    