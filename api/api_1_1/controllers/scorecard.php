<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Members API
 *
 * 
 *
 * @package     AFP
 * @subpackage  Rest Server
 * @category    Controller
 * @author      Paul Radich
*/
require APPPATH.'/libraries/REST_Controller.php';


class scorecard extends REST_Controller {
        
    function members_get()
    {
        $cache =  $this->uri->uri_string();
        $cache = str_replace('/',"_", $cache);
        $cache = str_replace('scorecard_members_format_json_',"", $cache);
        //echo $cache;
        $cache_path = APPPATH."cache/".$cache.".txt";
        if(file_exists($cache_path)){
            $file = $cache_path;
            $users = unserialize(implode('',file($file)));  
        }else{
            
            //Check That a congress was sent   
            if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
            $data = array();
            $data['congress'] = $this->get('congress');
            
            // Check For a zip code search
            if($this->get('zip')){
                $users = $this->members->findByZip($this->get('zip'), $this->get('congress'));
            }else{
                //else return based on other params
                if($this->get('id')) { $data['congID'] = $this->get('id'); }
                if($this->get('first')){$data['fName'] = $this->get('first');}
                if($this->get('last')){$data['lName'] = $this->get('last');}
                if($this->get('state')){$data['state'] = $this->get('state');}
                if($this->get('party')){$data['party'] = $this->get('party');}
                if($this->get('chamber')){$data['chamber'] = $this->get('chamber');}
                $orderBy = ($this->get('orderBy')) ? $this->get('orderBy') : 'lName';
                $order = ($this->get('order')) ? $this->get('order') : 'ASC';
                
                $this->crud->use_table('afp_members');
                if($orderBy != 'score' && $orderBy != 'lifetime'){
                    $members = $this->crud->retrieve($data, '', 0, 0, array($orderBy => $order));
                }
                
                if($orderBy == 'score'){
                    $query = $this->db->query('SELECT *
                            FROM afp_members, afp_'.$this->get('congress').'_scores  
                            WHERE afp_members.congID = afp_'.$this->get('congress').'_scores.memID
                            AND afp_members.congress = '.$this->get('congress').'
                            ORDER BY afp_'.$this->get('congress').'_scores.score DESC
                    ');
                $members = $query->result();
                }
                
                if($orderBy == 'lifetime'){
                    $query = $this->db->query('SELECT *
                            FROM afp_members, afp_lifetime_scores, afp_'.$this->get('congress').'_scores  
                            WHERE afp_members.congID = afp_lifetime_scores.memID
                            AND afp_members.congID = afp_'.$this->get('congress').'_scores.memID
                            AND afp_members.congress = '.$this->get('congress').'
                            ORDER BY afp_lifetime_scores.lifeScore DESC
                    ');
                $members = $query->result();
                }
                
                //Adding in the good stuff (scores)
                $users = $this->members->addScores($members, $this->get('congress'), $this->get('votes'), $this->get('sortvotes') );
            }

            if($this->get('cache') == 'true'){
                $cacheData = serialize($users);
                $fp = fopen($cache_path,"w");
                fputs($fp, $cacheData);
                fclose($fp);
            }
            
        }

        //return results if we found them
        if(count($users) > 0){
            $this->response($users, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'User could not be found'), 404);
        }
    } 
        
        
        
        
        
        
        
        
        
    public function keyvotes_get(){
        $cache =  $this->uri->uri_string();
        $cache = str_replace('/',"_", $cache);
        $cache = str_replace('scorecard_keyvotes_format_json_',"", $cache);
        //echo $cache;
        $cache_path = APPPATH."cache/".$cache.".txt";
        if(file_exists($cache_path)){
            $file = $cache_path;
            $votes = unserialize(implode('',file($file)));  
        }else{
            //Check That a congress was sent   
            if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
            $data = array();
            $data['congress'] = $this->get('congress');
            $voteSort = 'id';
            
            //return based on params
            if($this->get('chamber')) { $data['chamber'] = $this->get('chamber'); }
            if($this->get('issue')){$data['issue'] = $this->get('issue');}
            if($this->get('id')){$data['id'] = $this->get('id');}
            if($this->get('bill')){$data['bill_id'] = $this->get('bill');}
            if($this->get('position')){$data['position'] = $this->get('position');}
            if($this->get('sort')){
                $sort = $this->get('sort');
                if($sort == 'vote_date' || $sort == 'chamber' || $sort == 'issue' || $sort == 'roll_call'){
                    $voteSort = $sort;
                    $sort = 'id';
                }
            }else{
               $sort = 'id';
            }
            
            
            // retrun results for a given vote (By ID)
            $this->crud->use_table('afp_key_votes');
            $allvotes = $this->crud->retrieve($data, '', 0, 0, array($voteSort => 'DESC'));
            $votes = array();
            $i = 0;
            $votes['results'] = '';
            foreach($allvotes as $v){
                $votes['results'][$i]['vote'] = $v;
                $chamber = strtolower($v->chamber);
               
                $query = $this->db->query('SELECT *
                    FROM afp_votes, afp_members, afp_'.$this->get('congress').'_scores 
                    WHERE afp_votes.voteID = "'.$v->id.'"
                    AND afp_members.congID = afp_votes.memID
                    AND afp_members.chamber = "'.$chamber.'"
                    AND afp_'.$this->get('congress').'_scores.chamber = "'.$chamber.'"
                    AND afp_members.congress = afp_votes.congress
                    AND afp_members.congID = afp_'.$this->get('congress').'_scores.memID
                    ORDER BY afp_members.'.$sort.'
                ');
               if($this->get('id')){
                  $votes['results'][$i]['members'] = $query->result();
               }
                
                //Calc Vote Party Averages
                $afp_position = $v->position;
                $rTotal = 0;
                $dTotal = 0;
                $rGood = 0;
                $dGood = 0;
                $yes = 0;
                $no = 0;
                
                foreach($query->result() as $mem){
                    if($mem->party == "R"){
                       $rTotal++;
                       if($afp_position == strtolower($mem->position)) {$rGood++;}
                    }else{
                       $dTotal++;
                       if($afp_position == strtolower($mem->position)) {$dGood++;}
                    }
                    
                    if($mem->position == 'Yes'){$yes++;}
                    if($mem->position == 'No'){$no++;}
                }
                
                $votes['results'][$i]['Yes'] = $yes;
                $votes['results'][$i]['No'] = $no;
                
                if($rTotal != 0 && $dTotal != 0){
                    $votes['results'][$i]['ravg'] = round(($rGood/$rTotal) * 100);
                    $votes['results'][$i]['davg'] = round(($dGood/$dTotal) * 100);
                }else{
                    $votes['results'][$i]['ravg'] = 0;
                    $votes['results'][$i]['davg'] = 0;
                }
                
                $i++;   
            }
            
            if($this->get('issue')){
                $this->crud->use_table('afp_issues');
                $votes['issue'] = $this->crud->retrieve(array('issue_code' => $this->get('issue')), 'row', 0, 0, array('id' => 'DESC'));
                $this->crud->use_table('cms_pages');
                $votes['voteAlert'] = $this->crud->retrieve(array('issue' => $this->get('issue')), 'row', 0, 0, array('id' => 'DESC'));
            }
            
            if($this->get('cache') == 'true'){
                $cacheData = serialize($votes);
                $fp = fopen($cache_path,"w");
                fputs($fp, $cacheData);
                fclose($fp);
            }
        }


        if(count($votes) > 0){
            $this->response($votes, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'No votes found for congress '.$this->get('congress')), 404);
        }
    }    












     public function lastnames_get(){
        //Check That a congress was sent   
        if(!$this->get('congress')){$this->response(array('error' => 'No Congress Provided'), 404);}
        $data = array();
        $data['congress'] = $this->get('congress');
        
        
        $this->crud->use_table('afp_members');
        $this->db->like('lName', $this->get('query')); 
        $names = $this->crud->retrieve($data, '', 0, 0, array('id' => 'DESC'));
        $lnames = array();
        $result['query'] = $this->get('query');
        foreach($names as $n){
            if(!in_array($n->lName, $lnames)){
                array_push($lnames, $n->lName);
            }
        }
        $result['suggestions'] = $lnames;
        
        if(count($result) > 0){
            $this->response($result, 200); // 200 being the HTTP response code
        }else{
            $this->response(array('error' => 'No votes found for congress '.$this->get('congress')), 404);
        }
    }    
}
    