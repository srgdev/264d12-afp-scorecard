<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends CI_Model {
    
    function __construct()
    {
        parent::__construct();
        
        $this->slApiLocation = "http://services.sunlightlabs.com/api/legislators.get?apikey=";
        $this->slZipApiLocation = 'http://services.sunlightlabs.com/api/districts.getDistrictsFromZip?apikey=';
    }
        
    public function findByZip($zip, $congress){
        $this->crud->use_table('afp_members');
        $results = file_get_contents($this->slZipApiLocation.$this->config->item('SUNLIGHT_LABS_API_KEY')."&zip=".$zip);
        $results = json_decode($results, true);
        $data = $results['response'];
        $rows = array();
        $rows['results'] = "";
        $rows['zip'] = $zip;
        $states = array();
        
        //Loop each return for house district and state
        foreach($data['districts'] as $d) {
            $houseSearch = array(
                'state' => $d['district']['state'],
                'district' => $d['district']['number'],
                'congress' => $congress,
                'chamber' => 'house'
            ); 
            $this->crud->use_table('afp_members');
            $houseUsers = $this->crud->retrieve($houseSearch, '', 0, 0, array('id' => 'DESC'));
            $i = 0;
           foreach($houseUsers as $member){
               $memID = $member->congID;
               $rows['results'][$i]['member'] = $member;
               $this->crud->use_table('afp_'.$congress.'_scores');
               $rows['results'][$i]['scores'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
               $this->crud->use_table('afp_lifetime_scores');
               $rows['results'][$i]['lifetime'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
               $i++;
           }

            //add the state to array
            if(!in_array($d['district']['state'], $states)){array_push($states, $d['district']['state']);}
        }
        
        //Loop states array and add the senate
        foreach($states as $state){
            $senateSearch = array(
                'state' => $state,
                'congress' => $congress,
                'chamber' => 'senate'
            );
        $this->crud->use_table('afp_members');
        $senateUsers = $this->crud->retrieve($senateSearch, '', 0, 0, array('id' => 'DESC'));
       foreach($senateUsers as $member){
           $memID = $member->congID;
           $rows['results'][$i]['member'] = $member;
           $this->crud->use_table('afp_'.$congress.'_scores');
           $rows['results'][$i]['scores'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
           $this->crud->use_table('afp_lifetime_scores');
           $rows['results'][$i]['lifetime'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
           $i++;
       }
        }
        return $rows;
    }  

   public function addScores($members, $congress, $addvotes, $sortVotes){
       $results = array();
       $i = 0;
       $rTotal = 0;
       $dTotal = 0;
       $rscore = 0;
       $dscore = 0;
       $results['results'] = "";
       foreach($members as $member){
           $memID = $member->congID;
           $chamber = $member->chamber;
           $party = $member->party;
           $results['results'][$i]['member'] = $member;
           $this->crud->use_table('afp_'.$congress.'_scores');
           $scores = $this->crud->retrieve(array('memID' => $memID, 'chamber' => $chamber), 'row', 0, 0, array('id' => 'DESC'));
           $results['results'][$i]['scores'] = $scores;
           if($addvotes){
             $results['results'][$i]['votes'] = $this->addVotes($memID, $chamber, $congress, $sortVotes);
           }
           $this->crud->use_table('afp_lifetime_scores');
           $results['results'][$i]['lifetime'] = $this->crud->retrieve(array('memID' => $memID), 'row', 0, 0, array('id' => 'DESC'));
           
            
            if($party == 'R'){
              $rTotal++; 
              $rscore = $rscore + $scores->score; 
            }else{
              $dTotal++; 
              $dscore = $dscore + $scores->score; 
            }
           $i++;
       }
       if($rTotal > 0){
          $results['ravg'] = round(($rscore/$rTotal));
       }else{
          $results['ravg'] = 0; 
       }

       if($dTotal > 0){
          $results['davg'] = round(($dscore/$dTotal));
       }else{
          $results['davg'] = 0; 
       }
       
       return $results;
   }
   
   public function addVotes($mem, $chamber, $congress, $sortVotes  ){
       $votesArray = array();
       $this->crud->use_table('afp_key_votes');
       if($sortVotes) {$sortVotes = $sortVotes;}else{$sortVotes = 'id';}
       
       $votes = $this->crud->retrieve(array('chamber' => $chamber, 'congress' => $congress), '', 0, 0, array($sortVotes => 'DESC'));
       $i = 0;
       foreach($votes as $vote){
           $votesArray[$i]['vote'] = $vote;
           $this->crud->use_table('afp_votes');
           $voted = $this->crud->retrieve(array('voteID' => $vote->id, 'memID' => $mem,  'congress' => $congress), 'row', 0, 0, array('id' => 'DESC'));
           if(count($voted) > 0 ) {
            $votesArray[$i]['voted'] = $voted->position;
           }
           $i++;
       }
       return $votesArray;
   }
}
    