<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


function get_issue($issue){
    $CI =& get_instance();
    $CI->crud->use_table('afp_issues');
    $is = $CI->crud->retrieve(array('issue_code' => $issue), 'row', 0, 0, array('id' => 'DESC'));
    return $is->issue_name;
}

