<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author : Paul Radich
 * 
 * 
 * 
 */

 
 
 function get_full_name(){
     $CI =& get_instance();
     return $CI->session->userdata('first')." ".$CI->session->userdata('last');
 }
 
 function get_data($data){
     $CI =& get_instance();
     return $CI->session->userdata($data);
 }
 
 
