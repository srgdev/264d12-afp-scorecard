<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * @author : Paul Radich
 * 
 * Added to system core for the SRG CMS
 * 
 * 
 */

function cssPath(){
	
	return buildPath()."css";
}

function imgPath(){
	return buildPath()."img";
}

function imagesPath(){
	return buildPath()."images";
}

function jsPath(){
	return buildPath()."js";
}

function sharedCSS(){
	return buildSharedPath()."css";
}


function sharedJS(){
	return buildSharedPath()."js";
}

function sharedImg(){
	return buildSharedPath()."img";
}

function shared(){
	return buildSharedPath();
}

function buildPath(){
	$CI =& get_instance();
	
	$base =  base_url();
	$themePath =  $CI->config->item('theme_location');
	$theme =  $CI->config->item('theme');
	
	return $base.$themePath.$theme;
}

function buildSharedPath(){
	$CI =& get_instance();
	
	$base =  base_url();
	$themePath =  $CI->config->item('theme_location');
	return $base.$themePath."shared_assets/";
}
