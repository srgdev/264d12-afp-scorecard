<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi">
<title>AFP: SCORECARD</title>

<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Pontano+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo cssPath(); ?>/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/memberEmbed.css" rel="stylesheet" type="text/css" />



<script type="text/javascript" src="//use.typekit.net/qdc5wfq.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<!--[if < IE 10]>
<script type="text/javascript" src="js/PIE.js"></script>
<![endif]-->

</head>


<body >
  <div class="memOuter">
      <?php foreach($members as $member){ ?>
          
      <div class="memPic left">
          <?php if($member->image_path != '') {?>
             <img src="<?php echo $member->image_path ?>" /> 
          <?php }else{ ?>
            <img src="<?php echo imagesPath(); ?>/mempic.jpg" />
          <?php } ?>
      </div>
      
      <div class="memtitle right">
          
          <?php $title = ($member->district != '') ? 'Rep.' : 'Sen.' ; ?>
          <?php $title2 = ($member->district != '') ? 'house' : 'senate' ; ?>
          
          <h1> <?php echo $title; ?> <?php echo $member->fName; ?> <?php echo $member->lName; ?></h1>
          <?php $party = ($member->party == 'R') ? 'Republican' : 'Democrat' ; ?>
          <div class="sub"><?php echo $party; ?> (<?php echo $member->state; ?>)  <br /> <?php if($member->district){ echo 'district '.$member->district;} ?></div>
      </div>
      <br class="clear" />
      <table id="scoreTbl" class="rounded" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="<?php if($member->score > 50){echo 'curScore';}else{echo 'lifeScore';} ?>"><?php echo $member->score ?>%</td>
            <td class="<?php if($member->lifeScore > 50){echo 'curScore';}else{echo 'lifeScore';} ?>"><?php echo $member->lifeScore ?>%</td>
          </tr>
          <tr>
            <td class="lScoreLbl">CURRENT SCORE</td>
            <td class="rScoreLbl">LIFETIME SCORE</td>
          </tr>
      </table>
      
     
  </div>
   <div class="footer">
        <a href="http://afpscorecard.org/#member#<?php echo $member->congID.'#'.$title2 ?>" target="_blank"><img src="<?php echo imagesPath(); ?>/memBtn.jpg" /><a>
      </div>
      <?php } ?>
</body>
</html>