<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, target-densitydpi=device-dpi">
<title>AFP: SCORECARD</title>

<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Pontano+Sans' rel='stylesheet' type='text/css'>
<link href="<?php echo cssPath(); ?>/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo cssPath(); ?>/memberEmbed.css" rel="stylesheet" type="text/css" />



<script type="text/javascript" src="//use.typekit.net/qdc5wfq.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<!--[if < IE 10]>
<script type="text/javascript" src="js/PIE.js"></script>
<![endif]-->

</head>


<body >
    
  <?php
  $abrv = $state; 
    switch($state){
        case 'al':
            $state = 'Alabama';
            $img = 'alabama';
            break;
        case 'ak':
            $state = 'Alaska';
            $img = 'alaska';
            break;
        case 'ar':
            $state = 'Arkansas';
            $img = 'arkansas';
            break;    
        case 'az':
            $state = 'Arizona';
            $img = 'arizona';
            break;
        case 'ca':
            $state = 'california';
            $img = 'california';
            break;
        case 'co':
            $state = 'Colorado';
            $img = 'colorado';
            break;
        case 'co':
            $state = 'Colorado';
            $img = 'colorado';
            break;
        case 'ct':
            $state = 'Connecticut';
            $img = 'connecticut';
            break;
        case 'de':
            $state = 'Delaware';
            $img = 'delaware';
            break;
        case 'fl':
            $state = 'Florida';
            $img = 'florida';
            break;
        case 'ga':
            $state = 'Georgia';
            $img = 'georgia';
            break;
        case 'hi':
            $state = 'Hawaii';
            $img = 'hawaii';
            break;
        case 'id':
            $state = 'Idaho';
            $img = 'idaho';
            break;
        case 'il':
            $state = 'Illinois';
            $img = 'illinois';
            break;
        case 'in':
            $state = 'Indiana';
            $img = 'indiana';
            break;
        case 'ia':
            $state = 'Iowa';
            $img = 'iowa';
            break;
       case 'ks':
            $state = 'Kansas';
            $img = 'kansas';
            break;
       case 'ky':
            $state = 'Kentucky';
            $img = 'kentucky';
            break;    
       case 'la':
            $state = 'Louisiana';
            $img = 'louisiana';
            break;     
       case 'me':
            $state = 'Maine';
            $img = 'maine';
            break;    
       case 'md':
            $state = 'Maryland';
            $img = 'maryland';
            break;     
       case 'ma':
            $state = 'Massachusetts';
            $img = 'massachusetts';
            break;     
       case 'mi':
            $state = 'Michigan';
            $img = 'michigan';
            break;     
       case 'mn':
            $state = 'Minnesota';
            $img = 'minnesota';
            break;     
       case 'ms':
            $state = 'Mississippi';
            $img = 'mississippi';
            break;
       case 'mo':
            $state = 'Missouri';
            $img = 'missouri';
            break;
       case 'mt':
            $state = 'Montana';
            $img = 'montana';
            break;
       case 'ne':
            $state = 'Nebraska';
            $img = 'nebraska';
            break;
       case 'nv':
            $state = 'Nevada';
            $img = 'nevada';
            break;
       case 'nh':
            $state = 'New Hampshire';
            $img = 'newhampshire';
            break;
       case 'nj':
            $state = 'New Jersey';
            $img = 'newjersey';
            break;
       case 'nm':
            $state = 'New Mexico';
            $img = 'newmexico';
            break;
       case 'ny':
            $state = 'New York';
            $img = 'newyork';
            break;
       case 'nc':
            $state = 'North Carolina';
            $img = 'northcarolina';
            break;
       case 'nd':
            $state = 'North Dakota';
            $img = 'northdakota';
            break;
       case 'oh':
            $state = 'Ohio';
            $img = 'ohio';
            break;
       case 'ok':
            $state = 'Oklahoma';
            $img = 'oklahoma';
            break;
       case 'or':
            $state = 'Oregon';
            $img = 'oregon';
            break;
       case 'pa':
            $state = 'Pennsylvania';
            $img = 'pennsylvania';
            break;
       case 'ri':
            $state = 'Rhode Island';
            $img = 'rhodeisland';
            break;
       case 'sc':
            $state = 'South Carolina';
            $img = 'southcarolina';
            break;
       case 'sd':
            $state = 'South Dakota';
            $img = 'southdakota';
            break;
       case 'tn':
            $state = 'Tennessee';
            $img = 'tennessee';
            break;
       case 'tx':
            $state = 'Texas';
            $img = 'texas';
            break;
       case 'ut':
            $state = 'Utah';
            $img = 'utah';
            break;     
       case 'vt':
            $state = 'Vermont';
            $img = 'vermont';
            break;
       case 'va':
            $state = 'Virginia';
            $img = 'virginia';
            break;
       case 'wa':
            $state = 'Washington';
            $img = 'washington';
            break;     
       case 'wv':
            $state = 'West Virginia';
            $img = 'westvirginia';
            break;     
       case 'wi':
            $state = 'Wisconsin';
            $img = 'wisconsin';
            break;      
       case 'wy':
            $state = 'Wyoming';
            $img = 'wyoming';
            break;
       case 'dc':
            $state = 'District of Columbia';
            $img = 'dc';
            break;
   }
   ?>
  <div class="stateOuter">
      <div class="statePic left">
          <img src="<?php echo imagesPath(); ?>/Embed_States/<?php echo $img?>.png" />
      </div>
          
      <div class="statetitle left">
          <h1> <?php echo $state; ?></h1>
      </div>
      <br class="clear" />
      <table id="scoreTbl" class="rounded state" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="repScore"><?php echo $ravg; ?>%</td>
            <td class="demScore"><?php echo $davg; ?>%</td>
          </tr>
          <tr>
            <td class="lScoreLbl">REPUBLICAN SCORE</td>
            <td class="rScoreLbl">DEMOCRAT SCORE</td>
          </tr>
      </table>
     
     <div class="stateListing">
         <table class="stateTable" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <th class="one">NAME</th>
                <th>DIST</th>
                <th>SCORE</th>
             </tr>
             <?php foreach($members as $member){ ?>
                 <tr>
                     <?php $title = ($member->chamber == 'house') ? 'Rep.' : 'Sen.'; ?>
                    <td class="one"><?php echo $title ?> <?php echo $member->fName; ?> <?php echo $member->lName; ?></td>
                    <td><?php echo $member->district; ?></td>
                    <td><?php echo $member->score; ?>%</td>
                 </tr>
             <?php } ?>
         </table>
     </div>
     
  </div>
   <div class="footer">
        <a href="http://afpscorecard.org/#state#<?php echo $abrv ?>" target="_blank"><img src="<?php echo imagesPath(); ?>/memBtn.jpg"  /><a>
      </div>
</body>
</html>