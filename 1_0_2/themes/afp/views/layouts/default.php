<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, target-densitydpi=device-dpi">
  <meta property="og:image" content="http://afpscorecard.org/site/themes/afp/images/logo-afp.png" />

  <title>AFP: SCORECARD</title>

  <link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic,700|Lato|Raleway:400,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo cssPath(); ?>/style.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo cssPath(); ?>/responsive.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo cssPath(); ?>/selectyze.jquery.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo cssPath(); ?>/add.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo cssPath(); ?>/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo cssPath(); ?>/validationEngine.jquery.css" rel="stylesheet" type="text/css" />

  <?php if ($this->agent->is_browser()){ ?>
    <link href="<?php echo cssPath(); ?>/desktop.css" rel="stylesheet" type="text/css" />    
  <?php } ?>

  <script type="text/javascript">
    var AFP_url = '<?php echo base_url(); ?>';
  </script>

  <script type="text/javascript" charset="utf-8"> var imagePath = '<?php echo VERSION ?>/themes/afp/images' </script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/clearInput.jquery.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/selectyze.jquery.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/blockui.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/jquery.autocomplete.min.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/app.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/init.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/routes.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/jquery.validationEngine.js"></script>
  <script src="<?php echo jsPath(); ?>/languages/jquery.validationEngine-en.js" type="text/javascript" ></script>

  <!--[if < IE 10]>
  <script type="text/javascript" src="<?php echo jsPath(); ?>/PIE.js"></script>
  <![endif]-->

</head>

<body>

<?php if(ENVIRONMENT == 'development'){ ?>
  <div class="devAlert">
      Development Version <?php echo VERSION ?>  <a href="#" class="openLog">Change Log</a>
      <div class="changelog">
          <?php $this->load->view('templates/changelog') ?>
      </div>
  </div>
<?php } ?>

<div id="stickyOuter">
  <div id="stickyInner">
      <?php echo $template['partials']['header']; ?>
      <div id="rowOuter" class="contentRow">
          <?php echo $template['partials']['index']; ?>
      </div><!-- end contentRow -->
  </div><!-- end stickyInner -->
</div><!-- end stickyOuter -->

<div id="fade"></div>

<?php echo $template['partials']['footer']; ?>

<script type="text/javascript" src="<?php echo jsPath(); ?>/globalFunctions.js"></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-7304859-34', 'auto');
ga('send', 'pageview');</script>

</body>
</html>
