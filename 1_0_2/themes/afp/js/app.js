// JavaScript Document
var debug = true;
var log = function(msg) { if(debug) {try { if (console != null && console.log != null && debug != false) { console.log(msg); } } catch (e) { } } }

if (!this.app) { 
	this.app = new function() {
		
		var defaults = {};
		
		var sbfCache = {};
		var that = this;
		var titleHolder = "";
		var lasturl= "";	//here we store the current URL hash
		var hash = window.location.hash;
		
		String.prototype.replaceAll = function(s1, s2) { return this.split(s1).join(s2)}
		
		this.init = function (options) {
			defaults = {
				defaultState: 'map',
				session: '115',
				discriptionLength: '3',
				members_url: AFP_url + 'api/index.php/scorecard/members/format/jsonp/congress/',
				votes_url: AFP_url + 'api/index.php/scorecard/keyvotes/format/jsonp/congress/',
				pages_url: AFP_url + 'index.php/welcome/',
				template_path: '1_0_2/views/templates/',
				apiVersion: 'api_1_1',
				apiCache: 'true'
			}
			var options = $.extend(defaults, options);
			that.checkSession();
			log('app Init');
		}
		
		/// Session Chaneing functions
		this.changeSession = function(session){ this.createCookie('session', session, '1'); defaults.session = session;}
		this.checkSession = function(){var session = this.readCookie('session');if(session){defaults.session = session;}}
		this.getSession = function(){var session = this.readCookie('session');if(session){return session;}else{return defaults.session;}}
		
		
		
		///// Tempalte Helpers
		this.getUrlContent = function(url) {
			var xhr = getXHR();
			xhr.open("GET", url, false);
			xhr.send(null);
			return xhr.responseText;
		}
		
		var getXHR = function() {
			try { return new XMLHttpRequest(); } catch (e) { }
			try { return new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { }
			try { return new ActiveXObject("Msxml3.XMLHTTP"); } catch (e) { }
			try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { }
			return null;
		}
		
		//add data to temple and return the HTML
		this.getPage = function(html, data) {
			//this.clearCachedData(html);
			if(!this.getTempCachedData(html)){
				pageHtml = that.getUrlContent(defaults.template_path+html + '.html');
				this.saveTempCachedData(html, pageHtml);
			}
			pageHtml = this.getTempCachedData(html);
			return tmpl(pageHtml, data);
		}
		
		//Write template to page
		this.loadTemplate = function(hash, contianer) {
			//Check if hash Contains HTML
			
				var html = hash;
				var page = that.getTempCachedData("loadPageHolder");
			
			//log(html);
			jQuery(contianer).html(html);
			//log(jQuery(contianer).html());
			//alert(html);
			
		}
		
		/// Cache Functions (using amplify.js amplify.store) uses Local Cache
		this.getCachedData = function(vert) { return amplify.store(vert)}
		this.clearCachedData = function(vert) { amplify.store( vert, null );}
		this.saveCachedData = function(vert, data) {amplify.store( vert, data, {expires: 1000000})}
		
		this.getTempCachedData = function(vert) { return sbfCache[vert];}
		this.clearTempCachedData = function(vert) { if(vert) { sbfCache[vert] = ""; }else{ sbfCache = {}; }}
		this.saveTempCachedData = function(vert, data) { sbfCache[vert] = data;}
		
		//Cookie Functions
		this.createCookie = function(name, value, days) { if (days) { var date = new Date(); date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000)); var expires = "; expires=" + date.toGMTString(); } else { var expires = ""; } document.cookie = name + "=" + value + expires + "; path=/"; }
		this.readCookie = function(name) { var nameEQ = name + "="; var ca = document.cookie.split(';'); for (var i = 0; i < ca.length; i++) { var c = ca[i]; while (c.charAt(0) == ' ') { c = c.substring(1, c.length); } if (c.indexOf(nameEQ) == 0) { return c.substring(nameEQ.length, c.length); } } return null; }
		this.eraseCookie = function(name) { this.createCookie(name, "", -1); }

		
		
		
		
		
		
		///////////////// paging functions //////////////
		this.checkURL = function(){
			hash = window.location.hash;
			hash = hash.replaceAll("%23","#");
			newhash = hash;
			if(newhash != lasturl)	// if the hash value has changed
			{
				app.blockUI();
				log(newhash+' - '+lasturl);
				lasturl = newhash;
				log("lasturl = "+lasturl); 
				that.loadPage(newhash);	// and load the new page
				// if(lasturl == ''){
				// 	app.unBlockUI();	
				// }
				
			}
		}
		
		if(that.getSession() == 'NONE'){
			that.changeSession('113');
		}
		
		that.loadPage = function(pagehash) {
			if(hash == '') {that.routeTable('home');}
			if(hash.split('#')[1] == "") { that.routeTable('home');  }
			if(hash.split('#')[1] == "home") { that.routeTable('home');  }
			if(hash.split('#')[1] == "about") { that.routeTable('about');  }
			if(hash.split('#')[1] == "allResults") { that.routeTable('allResults');  }
			if(hash.split('#')[1] == "zip") { that.routeTable('zip');  }
			if(hash.split('#')[1] == "vote") { that.routeTable('vote');  }
			if(hash.split('#')[1] == "member") { that.routeTable('member');  }
			if(hash.split('#')[1] == "issue") { that.routeTable('issue');  }
			if(hash.split('#')[1] == "last") { that.routeTable('last');  }
			if(hash.split('#')[1] == "state") { that.routeTable('state');  }
			if(hash.split('#')[1] == "singleAlert") { that.routeTable('singleAlert');  }
			if(hash.split('#')[1] == "voteAlerts") { that.routeTable('voteAlerts');  }
			if(hash.split('#')[1] == "alertSearch") { that.routeTable('alertSearch');  }
			if(hash.split('#')[1] == "alertissue") { that.routeTable('alertissue');  }
			if(hash.split('#')[1] == "alertChamber") { that.routeTable('alertChamber');  }
			if(hash.split('#')[1] == "sortVote") { that.routeTable('sortVote');  }
			if(hash.split('#')[1] == "sortMem") { that.routeTable('sortMem');  }
			if(hash.split('#')[1] == "sortIssue") { that.routeTable('sortIssue');  }
			if(hash.split('#')[1] == "sortOverall") { that.routeTable('overallSort');  }
			if(hash.split('#')[1] == "allResultsMembers") { that.routeTable('allResultsMembers');  }
			if(hash.split('#')[1] == "sortOverallMembers") { that.routeTable('sortOverallMembers');  }
			
		}
		
		this.routeTable = function(route) {
			log("routing "+route);
			switch(route){
				case 'home':
					this.makeHTMLCall("homePage", defaults.pages_url, function(data){routes.route('home', data)});
					break;
				case 'about':
					this.makeHTMLCall("aboutPage", defaults.pages_url, function(data){routes.route('home', data)});
					break;
				case 'overallSort':
					var sort = hash.split('#')[2]
					this.makeCall(that.getSession()+'/sort/'+sort, defaults.votes_url, function(data){routes.route('allResults', data)});
					break;
				case 'allResults':
					this.makeCall(that.getSession(), defaults.votes_url, function(data){routes.route('allResults', data)});
					break;
				case 'zip':
					var zip = hash.split('#')[2]
					this.makeCall(that.getSession()+'/zip/'+zip, defaults.members_url, function(data){data['title'] = zip; routes.route('zip', data)});
					break;
				case 'allResultsMembers':
					this.makeCall(that.getSession()+'/orderBy/score', defaults.members_url, function(data){routes.route('allResultsMembers', data)});
					break;
				case 'sortOverallMembers':
					var sort = hash.split('#')[2]
					this.makeCall(that.getSession()+'/orderBy/'+sort, defaults.members_url, function(data){routes.route('allResultsMembers', data)});
					break;
				case 'vote':
					var id = hash.split('#')[2]
					this.makeCall(that.getSession()+'/id/'+id, defaults.votes_url, function(data){routes.route('vote', data)});
					break;
				case 'sortVote':
					var id = hash.split('#')[2]
					var sort = hash.split('#')[3]
					this.makeCall(that.getSession()+'/id/'+id+'/sort/'+sort, defaults.votes_url, function(data){routes.route('vote', data)});
					break;
				case 'sortIssue':
					var id = hash.split('#')[2]
					var sort = hash.split('#')[3]
					this.makeCall(that.getSession()+'/issue/'+id+'/sort/'+sort, defaults.votes_url, function(data){routes.route('issue', data)});
					break;
				case 'member':
					var id = hash.split('#')[2]
					var chamber = hash.split('#')[3]
					this.makeCall(that.getSession()+'/id/'+id+'/votes/yes/chamber/'+chamber, defaults.members_url, function(data){routes.route('member', data)});
					break;
				case 'sortMem':
					var id = hash.split('#')[2]
					var issue = hash.split('#')[3] 
					this.makeCall(that.getSession()+'/id/'+id+'/votes/yes/sortvotes/'+issue, defaults.members_url, function(data){routes.route('member', data)});
					break;
				case 'issue':
					var id = hash.split('#')[2]
					this.makeCall(that.getSession()+'/issue/'+id, defaults.votes_url, function(data){routes.route('issue', data)});
					break;
				case 'last':
					var id = hash.split('#')[2]
					this.makeCall(that.getSession()+'/last/'+id, defaults.members_url, function(data){data['title'] = id; routes.route('name', data)});
					break;
				case 'state':
					var id = hash.split('#')[2]
					this.makeCall(that.getSession()+'/state/'+id+'/orderBy/chamber/order/DESC', defaults.members_url, function(data){data['title'] = id; routes.route('state', data)});
					break;
				case 'singleAlert':
					var id = hash.split('#')[2]
					this.makeHTMLCall("single?id="+id, defaults.pages_url, function(data){routes.route('singleAlert', data)});
					break;
				case 'voteAlerts':
					var page = (hash.split('#')[2]) ? hash.split('#')[2] : 1;
					this.makeHTMLCall("alerts/p/"+page, defaults.pages_url, function(data){routes.route('voteAlerts', data)});
					break;
				case 'alertSearch':
					var search = hash.split('#')[2];
					data = ({search : search});
					this.makeHTMLCall("alertSearch", defaults.pages_url, function(data){routes.route('voteAlerts', data)}, data);
					break;
				case 'alertissue':
					var issue = hash.split('#')[2];
					var page = (hash.split('#')[3]) ? hash.split('#')[3] : 1;
					this.makeHTMLCall("alertissue/issue/"+issue+'/p/'+page, defaults.pages_url, function(data){routes.route('voteAlerts', data)});
					break;
				case 'alertChamber':
					var chamber = hash.split('#')[2];
					this.makeHTMLCall("alertChamber/chamber/"+chamber, defaults.pages_url, function(data){routes.route('voteAlerts', data)});
					break;	
			}
		}
		
		
		///////////////// UI Blocking functions //////////////
		
		this.blockUI = function() {
			$('html, body').animate({
				scrollTop: $(".contentRow").offset().top
			}, 1000);
			
			
			$('.contentRow').block({ 
            message: 'Loading..',
            css: { 
				marginLeft: '0px',
                width: '371px',
				border: 'none', 
            	padding: '10px',
				backgroundColor: '#fff',
				top:            '10%', 
        left:           '35%', 
                  },
                  centerY: false 
       		}); 
		}
// 		
		this.unBlockUI = function() {
			$('.contentRow').unblock();
		}
		
		
		
		
		/////////////////// APi Calls //////////////////
				
		this.makeCall = function(method, api, callback) {
			var obj ;
			var url = api + method + '/cache/'+defaults.apiCache+ '?v='+defaults.apiVersion;
			if(!this.getTempCachedData(url)){
				log("making Call: " + url);				
				$.ajax({url: url, type: "GET", dataType: 'jsonp', async: true, success: function(data) { log("JSON call Success"); that.saveTempCachedData(url, data); callback(data);  } });
			}else{
				log("Retriving Cached  Call: " + url);
				data = this.getTempCachedData(url);
				callback(data);
			}
		}
		
		this.makeHTMLCall = function(method,  url, callback, data) {
			var obj ;
			var url = url + method;
			log("making Call: " + url);
			$.ajax({url: url, type: "POST", async: true, data:data, success: function(data) { log("HTML call Success"); callback(data);  } });
		}
		
		
	
		
	}//end App
}







// John Resig - http://ejohn.org/ - MIT Licensed
        

// Simple JavaScript Templating
// John Resig - http://ejohn.org/ - MIT Licensed
(function () {
	var cache = {};

	this.tmpl = function tmpl(str, data) {
		// Figure out if we're getting a template, or if we need to
		// load the template - and be sure to cache the result.
		var fn = !/\W/.test(str) ?      cache[str] = cache[str] ||   tmpl(document.getElementById(str).innerHTML) :

		// Generate a reusable function that will serve as a template
		// generator (and which will be cached).
		new Function("obj","var p=[],print=function(){p.push.apply(p,arguments);};" +

		// Introduce the data as local variables using with(){}
		"with(obj){p.push('" +

		// Convert the template into pure JavaScript
		str
			  .replace(/[\r\t\n]/g, " ")
			  .split("<%").join("\t")
			  .replace(/((^|%>)[^\t]*)'/g, "$1\r")
			  .replace(/\t=(.*?)%>/g, "',$1,'")
			  .split("\t").join("');")
			  .split("%>").join("p.push('")
			  .split("\r").join("\\'")
		  + "');}return p.join('');");

		// Provide some basic currying to the user
		return data ? fn(data) : fn;
	};
})();