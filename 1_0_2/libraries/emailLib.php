<?php defined('BASEPATH') OR exit('No direct script access allowed');
class emailLib {


    


    function emailLib(){
        $this->CI =& get_instance();
        $this->CI->load->library('email');
    }
    
    
    function sendMail($to = '', $cc = '', $subject = '', $message = '', $from = "") {
        $config['mailtype'] = 'html';

        
        ini_set("SMTP","mail.capitolfaces.com");
        
        
        $this->CI->email->initialize($config);
        
       
        $this->CI->email->from($from);
        $this->CI->email->to($to);
        $this->CI->email->cc($cc);
        //$this->email->bcc('them@their-example.com');
        
        $this->CI->email->subject($subject);
        $data['message'] = $message;
        $this->CI->email->message($this->CI->load->view('emails/email', $data, true));
        
        $this->CI->email->send();
        
        //$this->CI->email->print_debugger();
    }
    
    
}