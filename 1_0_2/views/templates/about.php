<div id="rowInner">
    <div id="conBox" class="hasSidebar">
        <div id="mainBox" class="rounded column">
            <?php foreach($data as $page){ ?>
            <h1><?php echo stripslashes($page->title) ?></h1>
                <?php echo stripslashes($page->content); ?>            
            
            <?php } ?>
            
        </div>
        <div id="sidebar" class="rounded column">
            <?php $this->load->view('templates/joinForm') ?>
            
        </div>
        <br class="clear" />
    </div> <!--end conBox -->
    
</div>

