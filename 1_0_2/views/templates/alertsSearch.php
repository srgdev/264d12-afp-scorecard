<script>
    // ClearInput   
    $('.clearinput').clearInput();
</script>

<h1>Search Key Vote Alerts</h1>
<div class="alertSearchBox">
    <h2>Search by Keyword</h2>
    <form name="alertSearch" class="alertSearch">
        <input name="search" id="search" class="searchField rounded clearinput" type="text" value="SEARCH" />
        <input name="submit" type="submit" value="GO" id="submit" class="searchSubmit rounded btnGold" />
    </form>
</div>
<div class="alertSearchBox">
    <h2>Search by Chamber</h2>
    <a href="#alertChamber#house" class="button btnDkGrey chamberBtn">House</a>
    <a href="#alertChamber#senate" class="button btnDkGrey chamberBtn">Senate</a>
</div>
<br class="clear" />
<h2>Search by Issue</h2>
<div class="alertSearchBox">
    <a href="#alertissue#budget" class="issueItem"><i class="budgetIcn"></i>Budget &amp; Spending</a>
    <a href="#alertissue#energy" class="issueItem"><i class="energyIcn"></i>Energy &amp; Enviroment</a>
    <a href="#alertissue#health" class="issueItem"><i class="healthIcn"></i>Health Care &amp; Entitlements</a>
    <a href="#alertissue#taxes" class="issueItem"><i class="taxesIcn"></i>Taxes</a>
</div>
<div class="alertSearchBox">  
    <a href="#alertissue#labor" class="issueItem"><i class="laborIcn"></i>Labor, Education &amp; Pensions</a>
    <a href="#alertissue#limited" class="issueItem"><i class="limitedIcn"></i>Limited Government</a>
    <!--<a href="#alertissue#first" class="issueItem"><i class="firstIcn"></i>First Amendment</a>-->
</div>
<br class="clear" />
<br /><br />