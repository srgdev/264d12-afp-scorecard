<h5>Version 1_0_0:</h5>
Initial Launch

<h5>Version 1_0_1:</h5>

Added change Log <br />
Added all results for Members <br />
Removed scroll bar from sort dropdown <br />
Added score sort for overall members <br />
Added lifetime sort for overall members <br />  
Updated responsiveness on the overall results page <br />
Updated members sort order <br />
fixed duplicate members bug <br />
Chnaged Member sort to score default