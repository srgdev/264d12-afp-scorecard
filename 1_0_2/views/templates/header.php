    <div class="bg_image_cont"">
        <div id="rowOuter" class="topRow">
            <div id="rowInner">
                <div id="logoAFP">
                    <a href="http://americansforprosperity.org"><img src="<?php echo imagesPath(); ?>/logo-afp.png" width="258" height="71" />
                    </a>
                </div>
                <div class="right">
                    <a id="midBtn" class="menu-item" href="http://action.americansforprosperity.org/?">Take Action</a>
                    <a id="midBtn" class="menu-item" href="http://americansforprosperity.org/donatescorecard">Donate</a>  
                    <!--<a id="rtBtn" class="button btnGreen" href="#">Select Session <img class="dropArrow" src="<?php echo imagesPath(); ?>/icon-darrow.png" width="17" height="16" /></a>-->
                    <a class="menu-item" href="#voteAlerts">Key Vote Alerts</a></li>

                     <li class="hasDropdown menu-item">
                        <select name="session" id="session" class="sessionList">
                            <option value="NONE" selected="selected">Select Session</option>
                            <?php 
                                $sessions = $this->congress->getSessions();
                                foreach($sessions as $s){ ?>
                                    <option value="<?php echo $s->congress ?>" ><?php echo $s->congress ?>th (<?php echo $s->years; ?>)</option>
                                <?php } ?>
                        </select>
                    </li>
                </div>
                <br class="clear" />
            </div>
        </div> <!-- end topRow -->
        
        <div id="rowOuter" class="bannerRow">
            <div id="rowInner">
                <div id="logoSCard"><a href="http://afpscorecard.org"><img src="<?php echo imagesPath(); ?>/logo-scorecard.png" width="332" height="114" /></a></div>
                <div id="socnet">
                    <span>Share The Scorecard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="http://twitter.com/share?text= I'm keeping my legislators accountable using the @AFPhq scorecard. Join me: &hashtags=tcot, fightback&url=http://www.afpscorecard.org"><img src="<?php echo imagesPath(); ?>/socnet-twt.png" width="35" height="23" /></a>
                    <a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=Americans For Prosperity - ScoreCard&amp;p[summary]=Are your federal legislators voting for economic freedom? You deserve to know. Check out Americans for Prosperity’s Federal Scorecard and see where they rank&amp;p[url]=http://afpscorecard.org&p[images][0]=http://afpscorecard.org/site/themes/afp/images/logo-scorecard.jpg"><img src="<?php echo imagesPath(); ?>/socnet-fb.png" width="25" height="23" /></a>
                    <a href="mailto:?subject=AFP Scorecard&body=I just learned how my lawmakers are voting on issues by using the Americans For Prosperity Scorecard.  Take a look! AFPscorecard.org The scorecard gives a really straightforward way to see if our legislators are standing with us taxpayers on free market principles. This information is very important as it allows us to hold lawmakers accountable when they vote against economic freedom.%0D%0A%0D%0AAmericans for Prosperity is a large grassroots based organization that exists to advocate and agitate for free market principles and liberty. This scorecard is but one example of what Americans for Prosperity does to protect and enhance your economic freedom. Please consider a making a donation to help fight for a freer America.%0D%0A%0D%0AAmericans for Prosperity has tirelessly worked to promote policies that make America freer and stronger. That’s why I support them and I hope you will too."><img src="<?php echo imagesPath(); ?>/socnet-email.png" width="33" height="23" /></a>
                </div>
                <br class="clear" />
            </div>
        </div><!-- end bannerRow -->
        <br class="clear" />
    </div>
    
    <div id="rowOuter" class="searchRow active">
        <div id="rowInner">
            <div class="wrap">
                <div id="searchNavBox">
                    <div id="searchNav">
                        <p>Americans for Prosperity ranks members of Congress based on their votes for economic freedom.</p>
                        <h1>Start Your Search</h1>
                        <div title="byName" class="button btnSearch btnGreen">Search by Name</div>
                        <div title="byZip" class="button btnSearch btnGreen">Search by Zip Code</div>
                        <div title="byState" class="button btnSearch btnGreen active" style="color: rgb(255, 255, 255);">Search by State</div>
                        <div title="byIssue" class="button btnSearch btnGreen">Search by Issue</div>
                        <div title="oResults" class="button btnSearch btnGreen route" data-role="allResults">Overall Results</div>
                        
                      <div title="byName" class="button fieldSearch btnGreen noHover">
                            <form class="namesearch">
                                <input name="name" id="name" class="searchNavField rounded clearinput bigname name" type="text" value="SEARCH BY LAST NAME" />
                                <input name="submit" type="submit" class="rounded btnGreen" value="Submit" id="submit" />
                            </form>
                        </div>
                        <div title="byZip" class="button fieldSearch btnGreen noHover">
                            <form class="zipsearch">
                                <input name="zip" id="zip" class="searchNavField rounded clearinput zip" type="text" value="SEARCH BY ZIP CODE" />
                                <input name="submit" type="submit" class="rounded btnGreen" value="Submit" id="submit" />
                            </form>
                        </div>
                        <div title="byState" class="button fieldSearch btnGreen noHover">
                            <form class="smallState">
                               <div class="searchNavField rounded">
                                <select name="state" id="state">
                                    <option value="None" selected="selected">SEARCH BY STATE</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DE">Delaware</option>
                                    <option value="DC">District of Columbia</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                                </div>
                              <input name="submit" type="submit" class="rounded btnGreen" value="Submit" id="submit" />
                            </form>
                        </div>
                        <div title="byIssue" class="button fieldSearch btnGreen noHover">
                            <form class="smallIssue">
                                <div class="searchNavField rounded">
                                <select name="issue" id="issue">
                                    <option value="None" selected="selected">SEARCH BY ISSUE</option>
                                    <option value="budget">BUDGET &amp; SPENDING</option>
                                    <option value="energy">ENERGY &amp; ENVIROMENT</option>
                                    <option value="healthcare">HEALTHCARE &amp; ENTITLEMENTS</option>
                                    <option value="taxes">TAXES</option>
                                    <option value="labor">LABOR, EDUCATION &amp; PENSIONS</option>
                                    <option value="limited">LIMITED GOVERNMENT</option>
                                    <!--<option value="first">FIRST AMENDMENT</option>-->
                                </select>
                                </div>
                                <input name="submit" type="submit" class="rounded btnGreen" value="Submit" id="submit" />
                            </form>
                        </div>
                        <a href="#" class="route" data-role="allResults"> <div title="oResults" class="button fieldSearch btnGreen">Overall Results</div></a>
                        
                  </div>
                    <!-- <div class="searchToggle"></div> -->
                </div><!-- end SearchNavBox -->
            
                <div id="searchConBox">
                    <!-- BY NAME SEARCHBOX CONTENT -->
                    <div id="byName" class="searchDD">
                        <img src="<?php echo imagesPath(); ?>/hdr-byName.png" width="318" height="111" />
                        <form class="namesearch">
                            <div class="searchFieldOuter rounded"><input name="name" id="bigname" class="searchField rounded clearinput name bigname" type="text" value="LAST NAME" /></div>
                            <br />
                            <input name="submit" type="submit" class="button btnGreen" id="submit" value="SUBMIT" />
                        </form>
                    </div>
                    <!-- BY ZIPCODE SEARCHBOX CONTENT -->
                    <div id="byZip" class="searchDD">
                        <img src="<?php echo imagesPath(); ?>/hdr-byZip.png" width="254" height="111" />
                        <form class="zipsearch">
                            <div class="searchFieldOuter rounded"><input name="zip" id="zip" class="searchField rounded clearinput zip" type="text" value="SEARCH" /></div>
                            <br />
                            <input name="submit" type="submit" class="button btnGreen" id="submit" value="SUBMIT" />
                        </form>
                    </div>
                    <!-- BY STATE SEARCHBOX CONTENT -->
                    <div id="byState" class="searchDD"><img src="<?php echo imagesPath(); ?>/map_2.png" usemap="#Map" width="549" height="400" /></div>
                    <!-- BY ISSUE SEARCHBOX CONTENT -->
                    <div id="byIssue" class="searchDD">
                        <a href="#" class="route" data-role="issue#budget"><img src="<?php echo imagesPath(); ?>/sIcon-budget.png" width="98" height="105" /></a>
                        <a href="#" class="route" data-role="issue#energy"><img src="<?php echo imagesPath(); ?>/sIcon-energy.png" width="98" height="105" /></a>
                        <a href="#" class="route" data-role="issue#health"><img src="<?php echo imagesPath(); ?>/sIcon-health.png" width="98" height="105" /></a>
                        <a href="#" class="route" data-role="issue#taxes"><img src="<?php echo imagesPath(); ?>/sIcon-taxes.png" width="98" height="105" /></a>
                        <a href="#" class="route" data-role="issue#labor"><img src="<?php echo imagesPath(); ?>/sIcon-labor.png" width="98" height="105" /></a>
                        <a href="#" class="route" data-role="issue#limited"><img src="<?php echo imagesPath(); ?>/sIcon-limited.png" width="98" height="105" /></a>
                        <!--<a href="#" class="route" data-role="issue#first"><img src="<?php echo imagesPath(); ?>/sIcon-first.png" width="98" height="105" /></a>-->
                        <br class="clear" />
                        <p>Click an Issue to go to the Issue Page</p>
                    </div>
                    <!-- OVERALL RESULTS SEARCHBOX CONTENT -->
                    <div id="oResults" class="searchDD" >
                        
                    </div>
                </div><!-- end SearchBox -->
            
            <br class="clear" />
            </div>
        </div>
    </div><!-- end searchRow -->