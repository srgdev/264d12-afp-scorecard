<?php


/*
 * @author : Paul Radich
 * 
 * Added to system core for the SRG CMS
 * 
 * 
 */
 
 
class Auth {
	
	var $CI;
	var $badUrl;
	var $goodUrl;
	
	function Auth(){
		$this->CI =& get_instance();
		$this->badUrl = $this->CI->config->item("bad_url");
		$this->goodUrl = $this->CI->config->item("good_url");
		
	}
	
	function checkStatus(){
		$check = $this->CI->session->userdata('logged_in');
		
		if($check){
		}else{
			setMessage('error', 'You must be logged in to view this page');
			redirect($this->badUrl, 'location');
		}
	}
	
	function logIn($uname,$pass,$remeber = ''){
		$try = $this->tryLogIn($uname, $pass, $remeber);
		
		if($try != "bad"){ $this->setUserData($try); redirect($this->goodUrl, 'location'); }else{ return "bad";}
	}
	
	function tryLogIn($uname,$pass,$remeber = ''){
		$pass5 = do_hash($pass, 'md5');
		$query = $this->CI->db->get_where('cms_users', array('username' => $uname, 'password' => $pass5));
		if($query->num_rows() >= 1){
			$row = $query->row(); 
			$newdata = array(
                   'username'  => $row->username,
                   'logged_in' => TRUE,
                   'level' => $row->acl,
                   'group' => $row->acg
               );
			// $this->session->set_userdata($newdata);
			
			if($remeber){
				set_cms_cookie('cmsUname',$uname);
				set_cms_cookie('cmsPass',$pass);
			}else{
				delete_cms_cookie('cmsUname');
				delete_cms_cookie('cmsPass');
				
			}
			
			
			return $newdata;
		}else{
			setMessage('error', 'Invaild Username or Password'); 
			redirect($this->badUrl, 'location');
		}
	}
	
	function setUserData($try){
		$this->CI->session->set_userdata($try);
	}
	
	function getUserName(){
		
			return $this->CI->session->userdata('username');
		
	}
	
	// function getAccessLevel(){
// 		
			// return $this->CI->session->userdata('level');
// 		
	// }
	
	function logOut(){
		$this->CI->session->unset_userdata('logged_in');
		return "done";
	}
	
	function addUser($uname, $pass, $level){
		$this->CI->db->query("INSERT INTO cms_users SET username = '$uname', password = '$pass', level = '$level'");
		return "ok";
	}
	
	function checkLevel($param) {
		$level = $this->CI->session->userdata('level');
		if($level <= $param || $level == "0"){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function getLevel(){
		return $this->CI->session->userdata('level');
	}
	
	function checkACG($group){
		$groups = $this->CI->session->userdata('group');
		if($groups == "all" || $this->CI->configs->get('useGroups') == 'false'){
			return TRUE;
		}else{
			$acg = explode(",",$groups);
			//$acg = array_map('strtolower', $acg);
			return in_array($group,$acg);
		}
		
	}
	
	function getACG(){
		return $this->CI->session->userdata('group');
	}
	
	
	function changePass($newPass) {
		$user = $this->getUserName();
		$pass5 = do_hash($newPass, 'md5');
		$this->CI->crud->use_table('cms_users');
		if($this->CI->crud->update(array('username' => $user),array('password' => $pass5))){
			if(get_cms_cookie('cmsUname') != '') {
				set_cms_cookie('cmsPass',$newPass);
			}
			return TRUE;
		}else{
			return FALSE;
		}
		
	}
	
}