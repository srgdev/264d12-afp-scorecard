<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


/*
 * @author : Paul Radich
 * 
 * Added to system core for the SRG CMS
 * 
 * 
 */
 
function setMessage($type = '', $message = ''){
	$CI =& get_instance();
     $CI->load->library('session');
	 
	  $CI->session->set_flashdata('type', $type);
	  $CI->session->set_flashdata('message', $message); 
}

function getMessage() {
	$CI =& get_instance();
    $CI->load->library('session');
	
	
	if($CI->session->flashdata('type')){
	$type =  $CI->session->flashdata('type');
	$message = $CI->session->flashdata('message');
		if($type == "info"){
			return '<div class="alert alert-info"><i></i>'.$message.'</div>';
		}
		
		if($type == "error"){
			return '<div class="alert alert-error"><i></i>'.$message.'</div>';
		}
		
		if($type == "success"){
			return '<div class="alert alert-success"><i></i>'.$message.'</div>';
		}
		
		if($type == "warning"){
			return '<div class="alert alert-warning"><i></i>'.$message.'</div>';
		}
		
	}else{
		return "";
	}
}
