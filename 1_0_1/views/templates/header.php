<div id="rowOuter" class="topRow">
        <div id="rowInner">
            <a id="ltBtn" class="button btnGreen" href="http://www.americansforprosperity.org ">Return to Main Site</a>
            <div id="rtBtn" class="button btnGreen noHover">
                <select name="session" id="session" class="sessionList">
                    <option value="NONE" selected="selected">Select Session</option>
                    <?php 
                        $sessions = $this->congress->getSessions();
                        foreach($sessions as $s){ ?>
                            <option value="<?php echo $s->congress ?>" ><?php echo $s->congress ?>th (<?php echo $s->years; ?>)</option>
                        <?php } ?>
                </select>
            </div>
            
            <!--<a id="rtBtn" class="button btnGreen" href="#">Select Session <img class="dropArrow" src="<?php echo imagesPath(); ?>/icon-darrow.png" width="17" height="16" /></a>-->
            
            <a id="midBtn" class="button gold" href="http://action.americansforprosperity.org/?">Take Action</a>
            <a id="midBtn" class="button red" href="http://americansforprosperity.org/donatescorecard">Donate</a>
            
            <br class="clear" />
        </div>
    </div> <!-- end topRow -->
    
    <div id="rowOuter" class="navRow">
        <div id="rowInner">
            <div id="logoAFP"><a href="http://afpscorecard.org"><img src="<?php echo imagesPath(); ?>/logo-afp.png" width="258" height="71" /></a></div>
            <ul id="nav" class="rounded">
                <li><a href="#home">Home</a></li>
                <li><a href="#about">About the Scorecard</a></li>
                <li><a href="#voteAlerts">Key Vote Alerts</a></li>                
                <li><a href="#" class="searchToggle">Search <span class="ddArrow active"></span></a></li>
            </ul>
            <br class="clear" />
        </div>
    </div> <!-- end navRow -->
    
    <div id="rowOuter" class="bannerRow shadow_up">
        <div id="rowInner">
            <div id="logoSCard"><a href="http://afpscorecard.org"><img src="<?php echo imagesPath(); ?>/logo-scorecard.jpg" width="332" height="114" /></a></div>
            <div id="socnet">
                <span>Share The Scorecard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <a href="http://twitter.com/share?text=Check out your lawmakers’ votes at the @AFPhq scorecard &hashtags=tcot, fightback&url=http://www.afpscorecard.org"><img src="<?php echo imagesPath(); ?>/socnet-twt.png" width="35" height="23" /></a>
                <a href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=Americans For Prosperity - ScoreCard&amp;p[summary]=Are your federal legislators voting for economic freedom? You deserve to know. Check out Americans for Prosperity’s Federal Scorecard and see where they rank&amp;p[url]=http://afpscorecard.org&p[images][0]=http://afpscorecard.org/site/themes/afp/images/logo-scorecard.jpg"><img src="<?php echo imagesPath(); ?>/socnet-fb.png" width="25" height="23" /></a>
                <a href="mailto:?subject=AFP Scorecard&body= I just learned all about how my lawmakers are voting on issues of economic freedom.  Americans for Prosperity’s scorecard gives you a great look at how our elected officials are voting.  Click here to find your legislators’ scores. http://www.afpscorecard.org"><img src="<?php echo imagesPath(); ?>/socnet-email.png" width="33" height="23" /></a>
            </div>
            <br class="clear" />
        </div>
    </div><!-- end bannerRow -->
    
    <div id="rowOuter" class="searchRow active">
        <div id="rowInner">
            <div id="searchNavBox">
                <div id="searchNav">
                    <p>Americans for Prosperity ranks members of Congress based on their votes for economic freedom.</p>
                    <h1>Start Your Search</h1>
                    <div title="byName" class="button btnSearch btnGold">Search by Name</div>
                    <div title="byZip" class="button btnSearch btnGold">Search by Zip Code</div>
                    <div title="byState" class="button btnSearch btnGold active" style="color: rgb(255, 255, 255);">Search by State</div>
                    <div title="byIssue" class="button btnSearch btnGold">Search by Issue</div>
                    <div title="oResults" class="button btnSearch btnGold route" data-role="allResults">Overall Results</div>
                    
                  <div title="byName" class="button fieldSearch btnGold noHover">
                        <form class="namesearch">
                            <input name="name" id="name" class="searchNavField rounded clearinput bigname name" type="text" value="SEARCH BY LAST NAME" />
                            <input name="submit" type="image" class="rounded btnBlack" src="<?php echo imagesPath(); ?>/icon-search.png" id="submit" />
                        </form>
                    </div>
                    <div title="byZip" class="button fieldSearch btnGold noHover">
                        <form class="zipsearch">
                            <input name="zip" id="zip" class="searchNavField rounded clearinput zip" type="text" value="SEARCH BY ZIP CODE" />
                            <input name="submit" type="image" class="rounded btnBlack" src="<?php echo imagesPath(); ?>/icon-search.png" id="submit" />
                        </form>
                    </div>
                    <div title="byState" class="button fieldSearch btnGold noHover">
                        <form class="smallState">
                           <div class="searchNavField rounded">
                            <select name="state" id="state">
                                <option value="None" selected="selected">SEARCH BY STATE</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            </div>
                          <input name="submit" type="image" class="rounded btnBlack" src="<?php echo imagesPath(); ?>/icon-search.png" id="submit" />
                        </form>
                    </div>
                    <div title="byIssue" class="button fieldSearch btnGold noHover">
                        <form class="smallIssue">
                            <div class="searchNavField rounded">
                            <select name="issue" id="issue">
                                <option value="None" selected="selected">SEARCH BY ISSUE</option>
                                <option value="budget">BUDGET &amp; SPENDING</option>
                                <option value="energy">ENERGY &amp; ENVIROMENT</option>
                                <option value="healthcare">HEALTHCARE &amp; ENTITLEMENTS</option>
                                <option value="taxes">TAXES</option>
                                <option value="labor">LABOR, EDUCATION &amp; PENSIONS</option>
                                <option value="banking">BANKING &amp; FINANCIAL SERVICES</option>
                                <option value="property">PROPERTY RIGHTS</option>
                                <option value="technology">TECHNOLOGY</option>
                            </select>
                            </div>
                            <input name="submit" type="image" class="rounded btnBlack" src="<?php echo imagesPath(); ?>/icon-search.png" id="submit" />
                        </form>
                    </div>
                    <a href="#" class="route" data-role="allResults"> <div title="oResults" class="button fieldSearch btnGold">Overall Results</div></a>
                    
              </div>
                <div class="searchToggle"></div>
            </div><!-- end SearchNavBox -->
            
            <div id="searchConBox">
                <!-- BY NAME SEARCHBOX CONTENT -->
                <div id="byName" class="searchDD">
                    <img src="<?php echo imagesPath(); ?>/hdr-byName.png" width="318" height="111" />
                    <form class="namesearch">
                        <div class="searchFieldOuter rounded"><input name="name" id="bigname" class="searchField rounded clearinput name bigname" type="text" value="LAST NAME" /></div>
                        <br />
                        <input name="submit" type="submit" class="button btnGold" id="submit" value="SUBMIT" />
                    </form>
                </div>
                <!-- BY ZIPCODE SEARCHBOX CONTENT -->
                <div id="byZip" class="searchDD">
                    <img src="<?php echo imagesPath(); ?>/hdr-byZip.png" width="254" height="111" />
                    <form class="zipsearch">
                        <div class="searchFieldOuter rounded"><input name="zip" id="zip" class="searchField rounded clearinput zip" type="text" value="SEARCH" /></div>
                        <br />
                        <input name="submit" type="submit" class="button btnGold" id="submit" value="SUBMIT" />
                    </form>
                </div>
                <!-- BY STATE SEARCHBOX CONTENT -->
                <div id="byState" class="searchDD"><img src="<?php echo imagesPath(); ?>/map.png" usemap="#Map" width="549" height="400" /></div>
                <!-- BY ISSUE SEARCHBOX CONTENT -->
                <div id="byIssue" class="searchDD">
                    <a href="#" class="route" data-role="issue#budget"><img src="<?php echo imagesPath(); ?>/sIcon-budget.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#energy"><img src="<?php echo imagesPath(); ?>/sIcon-energy.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#health"><img src="<?php echo imagesPath(); ?>/sIcon-health.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#taxes"><img src="<?php echo imagesPath(); ?>/sIcon-taxes.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#labor"><img src="<?php echo imagesPath(); ?>/sIcon-labor.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#banking"><img src="<?php echo imagesPath(); ?>/sIcon-bank.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#property"><img src="<?php echo imagesPath(); ?>/sIcon-property.png" width="98" height="105" /></a>
                    <a href="#" class="route" data-role="issue#technology"><img src="<?php echo imagesPath(); ?>/sIcon-tech.png" width="98" height="105" /></a>
                    <br class="clear" />
                    <p>Click an Issue to go to the Issue Page</p>
                </div>
                <!-- OVERALL RESULTS SEARCHBOX CONTENT -->
                <div id="oResults" class="searchDD" >
                    
                </div>
            </div><!-- end SearchBox -->
            
            <br class="clear" />
        </div>
    </div><!-- end searchRow -->