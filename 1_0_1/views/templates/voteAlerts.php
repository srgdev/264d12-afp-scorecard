<div id="rowInner">
    <div id="conBox" class="hasSidebar">
        <div id="mainBox" class="rounded column">
            <h1>Key Vote Alerts </h1>
            
            <?php
            if(count($data) == 0){
                ?>
                <h3>No Alerts Found</h3>
                <?php
            }
             foreach($data as $alert){ ?>
            <div class="newsItem">
                <h3 class="link" id="<?php echo $alert->id; ?>"><?php echo stripslashes($alert->title); ?></h3>
                <span class="date"><?php echo date_to_human($alert->date , $format = 'F j, Y') ?></span><img class="smallTag" src="<?php echo imagesPath() ?>/smallTag.png" />
                <span class="date"><?php echo ucwords(get_issue($alert->issue)) ?></span>
                <?php echo stripslashes($alert->excerpt); ?>
            </div>
            <?php } ?>
            <?php  if($total > 1){ ?>
                <div class="pagination">
                    <?php for($i =1; $i <= $total; $i++){ ?>
                        <div class="pagi <?php if($page == $i){ echo 'active';} ?>" id="<?php echo $i ?>"><?php echo  $i ?></div>
                    <? } ?>
                    <br class="clear" />
                </div>
            <?php }  ?>
        </div>
        
         <div id="sidebar" class="rounded column">
             <?php $this->load->view('templates/alertsSearch') ?>
            <?php $this->load->view('templates/joinForm') ?>
            
        </div>
        <br class="clear" />
    </div> <!--end conBox -->
    
</div>

