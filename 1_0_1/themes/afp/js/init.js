$(function () { 
		
	
	
	//set up url checking
	app.init();
	app.checkURL();	
	setInterval("app.checkURL()",250);
	$('.sesNum').html(app.getSession());
	//set up click Routes
	$('.route').live('click', function(){
		var route = $(this).attr('data-role');		
		window.open('#'+route, '_self');
		return false;
	})
	
	$('.routeState').live('click', function(){
		var route = $(this).attr('alt');		
		window.open('#'+route, '_self');
		return false;
	})
	
	$('.zipsearch').submit(function(){
		var zip = $('.zip:visible').attr('value');
		var route = 'zip#'+zip;	
		window.open('#'+route, '_self');
		return false;
	})
	
	$('.namesearch').submit(function(){		
		var name = $('.name:visible').attr('value');
		var route = 'last#'+name;	
		window.open('#'+route, '_self');
		return false;
	})
	
	$('.smallIssue').submit(function(){
		var issue = $('#issue').val();
		var route = 'issue#'+issue;	
		window.open('#'+route, '_self');
		return false;
	})
	
	
	$('.smallState').submit(function(){
		var state = $('#state').val();
		var route = 'state#'+state.toLowerCase();	
		window.open('#'+route, '_self'); 
		return false;
	})
	
	//Setup Auot last Name Suggestion
	$('.bigname').autocomplete({
        serviceUrl: 'http://afpscorecard.org/api/index.php/scorecard/lastnames/format/json/congress/'+app.getSession(),
        onSelect: function(suggestion) {
            
        }
    });
    
    
    //Key vote links
    $('.link').live('click', function(){
    	var id = $(this).attr('id');
    	var route = 'singleAlert#'+id;
    	window.open('#'+route, '_self');
		return false;
    })
    
    //key vote pagination
    $('.pagi').live('click', function(){
    	hash = window.location.hash;
		hash = hash.replaceAll("%23","#");
		
		if(hash.split('#')[1] == 'voteAlerts'){
	    	var id = $(this).attr('id');
	    	var route = 'voteAlerts#'+id;
	    	window.open('#'+route, '_self');
	   }
	   
	   if(hash.split('#')[1] == 'alertissue'){
	    	var id = $(this).attr('id');
	    	var issue = hash.split('#')[2]
	    	var route = 'alertissue#'+issue+'#'+id;
	    	window.open('#'+route, '_self');
	   }
	   
		return false;
    })
    
    $('form.alertSearch').live('submit',function(){
    	var search = $('#search').attr('value');
    	var route = 'alertSearch#'+search;
    	window.open('#'+route, '_self');
    	return false;
    });
    
    
    //Lower search button
    $('.lowerSearch').live('click', function(){
    	$('html, body').animate({
				scrollTop: $(".topRow").offset().top
			}, 500);
    })
    
    $('.sessionList').change(function(){
    	var session = $(this).val();
    	app.changeSession(session);
    	window.location.reload()
    })
    
    $('.sessionList2').live('change', function(){
    	var session = $(this).val();
    	$('.sessionList').val(session)
    	app.changeSession(session);
    	window.location.reload()
    })
    
    //Toggle scores
    $('.lifetimeToggle').live('click', function(){
    	$('.session').toggle()
    	$('.lifetime').toggle();
    	if($('.lifetime').is(":visible")){
    		$(this).html('<strong>LIFETIME</strong> | SESSION Scores');
    	}else{
    		$(this).html('LIFETIME | <strong>SESSION</strong> Scores');
    	}
    	return false;
    })
    
    //Chpter Select
    $('.chapterList').change(function(){
    	window.location = $(".chapterList option:selected").val();
    });
    
    
    $('.voteMemberSort').live('change', function(){
    	var val = $(this).val();
    	if(val != 'NONE'){
	    	var hash = window.location.hash;
	    	var route = 'sortVote#'+hash.split('#')[2]+'#'+val
	    	window.open('#'+route, '_self');
	    }	
    	return false;
    })
    
    $('.memVoteSort').live('change', function(){
    	var val = $(this).val();
    	if(val != 'NONE'){
	    	var hash = window.location.hash;
	    	var route = 'sortMem#'+hash.split('#')[2]+'#'+val
	    	window.open('#'+route, '_self');
	    }	
    	return false;
    })
    
    $('.issueVoteSort').live('change', function(){
    	var val = $(this).val();
    	if(val != 'NONE'){
	    	var hash = window.location.hash;
	    	var route = 'sortIssue#'+hash.split('#')[2]+'#'+val
	    	window.open('#'+route, '_self');
	    }	
    	return false;
    })
    
     $('.overallSort').live('change', function(){
    	var val = $(this).val();
    	if(val != 'NONE'){
	    	var hash = window.location.hash;
	    	var route = 'sortOverall#'+val
	    	window.open('#'+route, '_self');
	    }	
    	return false;
    })
    
    
    $('.allMemberSort').live('change', function(){
    	var val = $(this).val();
    	if(val != 'NONE'){
	    	var hash = window.location.hash;
	    	var route = 'sortOverallMembers#'+val
	    	window.open('#'+route, '_self');
	    }	
    	return false;
    })
    
    
    $('.embed').live('click', function(){
    	$('#myModal').modal('show');
    	return false;
    })
    
    //Join Form
    $('.joinForm').live('submit', function(){
    	var loading = '<input  type="button" class="button btnGold btnSend"  value="SENDING" />';
    	$('#submitOuter').html(loading);
    	var fname = $('#fname').val();
    	var lname = $('#lname').val();
    	var email = $('#email').val();
    	var zip   = $('#zipcode').val();
    	$.ajax({
    		type: "POST",
			url: "http://afpscorecard.org/index.php/welcome/formSubmit",
			data: { first_name: fname, last_name: lname, email: email, zip: zip },
			success: function(data){
				$('#submitOuter').html('Thank You')
			}
    	})
    	return false;
    })
    
    //Development ChangeLog
    $('.openLog').click(function(){
    	$('.changelog').toggle();
    	return false;
    })
	
});

function unescape(str)
    {
     return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
        }
      });

    
    }
            