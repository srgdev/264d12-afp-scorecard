//image path for templates


if (!this.routes) { 
	this.routes = new function() {
		
		
		this.route = function(route, data){
			switch(route){
				case 'home':
					var HTML = data;
					break;
				case 'allResults':
					log(data);
					var HTML = app.getPage("overall",data);
					break;
				case 'allResultsMembers':
					log(data);
					var HTML = app.getPage("overallMembers",data);
					break;
				case 'zip':
					log(data);
					var HTML = app.getPage("search",data);
					break;
				case 'vote':
					log(data);
					var HTML = app.getPage("vote",data);
					break;
				case 'member':
					log(data);
					var HTML = app.getPage("member",data);
					break;
				case 'issue':
					log(data);
					var HTML = app.getPage("issue",data);
					break;
				case 'name':
					log(data);
					var HTML = app.getPage("search",data);
					break;
				case 'state':
					log(data);
					var HTML = app.getPage("state",data);
					break;
				case 'singleAlert':
					var HTML = data;
					break;
				case 'voteAlerts':
					var HTML = data;
					break;
			}
			app.loadTemplate(HTML, ".contentRow");
			var sesNum = app.getSession();
			$('.sessionList2').val(sesNum)
			$('.sessionList').val(sesNum)
			$('.sesNum').html(app.getSession());
			$('.sortList').Selectyze({
		        theme : 'afpGrey'
		    });
		    $('.sessionList2').Selectyze({
				theme : 'afpLG'
			});
			app.unBlockUI();
		}
		
	}
}